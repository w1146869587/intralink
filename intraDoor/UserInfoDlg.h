#if !defined(AFX_USERINFODLG_H__4136D257_8C94_451E_9E3C_1EDBD25956FF__INCLUDED_)
#define AFX_USERINFODLG_H__4136D257_8C94_451E_9E3C_1EDBD25956FF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UserInfoDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CUserInfoDlg dialog
#include "AvatarCtrl.h"

class CUserInfoDlg : public CDialog
{
// Construction
public:
	CUserInfoDlg(USERINFO *pUserInfo);   // standard constructor
// Dialog Data
	//{{AFX_DATA(CUserInfoDlg)
	enum { IDD = IDD_USERINFO };
	CAvatarCtrl	m_ctrlAvatar;
	CString	m_strDesc;
	CString	m_strName;
	CString	m_strPhone;
	CString	m_strGroup;
	//}}AFX_DATA
	int	m_nSex;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUserInfoDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	afx_msg void OnSexClick(UINT uSex);

	// Generated message map functions
	//{{AFX_MSG(CUserInfoDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnUserAvatar();
	afx_msg void OnAvatarNormal();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_USERINFODLG_H__4136D257_8C94_451E_9E3C_1EDBD25956FF__INCLUDED_)
