// intraDoor.h : main header file for the INTRADOOR application
//

#if !defined(AFX_INTRADOOR_H__F7A1BF5B_15A2_45A2_8A98_D02B3AB6A9B5__INCLUDED_)
#define AFX_INTRADOOR_H__F7A1BF5B_15A2_45A2_8A98_D02B3AB6A9B5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CIntraDoorApp:
// See intraDoor.cpp for the implementation of this class
//
class CIntraDoorDlg;

class CIntraDoorApp : public CWinApp
{
public:
	int GetBroadcastIP(SOCKET s,DWORD dwBrdcstIP[20]);
	void BrdcstMsg(DWORD dwMainType,DWORD dwSubType,LPVOID pData,DWORD dwSize);
	BOOL InputUserInfo();
	BOOL LoadUserInfo();
	BOOL SendMsg(DWORD dwIP,USHORT uPort,DWORD dwMainType,DWORD dwSubType,const LPVOID pData,DWORD dwSize);
	int Recv(SOCKET s,char *szBuf,int nLen,DWORD dwTimeout=INFINITE);
	int Send(SOCKET s,char *szBuf,int nLen,DWORD dwTimeout=INFINITE);
	int RecvFrom(SOCKET s,char *szBuf,SOCKADDR* pAddr);
	BOOL IsLocalIP(DWORD dwIP);
	CIntraDoorApp();
	USERINFO		m_UserInfo;
	DWORD			*m_pdwLocalIP;
	char			m_szPath[MAX_PATH];
	DWORD			m_dwBrdcstIP[20];
	CIntraDoorDlg	*m_pDlg;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIntraDoorApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CIntraDoorApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

extern CIntraDoorApp theApp;
void SetListCtrlItemImage(CListCtrl *plstCtrl, int iItem,int iImage);
int GetListCtrlItemImage(CListCtrl *plstCtrl,int iItem);

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INTRADOOR_H__F7A1BF5B_15A2_45A2_8A98_D02B3AB6A9B5__INCLUDED_)
