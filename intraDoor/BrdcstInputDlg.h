#if !defined(AFX_BRDCSTINPUTDLG_H__73EAD857_B221_40D4_9C01_26E4985B29A2__INCLUDED_)
#define AFX_BRDCSTINPUTDLG_H__73EAD857_B221_40D4_9C01_26E4985B29A2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BrdcstInputDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CBrdcstInputDlg dialog

class CBrdcstInputDlg : public CDialog
{
// Construction
public:
	CBrdcstInputDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CBrdcstInputDlg)
	enum { IDD = IDD_BRDCST };
	CString	m_strContent;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBrdcstInputDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CBrdcstInputDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BRDCSTINPUTDLG_H__73EAD857_B221_40D4_9C01_26E4985B29A2__INCLUDED_)
