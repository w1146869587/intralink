// GameSetupDirDlg.cpp : implementation file
//

#include "stdafx.h"
#include "intradoor.h"
#include "GameSetupDirDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGameSetupDirDlg dialog


CGameSetupDirDlg::CGameSetupDirDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CGameSetupDirDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CGameSetupDirDlg)
	m_strDir = _T("");
	m_strTip = _T("");
	//}}AFX_DATA_INIT
}


void CGameSetupDirDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGameSetupDirDlg)
	DDX_Text(pDX, IDC_GAMEDIR, m_strDir);
	DDV_MaxChars(pDX, m_strDir, 50);
	DDX_Text(pDX, IDC_TIP, m_strTip);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CGameSetupDirDlg, CDialog)
	//{{AFX_MSG_MAP(CGameSetupDirDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGameSetupDirDlg message handlers

void CGameSetupDirDlg::OnOK() 
{
	UpdateData();
	char szFullPath[MAX_PATH];
	sprintf(szFullPath,"%s\\game\\%s",theApp.m_szPath,m_strDir);
	if(GetFileAttributes(szFullPath)!=-1)
	{
		MessageBox("目录已经存在，请重新输入");
		return;
	}
	EndDialog(IDOK);
}
