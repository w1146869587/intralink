#if !defined(AFX_GAMESVRDLG_H__DE0A7BFC_8E2F_46D9_BE5B_716B1601BC34__INCLUDED_)
#define AFX_GAMESVRDLG_H__DE0A7BFC_8E2F_46D9_BE5B_716B1601BC34__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GameSvrDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CGameSvrDlg dialog

class CGameSvrDlg : public CDialog
{
// Construction
public:
	CGameSvrDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CGameSvrDlg)
	enum { IDD = IDD_GAMESVR };
	CListCtrl	m_lstGameSvr;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGameSvrDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CGameSvrDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnLogin();
	afx_msg void OnEndsvr();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GAMESVRDLG_H__DE0A7BFC_8E2F_46D9_BE5B_716B1601BC34__INCLUDED_)
