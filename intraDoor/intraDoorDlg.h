// intraDoorDlg.h : header file
//

#if !defined(AFX_INTRADOORDLG_H__D9E38FE1_528A_4B66_8F30_45A01EAF0C3C__INCLUDED_)
#define AFX_INTRADOORDLG_H__D9E38FE1_528A_4B66_8F30_45A01EAF0C3C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CIntraDoorDlg dialog
#include <afxtempl.h>
#include "chatdlg.h"
#include "FileTransDlg.h"
#include "FindDlg.h"

#define WM_MYRESTORE	(WM_USER+200)
#define WM_SHELLNOTIFY	(WM_USER+201)

typedef struct tagGAMEINFO
{
	char szName[100];		//游戏名称
	char szPath[100];		//游戏路径
	char szGuid[200];		//游戏ID
	char szClient[100];		//客户端名称
	char szServer[100];		//服务器名称
	DWORD dwVersion;		//客户端版本
}GAMEINFO;

typedef struct tagGAMESVR
{
	DWORD dwThreadID;
	char szName[100];
	char szClientPath[MAX_PATH];
}GAMESVR;

class CIntraDoorDlg : public CDialog
{
// Construction
public:
	int FindUser(DWORD dwIP);
	void OnGameDownloadFinish(char *pszGameName,char *pszMsgBuf, DWORD dwLen);
	BOOL AddGame(GAMEINFO &gi);
	void ShowTray(BOOL bShow);
	void AddUser2List(DWORD dwIP,USERINFO ui);
	void RemoveUserFromList(DWORD dwIP);
	void ReqUploadDir(DWORD dwIP,LPCTSTR pszDirName);
	DWORD CalcDir(LPCTSTR pszDirName,int &nFiles,int &nDirs);
	USHORT GetSvrPort();
	void ReqUploadGame(DWORD dwIP, LPCTSTR pszDirName,LPCTSTR pszNewDirName,char * pszInviteMsg,DWORD dwInviteMsgLen);
	void ReqUploadFile(DWORD dwIP, LPCTSTR pszFileName);
	int GUID2GameInfo(char *pszGuid);
	void GetHostName(SOCKADDR_IN *pAddr,char *pszName);
	void ExtractUserInfo(char *pszBuf,int nSize,USERINFO &ui);
	DWORD FillUserInfo(char *pszBuf,USERINFO ui);
	CIntraDoorDlg(CWnd* pParent = NULL);	// standard constructor

	SOCKET		m_sockBcRecv;		//接收用的Socket

	CList<CChatDlg*,CChatDlg*>	m_lstChatDlg;	//保存聊天窗口列表
	CFileTransDlg			*	m_pFileTransDlg;//文件传输窗口
	CFindDlg				*	m_pFindDlg;		//用户查找窗口
	
	CMap<DWORD,DWORD,USERINFO,USERINFO&>	m_mapIP2UI;//所有用户的IP与信息对照表

	GAMESVR						m_GameSvr[MAX_GAMESVR];
	CImageList					m_ilGame,m_ilFace;
// Dialog Data
	//{{AFX_DATA(CIntraDoorDlg)
	enum { IDD = IDD_INTRADOOR_DIALOG };
	CTabCtrl	m_tabGroup;
	CListCtrl	m_lstGame;
	CListCtrl	m_lstUser;
	//}}AFX_DATA
	
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIntraDoorDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void UpdateGroupList();
	void SaveState();
	void DeleteGameList();
	afx_msg LRESULT OnSocketMsg(WPARAM wParam,LPARAM lParam);
	afx_msg LRESULT OnShellNotify(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMyRestore(WPARAM, LPARAM);
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CIntraDoorDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnDestroy();
	afx_msg void OnDblclkListUser(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnRclickListUser(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnUserChat();
	afx_msg void OnUserSendfile();
	afx_msg void OnUserSenddir();
	afx_msg void OnUserRefresh();
	afx_msg void OnUserFind();
	afx_msg void OnHelpAbout();
	afx_msg void OnHelpIndex();
	afx_msg void OnSysQuit();
	afx_msg void OnSysShowmain();
	afx_msg void OnSysUserinfo();
	afx_msg void OnWinGameSvr();
	afx_msg void OnClose();
	afx_msg void OnGameNew();
	afx_msg void OnGameDel();
	afx_msg void OnStateSet();
	afx_msg void OnHideme();
	afx_msg void OnUserBrdcst();
	afx_msg void OnGameRefresh();
	afx_msg void OnEndSession(BOOL bEnding);
	afx_msg void OnRclickListGame(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnFiletransMonitor();
	afx_msg void OnSelchangeTabGroup(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnUserChatgroup();
	//}}AFX_MSG
	virtual void OnOK(){}
	virtual void OnCancel(){PostMessage(WM_CLOSE);}
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INTRADOORDLG_H__D9E38FE1_528A_4B66_8F30_45A01EAF0C3C__INCLUDED_)
