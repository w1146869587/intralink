// ioGif.cpp: implementation of the CioGif class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ioGif.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CioGif::CioGif()
{
	m_byBlockSize=0;
	m_byBlockOffset=0;
	m_lCurBlock=m_lBegin;
}

CioGif::~CioGif()
{

}

UINT CioGif::Read(BYTE *pBuf, UINT uSize,BOOL bAutoSeek)
{
	ASSERT(m_byBlockSize!=0);
	UINT uRet=0;

	BYTE byBlockSize=m_byBlockSize;
	BYTE byBlockOffset=m_byBlockOffset;
	long lCurBlock=m_lCurBlock;
	
	while(uSize!=0)
	{
		BYTE byBlockRemain=byBlockSize-byBlockOffset;
		BYTE byRead=uSize>byBlockRemain?byBlockRemain:uSize;
		UINT uReaded=m_pFile->Read(pBuf,byRead);//fread(pBuf,byRead,1,m_pFile);

		uRet+=uReaded;
		uSize-=byRead;
		if(uReaded<byRead) break;//到段尾
		pBuf+=byRead;
		byBlockOffset+=byRead;
		if(byBlockOffset==byBlockSize)//block end
		{
			lCurBlock=m_pFile->GetPosition();
			byBlockSize=0;
			byBlockOffset=0;
			m_pFile->Read(&byBlockSize,1);
			if(byBlockSize==0) break;//raw data end
		}
	}
	if(bAutoSeek)
	{
		ASSERT(uSize==0);
		m_byBlockSize=byBlockSize;
		m_byBlockOffset=byBlockOffset;
		m_lCurBlock=lCurBlock;
	}else
	{
		m_pFile->Seek(m_lCurBlock+m_byBlockOffset+1,CFile::begin);
	}
	return uRet;
}

UINT CioGif::Write(BYTE *pBuf, UINT uSize,BOOL bAutoSeek)
{
	UINT uRet=0;

	BYTE byBlockSize=m_byBlockSize;
	BYTE byBlockOffset=m_byBlockOffset;
	long lCurBlock=m_lCurBlock;

	while(uSize!=0)
	{
		BYTE byBlockRemain=m_byBlockSize-m_byBlockOffset;
		if(byBlockRemain==0) 
		{//写入一个新段
			WriteBlankBlock();
			byBlockRemain=0xFF;
		}
		BYTE byWrite=uSize>byBlockRemain?byBlockRemain:uSize;
		m_pFile->Write(pBuf,byWrite);
		uSize-=byWrite;
		pBuf+=byWrite;

		uRet+=byWrite;
		m_byBlockOffset+=byWrite;
	}
	if(!bAutoSeek)
	{
		m_byBlockSize=byBlockSize;
		m_byBlockOffset=byBlockOffset;
		m_lCurBlock=lCurBlock;
		m_pFile->Seek(m_lCurBlock+m_byBlockOffset+1,CFile::begin);
	}
	return uRet;
}

void CioGif::Attach(CFile *f,BOOL bRead)
{
	m_pFile=f;
	m_lCurBlock=m_lBegin=m_pFile->GetPosition();
	if(bRead)
	{//read
		m_pFile->Read(&m_byBlockSize,1);
		m_byBlockOffset=0;
	}else
	{//write
		m_byBlockOffset=0xFF;//在写入数据后，offset会更新
		m_byBlockSize=0xFF;
	}
}

CFile * CioGif::Detach()
{
	CFile *pRet=m_pFile;
	m_pFile=NULL;
	return pRet;
}

CFile * CioGif::WriteEndFlag()
{
	m_pFile->Seek(m_lCurBlock,CFile::begin);
	m_pFile->Write(&m_byBlockOffset,1);//更新最后一块的块大小
	m_pFile->Seek(m_byBlockOffset,CFile::current);
	if(m_byBlockOffset!=0)
	{
		BYTE byEndFlag=0;
		m_pFile->Write(&byEndFlag,1);
	}
	DWORD dwLen=m_pFile->GetPosition();
	m_pFile->SetLength(dwLen);//丢弃一段中没有使用的空间
	return Detach();
}

void CioGif::WriteBeginFlag(BYTE byInitBits)
{
	m_pFile->Write(&byInitBits,1);
	WriteBlankBlock();
}

//return 0-FALSE,1-OK
DWORD CioGif::Seek(int nOffset,IOSEEK ioSeek)
{
	ASSERT(ioSeek==CUR);	//只支持CUR模式的Seek;
	if(nOffset<0)
	{
		int nTmp=m_byBlockOffset;
		nTmp+=nOffset;
		if(nTmp>=0)
		{//还在段内
			m_byBlockOffset+=nOffset;
			m_pFile->Seek(nOffset,CFile::current);
		}else
		{//到了前一个段,需要从数据段的最开始来找到上一段
			BYTE bySegLen[100]={0};//段长度栈
			char cSeg=0;
			m_pFile->Seek(m_lBegin,CFile::begin);
			//取得之前的所有段长度
			while((long)m_pFile->GetPosition()<m_lCurBlock )
			{
				m_pFile->Read(bySegLen+cSeg,1);
				m_pFile->Seek(bySegLen[cSeg++],CFile::current);
			}
			nOffset+=m_byBlockOffset;
			//回朔
			while(nOffset<0 && cSeg>=0)
			{
				nOffset+=bySegLen[cSeg];
				m_pFile->Seek(-(int)bySegLen[cSeg]-1,CFile::current);
				cSeg--;
			}
			ASSERT(nOffset>=0);
			m_lCurBlock=m_pFile->GetPosition();
			m_byBlockSize=bySegLen[cSeg];
			m_byBlockOffset=nOffset;
			m_pFile->Seek(1,CFile::current);
			m_pFile->Seek(m_byBlockOffset,CFile::current);
		}
	}else
	{//向后SEEK
		while(nOffset>0)
		{
			BYTE byRemain=m_byBlockSize-m_byBlockOffset;
			BYTE bySeek=nOffset>byRemain?byRemain:nOffset;
			m_pFile->Seek(bySeek,CFile::current);
			m_byBlockOffset+=bySeek;
			nOffset-=bySeek;
			if(m_byBlockOffset==m_byBlockSize)
			{
				m_lCurBlock=m_pFile->GetPosition();
				m_byBlockSize=0;
				m_pFile->Read(&m_byBlockSize,1);
				m_byBlockOffset=0;
				if(m_byBlockSize==0) return 0;
			}
		}
	}
	return 1;
}