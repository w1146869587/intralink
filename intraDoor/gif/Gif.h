/** \file Gif.h
 * GIF解码及显示类的定义
 */
 
/** @defgroup GIF GIF文件读取
 *  @author  黄建雄
 *  @version 1.0
 *  @date    2006
 *  @{
 */
#pragma once
#include "lzw.h"
#include "iogif.h"

/**计算一个象素的占用的字节数
* @param[in] bits:图象色深
*/
#define WIDTHBYTES(bits)    (((bits) + 31) / 32 * 4)

class CGIF;

/** @defgroup CGifFrame CGifFrame GIF图象帧
 *  @date    2006
 *  @{
 */
class  CGifFrame
{
	friend class CGIF;
public:
	CGifFrame(CGIF *pGif);
	virtual ~CGifFrame();
	HBITMAP ToHBITMAP();
	HBITMAP ToHBITMAP(int nLeft,int nTop,int nWidth,int nHeight);
	BOOL ShowImage(HDC hDC,int xDest,int yDest,int xSour,int ySour,int nWidth,int nHeight,DWORD dwRop=SRCCOPY);
	BOOL ShowImage2(HDC hDC,int xDest,int yDest,int xSour,int ySour,int nWidth,int nHeight,DWORD dwRop=SRCCOPY);
	LPBYTE GetImageData(){return m_pImageData;}
	RGBQUAD *GetPaletteData(){return m_pPalette;}
	BOOL SaveBitmap(LPCTSTR pszFileName);
	COLORREF GetTransColor()
	{
		return RGB(m_pPalette[m_idxTransColor].rgbRed,
					m_pPalette[m_idxTransColor].rgbGreen,
					m_pPalette[m_idxTransColor].rgbBlue);
	}
	BOOL IsTransColor(BYTE byIndex)
	{
		if(m_idxTransColor<0) return FALSE;
		return byIndex==(BYTE)m_idxTransColor;
	}

	USHORT	m_nDelayTime;
	USHORT	m_nMarginLeft;
	USHORT	m_nMarginTop;
	USHORT	m_nWidth;
	USHORT	m_nHeight;
	//!公用调色板标志
	BOOL	m_bCommonPal;		
private:
	short	m_idxTransColor;
    LPBYTE     m_pImageData;
	RGBQUAD	*  m_pPalette;
	BYTE	   m_byPalDeep;	//颜色表色深-1:0,3,7
	//!GIF指针
	CGIF	*  m_pGif;	
	BYTE	m_byDibHeader[sizeof(BITMAPINFOHEADER) + 256 * sizeof(RGBQUAD)];
};
/** @}*/ // CGifFrame

/** @defgroup CGIF CGIF CGIF
 *  @date    2006
 *  @{
 */
class  CGIF  
{
#pragma pack(push,1)
	//!GIF 文件頭		
	typedef struct tagGIFHEADER
	{
		char  Signature[6];//!<圖像格式，版本
		USHORT ScreenWidth;//!<圖像寬度
		USHORT ScreenDepth;//!<圖像高度
		UCHAR GlobalFlagByte;//!<全局標記 a0 -- a7 有含意
		UCHAR BackGroundColor;//!<背景色
		UCHAR AspectRadio;//!<89a版的長寬比
	}GIFHEADER;

	//!圖像數據區識別信息
	typedef struct tagIMAGEDATAINFO
	{
		USHORT  ImageLeft; 
		USHORT  ImageTop;
		USHORT  ImageWidth;
		USHORT  ImageHeight;
		UCHAR  LocalFlagByte; 
	}IMAGEDATAINFO;

	//!below only for 89a
	typedef struct tagGRAPHCTRL
	{
		UCHAR	PackedField;
		USHORT	DelayTime;
		UCHAR	TranColorIndex;
	}GRAPHCTRL;

	typedef struct tagDECODESTATUS
	{
		short   m_CurTColorIndex;
		USHORT  m_CurDelayTime;
		BOOL    m_CurSaveMode;
		USHORT  m_CurX,m_CurY,m_CurWidth,m_CurHeight;
	}DECODESTATUS,*PDECODESTATUS;
#pragma pack(pop)			

public:
	CGifFrame * GetFrame(UINT iFrame=0);
	BOOL Load(LPCTSTR pszFileName);
	BOOL Load(LPBYTE pBuf,int nSize);
	BOOL LoadFile(CFile *file);
	//!将第nFrame保存到GIF文件
	BOOL Save(LPCTSTR pszFileName,int nFrame);
	CGIF();
	virtual ~CGIF();

protected:
	void FreeData();
	CGifFrame * GetDibData(LPBYTE SrcData,PDECODESTATUS pds);

	void SkipBlock(CFile *pf);
	void SkipNoteBlock(CFile *pf);
	void SkipAppBlock(CFile *pf);
	void SkipTextBlock(CFile *pf);
	void GetGrphBlock(CFile *pf,PDECODESTATUS pds);
	void GetImageBlock(void *pImgList,CFile *pf,PDECODESTATUS pds);
	void AnalizeFileHeader(CFile *pf,PDECODESTATUS pds);

	//!图象组
	CGifFrame **m_pFrames;	
	//!全局调色板
	RGBQUAD	 *	m_pPalGlobal;
	//!调色板大小色深-1
	BYTE		m_byPalDeep;
public:
	//!背景颜色
	COLORREF	m_crBack;
	//!图象宽度
	USHORT		m_nWidth;
	//!图象高度
	USHORT		m_nHeight;
	//!图象数
	UINT		m_nFrameCount;
};
/** @}*/ // CGif

/**将hdcSrc中的图象以透明方式显示到hdcDest
* @param[in]  HDC hdcDest handle to destination device context
* @param[in]  int nXDest x-coordinate of destination rectangle's upper-left corner
* @param[in]  int nYDest y-coordinate of destination rectangle's upper-left corner
* @param[in]  int nWidth width of destination rectangle
* @param[in]  int nHeight height of destination rectangle
* @param[in]  HDC hdcSrc  handle to source device context
* @param[in]  int nXSrc x-coordinate of source rectangle's upper-left corner
* @param[in]  int nYSrc y-coordinate of source rectangle's upper-left corner
* @param[in]  COLORREF cTransparentColor 
* @return TRUE-SUCCESS;FALSE-FAILED
*/
BOOL BitBltEx( HDC hdcDest,
	int nXDest,  int nYDest, 
  	int nWidth, int nHeight,
  	HDC hdcSrc, 
  	int nXSrc,  int nYSrc,  
  	COLORREF cTransparentColor
);

/** @}*/ // GIF