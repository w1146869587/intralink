//LZW 压缩解压算法
#pragma once

#include <windows.h>
#include <afxtempl.h>
#include "ioObject.h"
//	解码程序用到String Table的最大长度
#define		LZW_MAX_TABLE_SIZE		4096



typedef CMap<DWORD,DWORD,WORD,WORD> LZWHASH,* PLZWHASH;

class CLZW_Decode
{
	typedef struct tagLZW_STRING
	{
		WORD	wPrefix ;	// 为 Old
		WORD	wSuffix ;	// 为 GetFirstChar (Old) 或 GetFirstChar (Code)
	} LZW_STRING, * PLZW_STRING ;

	typedef struct tagDECODE_STATE
	{
		WORD wCodeClear,wCodeEnd;
		BYTE byInitBits;		
		BYTE byCurBits;
		BYTE byInBits;
		LZW_STRING lzwStrTable[LZW_MAX_TABLE_SIZE];
		WORD wCurTableSize;
	}DECODE_STATE,*PDECODE_STATE;
public:
	/**LZW解码
	* @param[in] CioObject *pInput:输入对象
	* @param[in] CioObject *pOutput:输出对象
	* @param[in] BYTE byInitBits:编码位数
	* @return 是否成功
	* - TRUE 成功
	* - FALSE 失败
	*/
	BOOL	Decode (CioObject *pInput,CioObject *pOutput,BYTE byInitBits) ;
private:
	//	解码
	void	InitStringTable (PDECODE_STATE pDecodeState, BYTE byInitBits) ;
	WORD	GetNextCode (PDECODE_STATE pDecodeState,CioObject *pInput) ; // 最长12位
	bool	IsInTable (PDECODE_STATE pDecodeState,WORD sCode) ;
	void	AddStringToTable (PDECODE_STATE pDecodeState,WORD wPrefix, WORD wSuffix) ;
	BYTE	GetFirstChar (PDECODE_STATE pDecodeState,WORD sCode) ;
	void	WriteString_to8 (PDECODE_STATE pDecodeState,CioObject *pOutput,WORD wCode) ; // 解码到目标8位色位图
};

//!	LZW - 压缩解压算法
//	解码程序用到String Table - string结构
//	每个string可以形成一棵二叉树, 此二叉树仅有一个右节点
//	因为wPrefix总是指向String Table中的另一位置, 而wSuffix指向0 ~ (clear-1)
class CLZW_Encode
{
	typedef struct tagENCODE_STATE
	{
		WORD	wCodeClear,wCodeEnd;
		LZWHASH lzwHash;//!Hash表设计为:(Old << 8) | Pixel 其中存放的是String Table的Index
		WORD	wCurTableSize;
		BYTE	byInitBits;
		BYTE	byOutBits;
		BYTE	byCurBits;
	}ENCODE_STATE,*PENCODE_STATE;
public :

	/**LZW编码
	* @param[in] CioObject *pInput:输入对象
	* @param[in] CioObject *pOutput:输出对象
	* @param[in] BYTE byInitBits:编码位数
	* @return 是否成功
	* - TRUE 成功
	* - FALSE 失败
	*/
	BOOL  Encode (CioObject *pInput,CioObject *pOutput,BYTE byInitBit=8);
private :
	//	编码
	void	InitStringTable (PENCODE_STATE pEncodeState,BYTE byInitBits) ;
	BYTE	GetNextPixel (PENCODE_STATE pEncodeState,CioObject *pInput,DWORD &dwIndex,BYTE byInitBits) ;
	bool	IsInTable (PENCODE_STATE pEncodeState,WORD wOld, WORD wPixel) ; // Old和Pixel都指向在m_pStrTable中的index。
	void	AddStringToTable (PENCODE_STATE pEncodeState,WORD wOld, WORD wPixel) ;
	void	WriteIndex (PENCODE_STATE pEncodeState,CioObject *pOutput,WORD wCodeIndex) ; // 压缩时写String Index
} ;

//===================================================================
//	Implement
//===================================================================

//初始化字符串表
inline void  CLZW_Decode::InitStringTable (PDECODE_STATE pDecodeState,BYTE byInitBits)
{
	if(byInitBits!=0xFF)
	{
		pDecodeState->byInitBits=byInitBits;
		pDecodeState->byInBits=0;
	}
	memset (pDecodeState->lzwStrTable, 0xFF, LZW_MAX_TABLE_SIZE * sizeof(LZW_STRING)) ;
	pDecodeState->wCodeClear=1<<pDecodeState->byInitBits;
	pDecodeState->wCodeEnd=pDecodeState->wCodeClear+1;
	for (WORD i = 0 ; i < pDecodeState->wCodeClear ; i++) // 初始化String Table
		pDecodeState->lzwStrTable[i].wSuffix = i ;
	pDecodeState->wCurTableSize = pDecodeState->wCodeEnd + 1 ;
	pDecodeState->byCurBits=pDecodeState->byInitBits+1;
}

//!获取下一个编码数据，最长12BIT(最多跨越2-BYTE)
//与WriteIndex ()是相对应的
inline WORD  CLZW_Decode::GetNextCode (PDECODE_STATE pDecodeState,CioObject *pInput) 
{
	register DWORD	dwRet = 0 ;
	register UINT	uAdd = pDecodeState->byInBits + pDecodeState->byCurBits ;

	BYTE byCode[4];
	pInput->Read(byCode,4,FALSE);
	if (uAdd <= 8) // 在当前BYTE内
	{
		dwRet |= *byCode;
	}else
	{
		if (uAdd <= 16) // 跨1-BYTE
			dwRet |= *(WORD*)byCode ;
		else // 跨2-BYTE
		{
			dwRet |= * (byCode + 2) ;
			dwRet <<= 16 ;
			dwRet |= * (WORD *) byCode ;
		}
	}
	pInput->Seek((uAdd >> 3),CioObject::CUR);
	pDecodeState->byInBits = (uAdd & 0x00000007);//uAdd % 8;
	dwRet <<= 32 - uAdd ;
	dwRet >>= 32 - pDecodeState->byCurBits ; // 左右清零
	return (WORD)dwRet ;
}

inline bool  CLZW_Decode::IsInTable (PDECODE_STATE pDecodeState,WORD wCode)
{
	return (wCode < pDecodeState->wCurTableSize) ;
}

inline BYTE  CLZW_Decode::GetFirstChar (PDECODE_STATE pDecodeState,WORD wCode)
{
	while (pDecodeState->lzwStrTable[wCode].wPrefix != 0xFFFF)
		wCode = pDecodeState->lzwStrTable[wCode].wPrefix ;
	return (BYTE) pDecodeState->lzwStrTable[wCode].wSuffix ;
}

inline void  CLZW_Decode::AddStringToTable (PDECODE_STATE pDecodeState,WORD wPrefix, WORD wSuffix) 
{
	pDecodeState->lzwStrTable[pDecodeState->wCurTableSize].wPrefix = wPrefix ;
	pDecodeState->lzwStrTable[pDecodeState->wCurTableSize].wSuffix = wSuffix ;
	pDecodeState->wCurTableSize++;
	if ((pDecodeState->wCurTableSize == 0x008) || 
		(pDecodeState->wCurTableSize == 0x010) ||
		(pDecodeState->wCurTableSize == 0x020) || 
		(pDecodeState->wCurTableSize == 0x040) ||
		(pDecodeState->wCurTableSize == 0x080) || 
		(pDecodeState->wCurTableSize == 0x100) ||
		(pDecodeState->wCurTableSize == 0x200) || 
		(pDecodeState->wCurTableSize == 0x400) ||
		(pDecodeState->wCurTableSize == 0x800)
		)
		pDecodeState->byCurBits++ ;
}


//===================================================================
//	Encode
//	BYTE byInitBits==0xFF表示不是第一次初始化
//===================================================================
inline void  CLZW_Encode::InitStringTable (PENCODE_STATE pEncodeState,BYTE byInitBits)
{
	if(byInitBits!=0xFF)
	{
		pEncodeState->byOutBits=0;
		pEncodeState->byInitBits=byInitBits;
		pEncodeState->wCodeClear=1 << byInitBits;
		pEncodeState->wCodeEnd = pEncodeState->wCodeClear + 1 ;
	}
	pEncodeState->byCurBits=pEncodeState->byInitBits+1;
	pEncodeState->wCurTableSize = pEncodeState->wCodeEnd + 1 ;
	pEncodeState->lzwHash.RemoveAll();
}

inline BYTE  CLZW_Encode::GetNextPixel (PENCODE_STATE pEncodeState,CioObject *pInput,DWORD &dwIndex,BYTE byInitBits) 
{
	//	目前只支持1, 4, 8位色, 所以不会跨BYTE
	register BYTE		byRet ,byCode;
	pInput->Read(&byCode,1,FALSE);
	switch (byInitBits)
	{
		case 8 : byRet = byCode ; break ;
		case 4 : byRet = (dwIndex & 0x01 == 0) ? byCode >> 4: byCode & 0x0F ; break ;
		case 1 : byRet = 0x01 & (byCode >> (7 - (dwIndex & 7))) ; break ;
	}
	++dwIndex;
	if(dwIndex*8%byInitBits==0) pInput->Seek(1,CioObject::CUR);
	return byRet;
}

inline bool  CLZW_Encode::IsInTable (PENCODE_STATE pEncodeState,WORD wOld, WORD wPixel) 
{
	return pEncodeState->lzwHash[(wOld << 8) | wPixel] != 0;
}

inline void  CLZW_Encode::AddStringToTable (PENCODE_STATE pEncodeState,WORD wOld, WORD wPixel) 
{
	pEncodeState->lzwHash[(wOld << 8) | wPixel]=pEncodeState->wCurTableSize++;
	if ((pEncodeState->wCurTableSize == 0x009) || (pEncodeState->wCurTableSize == 0x011) ||
		(pEncodeState->wCurTableSize == 0x021) || (pEncodeState->wCurTableSize == 0x041) ||
		(pEncodeState->wCurTableSize == 0x081) || (pEncodeState->wCurTableSize == 0x101) ||
		(pEncodeState->wCurTableSize == 0x201) || (pEncodeState->wCurTableSize == 0x401) ||
		(pEncodeState->wCurTableSize == 0x801))
		pEncodeState->byCurBits++ ;
}

inline void  CLZW_Encode::WriteIndex (PENCODE_STATE pEncodeState,CioObject *pOutput,WORD wCodeIndex)
{
	register UINT	uAdd = pEncodeState->byOutBits + pEncodeState->byCurBits ;
	// 与解码不一样, 压缩时会预留一些内存, 可以用DWORD
	DWORD dwCode=0;
	pOutput->Read((BYTE*)&dwCode,4,FALSE);
	dwCode|=(wCodeIndex << pEncodeState->byOutBits) ;
	pOutput->Write((BYTE*)&dwCode,4,FALSE);
	pOutput->Seek(uAdd>>3,CioObject::CUR);
	pEncodeState->byOutBits = (uAdd & 0x00000007) ;//uAdd % 8 ;
}

