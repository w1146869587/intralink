// ioObject.h: interface for the CioObject class.
//
//////////////////////////////////////////////////////////////////////

#pragma once

class CioObject  
{
public:
	typedef enum tagIOSEEK{BEGIN,CUR,END} IOSEEK;
	CioObject();
	virtual ~CioObject();
	
	virtual UINT Read(BYTE *pBuf,UINT uSize,BOOL bAutoSeek);
	virtual UINT Write(BYTE *pBuf,UINT uSize,BOOL bAutoSeek);
	virtual DWORD Seek(int nOffset,IOSEEK ioSeek);
	virtual DWORD Tell();
	virtual	DWORD GetLength();

	void	Attach(BYTE *pBuf,DWORD dwSize);
protected:
	DWORD	m_dwIndex;
	DWORD	m_dwSize;
	BYTE	*m_pBuf;
};

