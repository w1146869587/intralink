// ioObject.cpp: implementation of the CioObject class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ioObject.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CioObject::CioObject()
{
	m_dwIndex=0;
	m_dwSize=0;
	m_pBuf=NULL;
}

CioObject::~CioObject()
{
	m_dwIndex=0;
	m_dwSize=0;
	m_pBuf=NULL;
}

void CioObject::Attach(BYTE *pBuf,DWORD dwSize)
{
	m_pBuf=pBuf;
	m_dwSize=dwSize;
	m_dwIndex=0;
}

DWORD CioObject::Seek(int nOffset,IOSEEK ioSeek)
{
	switch(ioSeek)
	{
	case BEGIN:m_dwIndex=nOffset;break;
	case CUR:m_dwIndex+=nOffset;break;
	case END:m_dwIndex=m_dwSize+nOffset;break;
	}
	return m_dwIndex;
}

DWORD CioObject::Tell()
{
	return m_dwIndex;
}

DWORD CioObject::GetLength()
{
	return m_dwSize;
}

UINT CioObject::Read(BYTE *pBuf,UINT uSize,BOOL bAutoSeek)
{
	DWORD dwRemain=m_dwSize-m_dwIndex;
	DWORD dwRead=uSize%(dwRemain+1);
	memcpy(pBuf,m_pBuf+m_dwIndex,dwRead);
	if(bAutoSeek) m_dwIndex+=dwRead;
	return dwRead;
}

UINT CioObject::Write(BYTE *pBuf,UINT uSize,BOOL bAutoSeek)
{
	DWORD dwRemain=m_dwSize-m_dwIndex;
	DWORD dwWrite=uSize%(dwRemain+1);
	memcpy(m_pBuf+m_dwIndex,pBuf,dwWrite);
	if(bAutoSeek) m_dwIndex+=dwWrite;
	return dwWrite;
}