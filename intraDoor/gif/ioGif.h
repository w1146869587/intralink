// ioGif.h: interface for the CioGif class.
//
//////////////////////////////////////////////////////////////////////

#pragma once

#include "ioObject.h"

class CioGif : public CioObject  
{
public:
	CFile * WriteEndFlag();
	void WriteBeginFlag(BYTE byInitBitss);
	CFile * Detach();
	void Attach(CFile *f,BOOL bRead);
	CioGif();
	virtual ~CioGif();
	
	UINT Read(BYTE *pBuf,UINT uSize,BOOL bAutoSeek);
	UINT Write(BYTE *pBuf,UINT uSize,BOOL bAutoSeek);
	DWORD Seek(int nOffset,IOSEEK ioSeek);
	
private:
	void WriteBlankBlock()
	{
		m_lCurBlock=m_pFile->GetPosition();
		if(m_lCurBlock==(long)m_pFile->GetLength())
		{
			m_byBlockSize=255;
			m_pFile->Write(&m_byBlockSize,1);
			BYTE byBlock[255]={0};
			m_pFile->Write(byBlock,255);
			m_pFile->Seek(-255,CFile::current);
		}else
		{
			m_pFile->Read(&m_byBlockSize,1);
		}
		m_byBlockOffset=0;
	}
	CFile	*m_pFile;
	BYTE	m_byBlockSize;	//块大小
	BYTE	m_byBlockOffset;//块偏移
	long	m_lBegin;		//数据起始位置的绝对偏移
	long	m_lCurBlock;	//当前段的绝对偏移
};
