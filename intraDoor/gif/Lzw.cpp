#include "stdafx.h"
#include "Lzw.h"

//===================================================================
// 目标为8位色
void  CLZW_Decode::WriteString_to8 (PDECODE_STATE pDecodeState,CioObject *pOutput,WORD wCode)
{
	if (wCode < pDecodeState->wCodeClear)
	{
		pOutput->Write((BYTE*)&pDecodeState->lzwStrTable[wCode].wSuffix,1,TRUE);
	}
	else
	{
		WriteString_to8 (pDecodeState,pOutput,pDecodeState->lzwStrTable[wCode].wPrefix) ;
		WriteString_to8 (pDecodeState,pOutput,pDecodeState->lzwStrTable[wCode].wSuffix) ;
	}
}
//===================================================================
BOOL  CLZW_Decode::Decode (CioObject *pInput,CioObject *pOutput,BYTE byInitBits)
{
	PDECODE_STATE pDecodeState=new DECODE_STATE;
	WORD		wCode, wOld ;

	//	初始化设置-----------------------------------------+
	InitStringTable (pDecodeState,byInitBits) ;
	while ((wCode = GetNextCode (pDecodeState,pInput)) != pDecodeState->wCodeEnd)
	{
		if (wCode == pDecodeState->wCodeClear)
		{
			InitStringTable (pDecodeState,0xFF) ;
			while ((wCode = GetNextCode (pDecodeState,pInput)) == pDecodeState->wCodeClear) ;
		}
		else
		{
			if (IsInTable (pDecodeState,wCode))
				AddStringToTable (pDecodeState,wOld, GetFirstChar (pDecodeState,wCode)) ;
			else
				AddStringToTable (pDecodeState,wOld, GetFirstChar (pDecodeState,wOld)) ;
		}
		WriteString_to8 (pDecodeState,pOutput,wCode) ;
		wOld = wCode ;
	}
	delete pDecodeState;
	return TRUE;
}


//===================================================================
BOOL  CLZW_Encode::Encode (CioObject *pInput,CioObject *pOutput,BYTE byInitBit)
{
	//	编码阶段不需要String Table，只需要m_wCurTableSize来获知写位数
	PENCODE_STATE pEncodeState=new ENCODE_STATE;
	if (!pEncodeState)	return FALSE ;

	DWORD dwIndex = 0 ;
	//	初始化设置完成-------------------------------------+

	WORD		wOld ;	// 保留字串
	BYTE		Pixel ;	// 当前读入字符

	InitStringTable (pEncodeState,byInitBit);

	BOOL bError=FALSE;

	WriteIndex (pEncodeState,pOutput,pEncodeState->wCodeClear) ; // 首先写clear
	wOld = GetNextPixel (pEncodeState,pInput,dwIndex,byInitBit) ;
	// 编码
	DWORD dwCodes=pInput->GetLength()*8/byInitBit;
	while (dwIndex < dwCodes)
	{
		Pixel = GetNextPixel (pEncodeState,pInput,dwIndex,byInitBit) ;
		if (IsInTable (pEncodeState,wOld, Pixel))
		{
			wOld = pEncodeState->lzwHash[(wOld << 8) | Pixel] ; // 已在表中, 取出索引, Hash Table中存放的是String Table的Index
		}else
		{
			//	不在表中, 把Old + Pixel添加到String Table中
			WriteIndex (pEncodeState,pOutput,wOld) ;
			AddStringToTable (pEncodeState,wOld, Pixel) ;
			wOld = Pixel ;
			if (pEncodeState->wCurTableSize == LZW_MAX_TABLE_SIZE) // 表填满
			{
				WriteIndex (pEncodeState,pOutput,Pixel) ;
				WriteIndex (pEncodeState,pOutput,pEncodeState->wCodeClear) ;
				InitStringTable (pEncodeState,0xFF) ;
				wOld = GetNextPixel (pEncodeState,pInput,dwIndex,byInitBit) ;
			}
		}
	}
	WriteIndex (pEncodeState,pOutput,wOld) ;
	WriteIndex (pEncodeState,pOutput,pEncodeState->wCodeEnd) ;
	pOutput->Seek(1,CioObject::CUR);//保留一个字节
	delete pEncodeState;
	return TRUE;
}
//===================================================================
