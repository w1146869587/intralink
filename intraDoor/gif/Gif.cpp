/** \file Gif.cpp
 * GIF解码及显示类的实现
 */
 
#include "stdafx.h"
#include "GIF.h"
#include <vector>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
CGifFrame::CGifFrame(CGIF *pGif)
{
	m_pImageData = NULL;
	m_pPalette=NULL;
	m_bCommonPal=TRUE;
	m_pGif=pGif;
}

CGifFrame::~CGifFrame()
{
	if (m_pImageData) GlobalFree(m_pImageData);
	if(!m_bCommonPal && m_pPalette) delete []m_pPalette;
}

//-----------------------------------------------------------------------------
//	将全部可见部分转成位图
//-----------------------------------------------------------------------------
HBITMAP CGifFrame::ToHBITMAP()
{
	int nWidth=m_nWidth;
	if(m_nMarginLeft+nWidth>m_pGif->m_nWidth)
		nWidth=m_pGif->m_nWidth-m_nMarginLeft;
	int nHeight=m_nHeight;
	if(m_nMarginTop+nHeight>m_pGif->m_nHeight)
		nHeight=m_pGif->m_nHeight-m_nMarginTop;
	return ToHBITMAP(m_nMarginLeft,m_nMarginTop,nWidth,nHeight);
}

//------------------------------------------------------------------------------
//	将图片的指定部分转成位图
//	int nLeft,int nTop:位置坐标,包括边距
//	int nWidth,int nHeight:尺寸
//	return:位图句柄
//------------------------------------------------------------------------------
HBITMAP CGifFrame::ToHBITMAP(int nLeft,int nTop,int nWidth,int nHeight)
{
	if(m_pImageData==NULL) return NULL;
	CRect rcRequire=CRect(nLeft,nTop,nLeft+nWidth,nTop+nHeight);
	if(nLeft<0 || 
		nTop<0 || 
		rcRequire.right>m_pGif->m_nWidth||
		rcRequire.bottom>m_pGif->m_nHeight)
		return NULL;
	
	HDC hDC=GetDC(NULL);
	HDC hMemDC=::CreateCompatibleDC(hDC);
	CRect rcSour=CRect(m_nMarginLeft,m_nMarginTop,m_nMarginLeft+m_nWidth,m_nMarginTop+m_nHeight);
	CRect rcInter;
	rcInter.IntersectRect(&rcSour,&rcRequire);

	//init dib info
	BITMAPINFOHEADER  BmpInfoHeader;
	BmpInfoHeader.biSize = sizeof(BITMAPINFOHEADER);
	
	BmpInfoHeader.biPlanes = 1;
	BmpInfoHeader.biBitCount = 8;
	BmpInfoHeader.biCompression = BI_RGB;
	BmpInfoHeader.biSizeImage = 0;
	BmpInfoHeader.biXPelsPerMeter = 0;
	BmpInfoHeader.biYPelsPerMeter = 0;
	BmpInfoHeader.biClrUsed = 0;
	BmpInfoHeader.biClrImportant = 0;
	BmpInfoHeader.biWidth = m_nWidth;
	BmpInfoHeader.biHeight = m_nHeight;	
	
	BITMAPINFO  *pBmInfo = (BITMAPINFO  *)GlobalAlloc(GPTR,sizeof(BITMAPINFOHEADER) + 256 * sizeof(RGBQUAD));
	memcpy(&pBmInfo->bmiHeader ,&BmpInfoHeader,sizeof(BITMAPINFOHEADER));
	memcpy(pBmInfo->bmiColors ,m_pPalette ,sizeof(RGBQUAD)*256);
	

	//render image
	HBITMAP hBmpRet = (HBITMAP)::CreateCompatibleBitmap(hDC,nWidth,nHeight);
	HBITMAP hOldBmp = (HBITMAP)::SelectObject(hMemDC,hBmpRet);

	//copy from CDC::FillSolidRect
	::SetBkColor(hMemDC, m_pGif->m_crBack);
	::ExtTextOut(hMemDC, 0, 0, ETO_OPAQUE, &CRect(0,0,nWidth,nHeight), NULL, 0, NULL);

	::StretchDIBits(hMemDC,
		rcInter.left-rcRequire.left ,rcInter.top-rcRequire.top,
		rcInter.Width(),rcInter.Height(),
		rcInter.left-m_nMarginLeft,m_nHeight-rcInter.Height()-(rcInter.top-m_nMarginTop),
		rcInter.Width(),rcInter.Height(),
		m_pImageData,pBmInfo,
		DIB_RGB_COLORS,SRCCOPY);//m_nHeight-height-ySour:调整原点坐标，位图数据的原点：bottom-up

	SelectObject(hMemDC,hOldBmp);
	DeleteDC(hMemDC);

	GlobalFree(pBmInfo);
	::ReleaseDC(NULL,hDC);
	return hBmpRet;
}

//-------------------------------------------------------------------
//	以透明方式显示本帧图象，如果没有定义透明色则以指定的光栅操作码显示
//	HDC hDC:目标DC
//	int xDest,int yDest:显示位置
//	int xSour,int ySour:图片左上角,包括了左边距及上边距
//	int nWidth,int nHeight:显示大小，,默认为－１，使用图片默认设置
//	DWORD dwRop:显示方式
//	return:TRUE-成功
//	remark:如果图象中定义了透明色，则dwRop无效
//-------------------------------------------------------------------
BOOL CGifFrame::ShowImage(HDC hDC,int xDest,int yDest,int xSour,int ySour,int nWidth,int nHeight,DWORD dwRop)
{
	if(m_idxTransColor <0) 
		return ShowImage2(hDC,xDest,yDest,xSour,ySour,nWidth,nHeight,dwRop);

	BITMAPINFOHEADER  BmpInfoHeader;
	BmpInfoHeader.biSize = sizeof(BITMAPINFOHEADER);
	
	BmpInfoHeader.biPlanes = 1;
	BmpInfoHeader.biBitCount = 8;
	BmpInfoHeader.biCompression = BI_RGB;
	BmpInfoHeader.biSizeImage = 0;
	BmpInfoHeader.biXPelsPerMeter = 0;
	BmpInfoHeader.biYPelsPerMeter = 0;
	BmpInfoHeader.biClrUsed = 0;
	BmpInfoHeader.biClrImportant = 0;
	BmpInfoHeader.biWidth = m_nWidth;
	BmpInfoHeader.biHeight = m_nHeight;	
	
	BITMAPINFO  *pBmInfo = (BITMAPINFO  *)GlobalAlloc(GPTR,sizeof(BITMAPINFOHEADER) + 256 * sizeof(RGBQUAD));
	if(!pBmInfo) return FALSE;

	memcpy(&pBmInfo->bmiHeader ,&BmpInfoHeader,sizeof(BITMAPINFOHEADER));
	memcpy(pBmInfo->bmiColors ,m_pPalette ,sizeof(RGBQUAD)*256);
	
	CRect rcSour=CRect(m_nMarginLeft,m_nMarginTop,m_nMarginLeft+m_nWidth,m_nMarginTop+m_nHeight);
	CRect rcRequire=CRect(xSour,ySour,xSour+nWidth,ySour+nHeight);
	CRect rcInter;
	rcInter.IntersectRect(&rcSour,&rcRequire);
	
	xDest+=rcInter.left-rcRequire.left;
	yDest+=rcInter.top-rcRequire.top;

	HDC hMemDC = ::CreateCompatibleDC(hDC);
	HBITMAP hBmp = (HBITMAP)::CreateCompatibleBitmap(hDC,rcInter.Width(),rcInter.Height());
	HBITMAP hOldBmp = (HBITMAP)::SelectObject(hMemDC,hBmp);
	::StretchDIBits(hMemDC,0 ,0,rcInter.Width(),rcInter.Height(),
		rcInter.left-m_nMarginLeft,m_nHeight-rcInter.Height()-(rcInter.top-m_nMarginTop),rcInter.Width(),rcInter.Height() ,m_pImageData,pBmInfo,
		DIB_RGB_COLORS,SRCCOPY);//m_nHeight-height-ySour:调整原点坐标，位图数据的原点：bottom-up

	BitBltEx(hDC,xDest ,yDest ,rcInter.Width(),rcInter.Height(),hMemDC,0,0 ,GetTransColor());
	
	::SelectObject(hMemDC,hOldBmp);
	::DeleteObject(hBmp);
	::DeleteDC(hMemDC);
	GlobalFree(pBmInfo);
	return TRUE;
}

//-------------------------------------------------------------------
//	显示本帧图象
//	HDC hDC:目标DC
//	int xDest,int yDest:显示位置
//	int xSour,int ySour:图片左上角,包括了左边距及上边距
//	int nWidth,int nHeight:显示大小
//	DWORD dwRop:显示方式
//	return:TRUE-成功
BOOL CGifFrame::ShowImage2(HDC hDC,int xDest,int yDest,int xSour,int ySour,int nWidth,int nHeight,DWORD dwRop)
{
	BITMAPINFOHEADER  BmpInfoHeader;
	BmpInfoHeader.biSize = sizeof(BITMAPINFOHEADER);
	
	BmpInfoHeader.biPlanes = 1;
	BmpInfoHeader.biBitCount = 8;
	BmpInfoHeader.biCompression = BI_RGB;
	BmpInfoHeader.biSizeImage = 0;
	BmpInfoHeader.biXPelsPerMeter = 0;
	BmpInfoHeader.biYPelsPerMeter = 0;
	BmpInfoHeader.biClrUsed = 0;
	BmpInfoHeader.biClrImportant = 0;
	BmpInfoHeader.biWidth = m_nWidth;
	BmpInfoHeader.biHeight = m_nHeight;	
	
	BITMAPINFO  *pBmInfo = (BITMAPINFO  *)GlobalAlloc(GPTR,sizeof(BITMAPINFOHEADER) + 256 * sizeof(RGBQUAD));
	if(!pBmInfo) return FALSE;

	memcpy(&pBmInfo->bmiHeader ,&BmpInfoHeader,sizeof(BITMAPINFOHEADER));
	memcpy(pBmInfo->bmiColors ,m_pPalette ,sizeof(RGBQUAD)*256);
	
	CRect rcSour=CRect(m_nMarginLeft,m_nMarginTop,m_nMarginLeft+m_nWidth,m_nMarginTop+m_nHeight);
	CRect rcRequire=CRect(xSour,ySour,xSour+nWidth,ySour+nHeight);
	CRect rcInter;
	rcInter.IntersectRect(&rcSour,&rcRequire);
	xDest+=rcInter.left-rcRequire.left;
	yDest+=rcInter.top-rcRequire.top;

	::StretchDIBits(hDC,xDest ,yDest,rcInter.Width(),rcInter.Height(),
		rcInter.left-m_nMarginLeft,m_nHeight-rcInter.Height()-(rcInter.top-m_nMarginTop),rcInter.Width(),rcInter.Height() ,m_pImageData,pBmInfo,
		DIB_RGB_COLORS,dwRop);//m_nHeight-height-ySour:调整原点坐标，位图数据的原点：bottom-up
	
	GlobalFree(pBmInfo);
	return TRUE;
}

BOOL CGifFrame::SaveBitmap(LPCTSTR pszFileName)
{
	BITMAPFILEHEADER BmpFileHeader;
	BmpFileHeader.bfType = 0x4d42;
	BmpFileHeader.bfSize = m_nWidth*m_nHeight ;
	BmpFileHeader.bfReserved1 = 0;
	BmpFileHeader.bfReserved2 = 0;
	BmpFileHeader.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + sizeof(RGBQUAD) * 256;

	BITMAPINFOHEADER BmpInfoHeader;
	BmpInfoHeader.biSize = sizeof(BITMAPINFOHEADER);
	BmpInfoHeader.biWidth = m_nWidth;
	BmpInfoHeader.biHeight = m_nHeight ;
	BmpInfoHeader.biPlanes = 1;
	BmpInfoHeader.biBitCount = 8;
	BmpInfoHeader.biCompression = BI_RGB;
	BmpInfoHeader.biSizeImage = 0;
	BmpInfoHeader.biXPelsPerMeter = 0;
	BmpInfoHeader.biYPelsPerMeter = 0;
	BmpInfoHeader.biClrUsed = 0;
	BmpInfoHeader.biClrImportant = 0;


	FILE *f=fopen(pszFileName,"wb");
	if(!f) return FALSE;

	fwrite(&BmpFileHeader,sizeof(BITMAPFILEHEADER),1,f);
	fwrite(&BmpInfoHeader,sizeof(BITMAPINFOHEADER),1,f);
	fwrite(m_pPalette,sizeof(RGBQUAD)*256,1,f);
	USHORT TrueWidth = WIDTHBYTES(m_nWidth * 8);
	fwrite(m_pImageData,TrueWidth*m_nHeight,1,f);
	fclose(f);
	return TRUE;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//	CGIF load file and decode to image array
////////////////////////////////////////////////////////////////////////////////////////////////////
CGIF::CGIF()
{
	m_crBack=0;
	m_nFrameCount=0;
	m_pFrames=NULL;
	m_byPalDeep=0;
	m_pPalGlobal=NULL;
}

CGIF::~CGIF()
{
	FreeData();
}

void CGIF::FreeData()
{
	m_byPalDeep=0;
	if(m_pPalGlobal){
		delete []m_pPalGlobal;
		m_pPalGlobal=NULL;
	}
	if(m_nFrameCount==0) return;
	for(UINT i=0;i<m_nFrameCount;i++)
		delete m_pFrames[i];
	delete []m_pFrames;
	m_nFrameCount=0;
	m_nWidth=m_nHeight=0;
	m_crBack=0;
}

BOOL CGIF::Load(LPBYTE pBuf,int nSize)
{
	CMemFile memfile;
	memfile.Attach(pBuf,nSize);
	return LoadFile(&memfile);
}

BOOL CGIF::Save(LPCTSTR pszFileName,int nFrame)
{
	CGifFrame *pFrame=GetFrame(nFrame);
	if(!pFrame) return FALSE;
	CFile f;
	if(!f.Open(pszFileName,CFile::modeCreate|CFile::modeReadWrite|CFile::typeBinary))
		return FALSE;
	//文件头
	GIFHEADER header;
	strncpy(header.Signature,"GIF89a",6);
	header.ScreenWidth=pFrame->m_nWidth;
	header.ScreenDepth=pFrame->m_nHeight;
	header.BackGroundColor=0;
	header.AspectRadio=0;
	header.GlobalFlagByte=0;//使用局部颜色表
	f.Write(&header,sizeof(GIFHEADER));

	//写入一个图象控制块
	BYTE byData=0x21;
	f.Write(&byData,1);
	byData=0xF9;
	f.Write(&byData,1);
	byData=4;
	f.Write(&byData,1);
	GRAPHCTRL ctrl;
	ctrl.DelayTime=0;
	ctrl.TranColorIndex=0;
	ctrl.PackedField=0;
	f.Write(&ctrl,4);
	byData=0;
	f.Write(&byData,1);

	//图象帧开始
	byData=0x2C;
	f.Write(&byData,1);
	//图象头
	IMAGEDATAINFO image;
	image.ImageLeft=0;
	image.ImageTop=0;
	image.ImageWidth=pFrame->m_nWidth;
	image.ImageHeight=pFrame->m_nHeight;
	image.LocalFlagByte=0x87;//保存为256色
	f.Write(&image,sizeof(IMAGEDATAINFO));
	//图象颜色表
	USHORT usPalSize=1<<(pFrame->m_byPalDeep+1);
	for(USHORT i=0;i<usPalSize;i++)
	{
		f.Write(&pFrame->m_pPalette[i].rgbRed,1);
		f.Write(&pFrame->m_pPalette[i].rgbGreen,1);
		f.Write(&pFrame->m_pPalette[i].rgbBlue,1);
	}
	if(usPalSize<256)//补充完全
	{
		BYTE byRgb[3]={0};
		for(USHORT i=usPalSize;i<256;i++) f.Write(byRgb,3);
	}

	//写图象数据
	BYTE byColorDeep=8;
	DWORD dwLineBytes=WIDTHBYTES(pFrame->m_nWidth*byColorDeep);
	DWORD dwRawLineBytes=pFrame->m_nWidth;//原始数据不以32bit对齐,还没有考虑8位以外的格式
	DWORD dwBufSize=pFrame->m_nHeight*dwRawLineBytes;
	BYTE *pbyBuf=new BYTE[dwBufSize];
	for(int iLine=0;iLine<pFrame->m_nHeight;iLine++)
	{//将图象数据调整为第一行在开始位置
		memcpy(pbyBuf+iLine*dwRawLineBytes,pFrame->m_pImageData+(pFrame->m_nHeight-1-iLine)*dwLineBytes,dwRawLineBytes);
	}

	CLZW_Encode lzw;
	CioObject iooInput;
	iooInput.Attach(pbyBuf,dwBufSize);
	CioGif	  iogOutput;
	iogOutput.Attach(&f,FALSE);

	iogOutput.WriteBeginFlag(byColorDeep);
	lzw.Encode(&iooInput,&iogOutput,byColorDeep);
	iogOutput.WriteEndFlag();
	delete []pbyBuf;
	//文件结束
	BYTE byEnd=0x3b;
	f.Write(&byEnd,1);
	f.Close();
	return TRUE;
}

BOOL CGIF::Load(LPCTSTR pszFileName)
{
	CFile file;
	if(!file.Open(pszFileName,CFile::modeRead|CFile::typeBinary))
		return FALSE;
	BOOL bRet=LoadFile(&file);
	file.Close();
	return bRet;
}

BOOL CGIF::LoadFile(CFile *file)
{
	FreeData();
	//得到有用信息	
	DECODESTATUS ds={0};
	AnalizeFileHeader(file,&ds);

	ds.m_CurTColorIndex = -1;
	ds.m_CurDelayTime = 0;

	std::vector<CGifFrame *> vctFrame;
	BYTE flag;
	while (1)
	{
		file->Read(&flag,1);
		if(flag==0x21)
		{//扩展块
			file->Read(&flag,1);
			switch(flag)
			{
			case 0xf9:GetGrphBlock(file,&ds);break;
			case 0x01:SkipTextBlock(file);break;
			case 0xff:SkipAppBlock(file);break;
			case 0xfe:SkipNoteBlock(file);break;
			default:ASSERT(0);break;
			}
		}
		else if (flag == 0x2c)
		{//數據塊
			GetImageBlock(&vctFrame,file,&ds);
		}
		else if(flag == 0x3b)
		{//文件结束标志
			break;
		}
	}
	m_nFrameCount=vctFrame.size();
	m_pFrames=new CGifFrame *[m_nFrameCount];
	std::vector<CGifFrame*>::iterator it;
	int i=0;
	for(it=vctFrame.begin();it<vctFrame.end();it++,i++)
	{
		m_pFrames[i]=*it;
	}
	return TRUE;
}

void  CGIF::AnalizeFileHeader(CFile *pf,PDECODESTATUS pds)
{
	// GIF File Header
	GIFHEADER gifHeader;//文件頭
	pf->Read(&gifHeader,sizeof(GIFHEADER));
	m_nWidth = gifHeader.ScreenWidth ;
	m_nHeight = gifHeader.ScreenDepth ;
	
	//得到調色板大小
	if (gifHeader.GlobalFlagByte & 0x80) //有通用調色板數據
	{ 
		m_byPalDeep=gifHeader.GlobalFlagByte & 0x07;
		USHORT usPalSize = 1<<(m_byPalDeep+1);
		m_pPalGlobal=new RGBQUAD[usPalSize];
		//read pal data and save it to BitMap pal
		BYTE byColor[3];
		for (UINT j = 0; j < usPalSize ; j ++)
		{
			pf->Read(byColor,3);
			m_pPalGlobal[j].rgbRed       =byColor[0];
			m_pPalGlobal[j].rgbGreen     =byColor[1];
			m_pPalGlobal[j].rgbBlue      =byColor[2];
			m_pPalGlobal[j].rgbReserved  = 0;
		}
		m_crBack=RGB(m_pPalGlobal[gifHeader.BackGroundColor].rgbRed,
			m_pPalGlobal[gifHeader.BackGroundColor].rgbGreen,
			m_pPalGlobal[gifHeader.BackGroundColor].rgbBlue);
	}
}
//處理圖形控制補充區
void CGIF::GetGrphBlock(CFile *pf,PDECODESTATUS pds)
{
	BYTE flag;
	pf->Read(&flag,1);
	ASSERT(flag==4);
	GRAPHCTRL GraphCtrl;
	pf->Read(&GraphCtrl,4);
	pf->Seek(1,CFile::current);
	pds->m_CurTColorIndex = -1;
	pds->m_CurDelayTime = 0;
	if (GraphCtrl.PackedField & 1)
	{
		pds->m_CurTColorIndex = GraphCtrl.TranColorIndex ;
	}
	pds->m_CurDelayTime = GraphCtrl.DelayTime ;
}

void  CGIF::SkipBlock(CFile *pf)
{
	BYTE bySize=0;
	pf->Read(&bySize,1);
	pf->Seek(bySize,CFile::current);
}

//處理文字補充區
void CGIF::SkipTextBlock(CFile *pf)
{
	BYTE flag;
	pf->Read(&flag,1);
	ASSERT(flag==12);//head size
	pf->Seek(flag,CFile::current);
	SkipBlock(pf);
	pf->Read(&flag,1);
	ASSERT(flag==0);//end flag
}

//處理應用補充區
void CGIF::SkipAppBlock(CFile *pf)
{
	BYTE flag;
	pf->Read(&flag,1);
	ASSERT(flag==11);//head size
	pf->Seek(flag,CFile::current);
	SkipBlock(pf);
	pf->Read(&flag,1);
	ASSERT(flag==0);//end flag
}

//處理附註補充區
void CGIF::SkipNoteBlock(CFile *pf)
{
	BYTE flag;
	SkipBlock(pf);
	pf->Read(&flag,1);
	ASSERT(flag==0);//end flag
}

void CGIF::GetImageBlock(void *pImgList,CFile *pf,PDECODESTATUS pds)
{
	std::vector<CGifFrame *> *pVctImgList=(std::vector<CGifFrame *>*) pImgList;
	IMAGEDATAINFO ImageData;
	pf->Read(&ImageData,sizeof(IMAGEDATAINFO));

	UINT PalSize = 0;
	pds->m_CurSaveMode = 0;
	pds->m_CurX = ImageData.ImageLeft;
	pds->m_CurY = ImageData.ImageTop ;
	pds->m_CurWidth = ImageData.ImageWidth ;
	pds->m_CurHeight = ImageData.ImageHeight;

	RGBQUAD *pPalLocal=m_pPalGlobal;
	BYTE byPalDeep=m_byPalDeep;

	if (ImageData.LocalFlagByte  & 0x80) //("有局部調色板數據!");
	{ 
		byPalDeep=(ImageData.LocalFlagByte&0x07);
		UINT uLocalPalSize= 1<<(byPalDeep+1);
		pPalLocal=new RGBQUAD[uLocalPalSize];
		BYTE byColor[3];
		for (UINT j = 0; j < uLocalPalSize ; j ++)
		{
			pf->Read(byColor,3);
			pPalLocal[j].rgbRed       =byColor[0];
			pPalLocal[j].rgbGreen     =byColor[1];
			pPalLocal[j].rgbBlue      =byColor[2];
			pPalLocal[j].rgbReserved  = 0;
		}
	}	
	else if (ImageData.LocalFlagByte  & 0x40)
	{
		pds->m_CurSaveMode = 1;
	}

	UINT uBufLen=pds->m_CurWidth*pds->m_CurHeight;
	LPBYTE pImageData= (LPBYTE)GlobalAlloc (GPTR,uBufLen);

	CLZW_Decode lzw;
	CioGif	iogInput;
	CioObject iooOutput;
	BYTE byInitBits=0;
	pf->Read(&byInitBits,1);
	iogInput.Attach(pf,TRUE);
	iooOutput.Attach(pImageData,uBufLen);
	lzw.Decode(&iogInput,&iooOutput,byInitBits);

	CGifFrame *pBmpImage=GetDibData(pImageData,pds);
	pBmpImage->m_bCommonPal= !(ImageData.LocalFlagByte & 0x80);
	pBmpImage->m_pPalette=pPalLocal;
	pBmpImage->m_byPalDeep= byPalDeep;

	pVctImgList->push_back(pBmpImage);

	GlobalFree(pImageData);
}

//------------------------------------------------------------------------
//	将GIF经LZW解码后的图象数据组织为DIB数据
//------------------------------------------------------------------------
CGifFrame * CGIF::GetDibData(LPBYTE SrcData,PDECODESTATUS pds)
{
	
	LPBYTE pSrcData = SrcData;
	
	UINT j,k,Step;
	if (pds->m_CurSaveMode )//交叉存處
	{
		LPBYTE p2 = (LPBYTE)GlobalAlloc(GPTR,pds->m_CurWidth*pds->m_CurHeight);
		LPBYTE pTmp = p2;
		
		k = 0; Step = 8;
		for (j = 0; j < pds->m_CurHeight  ; j ++)
		{
			memcpy(pTmp+k*pds->m_CurWidth,pSrcData+j*pds->m_CurWidth,pds->m_CurWidth);
			k += Step;
			if (k >= pds->m_CurHeight) break;
		}
		j++;//每行中存了一個越界值
		k = 4; Step = 8;
		for (; j < pds->m_CurHeight  ; j ++)
		{
			memcpy(pTmp+k*pds->m_CurWidth,pSrcData+j*pds->m_CurWidth,pds->m_CurWidth);
			k += Step;
			if (k >= pds->m_CurHeight) break;
		}
		j++;
		k = 2; Step = 4;
		for (; j < pds->m_CurHeight  ; j ++)
		{
			memcpy(pTmp+k*pds->m_CurWidth,pSrcData+j*pds->m_CurWidth,pds->m_CurWidth);
			k += Step;
			if (k >= pds->m_CurHeight) break;
		}
		j++;
		k = 1; Step = 2;
		for (; j < pds->m_CurHeight  ; j ++)
		{
			memcpy(pTmp+k*pds->m_CurWidth,pSrcData+j*pds->m_CurWidth,pds->m_CurWidth);
			k += Step;
			if (k >= pds->m_CurHeight) break;
		}
		
		pSrcData = p2;
	}
	CGifFrame * pBmpImage = new CGifFrame(this);
	if(!pBmpImage) return NULL;

	pBmpImage->m_idxTransColor = pds->m_CurTColorIndex;
	pBmpImage->m_nDelayTime = pds->m_CurDelayTime;
	pBmpImage->m_nMarginLeft = pds->m_CurX;
	pBmpImage->m_nMarginTop = pds->m_CurY;
	pBmpImage->m_nWidth = pds->m_CurWidth ;
	pBmpImage->m_nHeight = pds->m_CurHeight ;

	USHORT TrueWidth = WIDTHBYTES(pds->m_CurWidth * 8);//全轉成256顯示圖片
	pBmpImage->m_pImageData = (LPBYTE)GlobalAlloc(GPTR,TrueWidth*pds->m_CurHeight );

	LPBYTE p0,p1;
	p0 = pSrcData;
    p1 = pBmpImage->m_pImageData;

	for (j = 0; j < pds->m_CurHeight  ; j ++)//圖形翻轉
	{
		memcpy(p1+j*TrueWidth,p0+(pds->m_CurHeight-1-j)*pds->m_CurWidth,pds->m_CurWidth);
	}
	
	if (pds->m_CurSaveMode) //交叉存
	{
		GlobalFree(pSrcData);
	}
	return pBmpImage;
}

//**----------------------------------------------------------
//** STEP 2: Create the Mask and dump it into a CBitmap Object
//**----------------------------------------------------------
HBITMAP PrepareBMPMask(HDC hdcSrc,				//the dc who contains the source image
					   int nBmpWidth,int nBmpHeight,//image size
					   COLORREF clrpTransColor, // Set 0 if unknown
					   int iTransPixelX,      // = 0
					   int iTransPixelY       // = 0
					   )
{
	HBITMAP hBmpMask=::CreateBitmap(nBmpWidth,nBmpHeight, 1, 1, NULL);//attention!!!
	// We will need two DCs to work with. One to hold the Image
	// (the source), and one to hold the mask (destination).
	// When blitting onto a monochrome bitmap from a color, pixels
	// in the source color bitmap that are equal to the background
	// color are blitted as white. All the remaining pixels are
	// blitted as black.

	HDC  hdcDst=CreateCompatibleDC(NULL);

	// Load the bitmaps into memory DC
	HBITMAP hbmDstT = (HBITMAP) SelectObject(hdcDst,hBmpMask);

	// Dynamically get the transparent color
	COLORREF clrTrans= clrpTransColor != 0 ? clrpTransColor : GetPixel(hdcSrc,iTransPixelX, iTransPixelY);
	// Change the background to trans color
	COLORREF clrSaveBk  = SetBkColor(hdcSrc,clrTrans);

	// This call sets up the mask bitmap.
	BitBlt(hdcDst,0,0,nBmpWidth, nBmpHeight, hdcSrc,0,0,SRCCOPY);

	// Now, we need to paint onto the original image, making
	// sure that the "transparent" area is set to black. What
	// we do is AND the monochrome image onto the color Image
	// first. When blitting from mono to color, the monochrome
	// pixel is first transformed as follows:
	// if  1 (black) it is mapped to the color set by SetTextColor().
	// if  0 (white) is is mapped to the color set by SetBkColor().
	// Only then is the raster operation performed.

	COLORREF clrSaveSrcText = SetTextColor(hdcSrc,RGB(255,255,255));
	SetBkColor(hdcSrc,RGB(0,0,0));

	BitBlt(hdcSrc,0,0,nBmpWidth, nBmpHeight, hdcDst,0,0,SRCAND);

	// Clean up by deselecting any objects, and delete the DC's.
	SetTextColor(hdcSrc,clrSaveSrcText);

	SetBkColor(hdcSrc,clrSaveBk);
	SelectObject(hdcDst,hbmDstT);

	DeleteDC(hdcDst);
	return hBmpMask;
}

BOOL BitBltEx(
  HDC hdcDest, // handle to destination device context
  int nXDest,  // x-coordinate of destination rectangle's upper-left 
               // corner
  int nYDest,  // y-coordinate of destination rectangle's upper-left 
               // corner
  int nWidth,  // width of destination rectangle
  int nHeight, // height of destination rectangle
  HDC hdcSrc,  // handle to source device context
  int nXSrc,   // x-coordinate of source rectangle's upper-left 
               // corner
  int nYSrc,   // y-coordinate of source rectangle's upper-left 
  COLORREF cTransparentColor      // corner
   // raster operation code
)
{
	HBITMAP hBmpMask=PrepareBMPMask(hdcSrc,nWidth,nHeight,cTransparentColor,0,0);
	HDC hMemDC=CreateCompatibleDC(hdcSrc);
	HBITMAP hbmpTmp=(HBITMAP)SelectObject(hMemDC,hBmpMask);
	BitBlt(hdcDest,nXDest,nYDest,nWidth,nHeight,hMemDC,nXSrc,nYSrc,SRCAND);
	SelectObject(hMemDC,hbmpTmp);
	DeleteObject(hBmpMask);
	BitBlt(hdcDest,nXDest,nYDest,nWidth,nHeight,hdcSrc,nXSrc,nYSrc,SRCPAINT);
	DeleteDC(hMemDC);
	return TRUE;
}



CGifFrame* CGIF::GetFrame(UINT iFrame)
{
	if(iFrame>=m_nFrameCount) return NULL;
	return m_pFrames[iFrame];
}


