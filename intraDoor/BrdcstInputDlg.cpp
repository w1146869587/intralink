// BrdcstInputDlg.cpp : implementation file
//

#include "stdafx.h"
#include "intradoor.h"
#include "BrdcstInputDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBrdcstInputDlg dialog


CBrdcstInputDlg::CBrdcstInputDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CBrdcstInputDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CBrdcstInputDlg)
	m_strContent = _T("");
	//}}AFX_DATA_INIT
}


void CBrdcstInputDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CBrdcstInputDlg)
	DDX_Text(pDX, IDC_CONTENT, m_strContent);
	DDV_MaxChars(pDX, m_strContent, 400);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CBrdcstInputDlg, CDialog)
	//{{AFX_MSG_MAP(CBrdcstInputDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBrdcstInputDlg message handlers
