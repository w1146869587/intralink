// ChatDlg.cpp : implementation file
//

#include "stdafx.h"
#include "intradoor.h"
#include "ChatDlg.h"

#include "intraDoorDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CChatDlg dialog


CChatDlg::CChatDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CChatDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CChatDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_pMainDlg=NULL;
}


void CChatDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CChatDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CChatDlg, CDialog)
	//{{AFX_MSG_MAP(CChatDlg)
	ON_BN_CLICKED(IDC_SEND, OnSend)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_SAVE, OnSave)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChatDlg message handlers

void CChatDlg::OnSend() 
{
	CString str;
	GetDlgItemText(IDC_SAY,str);
	if(str.GetLength()>400)
	{
		MessageBox("超过消息允许长度","错误",MB_ICONSTOP|MB_OK);
		return;
	}
	theApp.SendMsg(m_dwIP,PORT_DOOR,MAIN_CHAT,0,(void*)(LPCTSTR)str,str.GetLength()+1);

	AddSent(theApp.m_UserInfo.szName,str);
	SetDlgItemText(IDC_SAY,"");
}

BOOL CChatDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	this->ModifyStyleEx(0,WS_EX_APPWINDOW,0);
	CString str;
	str.Format("与 %s 聊天...",m_strMateName);
	SetWindowText(str);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CChatDlg::AddSent(CString strAddressor, CString strSent)
{
	CEdit *pEdit=(CEdit*)GetDlgItem(IDC_HISTORY);
	pEdit->SetSel(-1);
	CString str;
	CTime time=CTime::GetCurrentTime();
	
	str.Format("%s 说: %d-%d-%d %d:%d\r\n%s\r\n",strAddressor,time.GetYear(),time.GetMonth(),time.GetDay(),time.GetHour(),time.GetMinute(),strSent);
	pEdit->ReplaceSel(str);
	pEdit->LineScroll(pEdit->GetLineCount());
}

void CChatDlg::PostNcDestroy() 
{
	CDialog::PostNcDestroy();
	delete this;
}

void CChatDlg::OnCancel()
{
	ASSERT(m_pMainDlg);
	POSITION pos=m_pMainDlg->m_lstChatDlg.GetHeadPosition();
	while(pos)
	{
		POSITION posBk=pos;
		CChatDlg *pDlg=m_pMainDlg->m_lstChatDlg.GetNext(pos);
		if(pDlg->m_dwIP==m_dwIP)
		{
			m_pMainDlg->m_lstChatDlg.RemoveAt(posBk);
			break;
		}
	}
	DestroyWindow();

}

void CChatDlg::OnDestroy() 
{
	HICON hIcon=GetIcon(FALSE);
	::DestroyIcon(hIcon);
	CDialog::OnDestroy();	
}

void CChatDlg::OnSave() 
{
	CFileDialog save(FALSE,"txt","聊天记录",0,"文本文件(*.txt)|*.txt||");
	if(save.DoModal()==IDOK)
	{
		CEdit *pEdit=(CEdit*)GetDlgItem(IDC_HISTORY);
		int nSize=pEdit->GetWindowTextLength();
		char *pBuf=(char*)malloc(nSize+1);
		GetDlgItemText(IDC_HISTORY,pBuf,nSize+1);

		CFile file;
		if(file.Open(save.GetPathName(),CFile::modeWrite|CFile::modeCreate|CFile::typeBinary))
		{
			file.Write(pBuf,nSize);
			file.Close();
		}
		free(pBuf);
	}
}
