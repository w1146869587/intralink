#if !defined(AFX_GAMESETUPDIRDLG_H__7415CF75_C28C_4C40_8B12_7E41E6B9E612__INCLUDED_)
#define AFX_GAMESETUPDIRDLG_H__7415CF75_C28C_4C40_8B12_7E41E6B9E612__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GameSetupDirDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CGameSetupDirDlg dialog

class CGameSetupDirDlg : public CDialog
{
// Construction
public:
	CGameSetupDirDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CGameSetupDirDlg)
	enum { IDD = IDD_GAMESETUPDIR };
	CString	m_strDir;
	CString	m_strTip;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGameSetupDirDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CGameSetupDirDlg)
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GAMESETUPDIRDLG_H__7415CF75_C28C_4C40_8B12_7E41E6B9E612__INCLUDED_)
