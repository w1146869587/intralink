// FindDlg.cpp : implementation file
//

#include "stdafx.h"
#include "intradoor.h"
#include "FindDlg.h"
#include "intradoorDlg.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFindDlg dialog


CFindDlg::CFindDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFindDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CFindDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_nBegin=-1;
}


void CFindDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFindDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFindDlg, CDialog)
	//{{AFX_MSG_MAP(CFindDlg)
	ON_BN_CLICKED(IDC_FIND, OnFind)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFindDlg message handlers

void CFindDlg::OnFind() 
{
	CIntraDoorDlg 	* pMainDlg=(CIntraDoorDlg *) GetOwner();
	int nCount=pMainDlg->m_lstUser.GetItemCount();
	char szName1[UIS_NAME+1],szName2[UIS_NAME+1];
	GetDlgItemText(IDC_NAME,szName1,UIS_NAME+1);
	for(int i=1;i<nCount;i++)
	{
		int iItem=(i+m_nBegin)%nCount;
		pMainDlg->m_lstUser.GetItemText(iItem,0,szName2,UIS_NAME+1);
		if(strstr(szName1,szName2))
		{
			pMainDlg->m_lstUser.SetSelectionMark(iItem);
			pMainDlg->SetActiveWindow();
			m_nBegin=iItem;
			return;
		}
	}
	MessageBox("û���ҵ�");
}

void CFindDlg::PostNcDestroy() 
{
	CDialog::PostNcDestroy();
	delete this;
}
