// FileTransDlg.cpp : implementation file
//

#include "stdafx.h"
#include "intradoor.h"
#include "FileTransDlg.h"
#include "IntraDoorDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define FLAG_FILETRANS			0xFFEEFFEE

#define FLAG_FILE		1
#define FLAG_DIR_DOWN	2
#define FLAG_DIR_UP		3
#define SIZE_BLOCK		(1<<20)

#define FTPM_FINISH		101		//数据接收完成
#define FTPM_CANCEL		102		//传输中止
#define FTPM_SOCKHALT	103		//Socket连接中断
#define FTPM_ERROR		-1		//通讯错误

#define PANEL_CONFIRM	0		//下载确认表
#define PANEL_DOWNLOAD	1		//下载任务表
#define PANEL_UPLOAD	2		//上传任务表

UINT g_uPanelConfirm[]=
{
	IDC_LIST_TASK,
	IDC_CONFIRM_RECIEVE,
	IDC_CONFIRM_REJECT,
};

UINT g_uPanelDownload[]=
{
	IDC_LIST_DOWNLOAD,
	IDC_DOWNLOAD_CANCELTASK,
	IDC_DOWNLOAD_DELETETASK,
	IDC_DOWNLOAD_OPENDIR,
};

UINT g_uPanelUpload[]=
{
	IDC_LIST_UPLOAD,
	IDC_UPLOAD_CANCELTASK,
	IDC_UPLOAD_DELETETASK,
	IDC_UPLOAD_AUTODEL,
};

/////////////////////////////////////////////////////////////////////////////
// CFileTransDlg dialog

#ifndef ListView_SetCheckState
   #define ListView_SetCheckState(hwndLV, i, fCheck) \
      ListView_SetItemState(hwndLV, i, \
      INDEXTOSTATEIMAGEMASK((fCheck)+1), LVIS_STATEIMAGEMASK)
#endif


CFileTransDlg::CFileTransDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFileTransDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CFileTransDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_sockSvr=0;
}


void CFileTransDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFileTransDlg)
	DDX_Control(pDX, IDC_LIST_TASK, m_lstTask);
	DDX_Control(pDX, IDC_TAB_TASKTYPE, m_tabTaskType);
	DDX_Control(pDX, IDC_LIST_UPLOAD, m_lstUpload);
	DDX_Control(pDX, IDC_LIST_DOWNLOAD, m_lstDownload);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFileTransDlg, CDialog)
	//{{AFX_MSG_MAP(CFileTransDlg)
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_TASKTYPE, OnSelchangeTabTasktype)
	ON_WM_DESTROY()
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_DOWNLOAD, OnDblclkListDownload)
	ON_BN_CLICKED(IDC_DOWNLOAD_CANCELTASK, OnDownloadCanceltask)
	ON_BN_CLICKED(IDC_DOWNLOAD_DELETETASK, OnDownloadDeletetask)
	ON_BN_CLICKED(IDC_DOWNLOAD_OPENDIR, OnDownloadOpendir)
	ON_BN_CLICKED(IDC_UPLOAD_CANCELTASK, OnUploadCanceltask)
	ON_BN_CLICKED(IDC_UPLOAD_DELETETASK, OnUploadDeletetask)
	ON_BN_CLICKED(IDC_CONFIRM_RECIEVE, OnConfirmRecieve)
	ON_BN_CLICKED(IDC_CONFIRM_REJECT, OnConfirmReject)
	//}}AFX_MSG_MAP
	ON_MESSAGE(UM_SOCKETMSG,OnSockMsg)
	ON_MESSAGE(UM_FTP_DOWNLOAD,OnFtpDownload)
	ON_MESSAGE(UM_FTP_UPLOAD,OnFtpUpload)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFileTransDlg message handlers

BOOL CFileTransDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_tabTaskType.InsertItem(PANEL_CONFIRM,"下载消息");
	m_tabTaskType.InsertItem(PANEL_DOWNLOAD,"下载任务");
	m_tabTaskType.InsertItem(PANEL_UPLOAD,"上传任务");
	RECT rcAdjust={0};
	m_tabTaskType.AdjustRect(FALSE,&rcAdjust);
	RECT rcTab;
	m_tabTaskType.GetWindowRect(&rcTab);
	ScreenToClient(&rcTab);
	rcTab.left+=rcAdjust.left;
	rcTab.top+=rcAdjust.top+3;
	rcTab.right+=rcAdjust.right;
	rcTab.bottom+=rcAdjust.bottom;

	// TODO: Add extra initialization here
	ListView_SetExtendedListViewStyle(m_lstTask.m_hWnd,LVS_EX_CHECKBOXES);
	m_lstTask.MoveWindow(&rcTab,FALSE);
	m_lstTask.BringWindowToTop();
	m_lstTask.InsertColumn(0,"来自",0,200);
	m_lstTask.InsertColumn(1,"文件名",0,200);
	m_lstTask.InsertColumn(2,"大小",0,200);
	m_lstTask.InsertColumn(3,"文件数",0,50);

	m_lstDownload.MoveWindow(&rcTab,FALSE);
	m_lstDownload.BringWindowToTop();
	m_lstDownload.InsertColumn(0,"进度",0,80);
	m_lstDownload.InsertColumn(1,"名称",0,200);
	m_lstDownload.InsertColumn(2,"来自",0,200);
	m_lstDownload.InsertColumn(3,"大小",0,50);
	m_lstDownload.InsertColumn(4,"耗时",0,80);

	m_lstUpload.MoveWindow(&rcTab,FALSE);
	m_lstUpload.BringWindowToTop();
	m_lstUpload.InsertColumn(0,"进度",0,80);
	m_lstUpload.InsertColumn(1,"名称",0,200);
	m_lstUpload.InsertColumn(2,"发往",0,200);
	m_lstUpload.InsertColumn(3,"大小",0,50);
	m_lstUpload.InsertColumn(4,"耗时",0,80);

	CBitmap	bmp;
	bmp.LoadBitmap(IDB_FTSTATE);
	m_ilState.Create(16,16,ILC_COLORDDB|ILC_MASK,0,0);
	m_ilState.Add(&bmp,RGB(255,0,255));
	m_lstDownload.SetImageList(&m_ilState,LVSIL_SMALL);
	m_lstUpload.SetImageList(&m_ilState,LVSIL_SMALL);

	ShowPanel(g_uPanelUpload,sizeof(g_uPanelUpload)/sizeof(UINT),FALSE);
	ShowPanel(g_uPanelDownload,sizeof(g_uPanelDownload)/sizeof(UINT),FALSE);
	ShowPanel(g_uPanelConfirm,sizeof(g_uPanelConfirm)/sizeof(UINT),TRUE);
	m_nCurPanel=PANEL_CONFIRM;
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CFileTransDlg::ListenStart()
{
	ASSERT(m_sockSvr==0);
	m_sockSvr=socket(AF_INET,SOCK_STREAM,0);
	SOCKADDR_IN addrSvr;
	addrSvr.sin_addr.S_un.S_addr=htonl(INADDR_ANY);
	addrSvr.sin_family=AF_INET;
	addrSvr.sin_port=htons(PORT_DOOR);
	bind(m_sockSvr,(SOCKADDR*)&addrSvr,sizeof(SOCKADDR));
	listen(m_sockSvr,SOMAXCONN);
	::WSAAsyncSelect(m_sockSvr,m_hWnd,UM_SOCKETMSG,FD_ACCEPT);
}

void CFileTransDlg::ListenEnd()
{
	closesocket(m_sockSvr);
	m_sockSvr=0;
}

LRESULT CFileTransDlg::OnSockMsg(WPARAM wParam, LPARAM lParam)
{
	switch(LOWORD(lParam))
	{
	case FD_ACCEPT:
		{
			ASSERT(m_sockSvr==wParam);
			SOCKADDR_IN addrFrom={0};
			int nLen=sizeof(SOCKADDR);
			SOCKET sockClient=accept(m_sockSvr,(SOCKADDR*)&addrFrom,&nLen);
			DWORD dwIP;
			memcpy(&dwIP,&addrFrom.sin_addr.S_un.S_un_b,4);
			//设置2M的缓冲区
			int nBufSize=SIZE_BLOCK*2;
			setsockopt(sockClient,SOL_SOCKET,SO_RCVBUF,(char *)&nBufSize,sizeof(int));
			WSAAsyncSelect(sockClient,m_hWnd,UM_SOCKETMSG,FD_CLOSE|FD_READ);/*FD_CLOSE|FD_READ*/
			//接收一个4字节握手信号
			DWORD	dwFlag=0;
			int nRecv=theApp.Recv(sockClient,(char*)&dwFlag,4,1000);
			if(FLAG_FILETRANS!=dwFlag)
			{
				closesocket(sockClient);
			}else
			{
				//接收文件名,是自己同意接收时发送到上传方的文件名
				char szPath[MAX_PATH+1];
				int nRead=theApp.Recv(sockClient,szPath,MAX_PATH+1,10000);
				TRACE("\nFD_ACCEPT,nRead=%d",nRead);
				if(nRead!=MAX_PATH+1) 
					closesocket(sockClient);
				else
					StartDownload(sockClient,szPath,dwIP);
			}
		}
		break;
	case FD_CLOSE:
		{
			TRACE("\nFD_CLOSE");
			int iTask=Sock2DownloadTask((SOCKET)wParam);
			if(iTask!=-1)
			{//socket close
				PostMessage(UM_FTP_DOWNLOAD,FTPM_SOCKHALT,wParam);
			}else 
			{
				iTask=Sock2UploadTask((SOCKET)wParam);
				if(iTask != -1)
				{
					Sleep(100);		//等待数据上传线程退出。
					BOOL bFinish=FALSE;
					PFTPARAM_UPLOAD pParam=(PFTPARAM_UPLOAD)m_lstUpload.GetItemData(iTask);
					EnterCriticalSection(&pParam->syncObj.cs);
					bFinish=pParam->syncObj.state==SS_FINISH;
					LeaveCriticalSection(&pParam->syncObj.cs);
					if(bFinish)
					{//文件传输完成
						PostMessage(UM_FTP_UPLOAD,FTPM_FINISH,wParam);
					}else
					{
						PostMessage(UM_FTP_UPLOAD,FTPM_SOCKHALT,wParam);
					}
				}
			}
		}
		break;
	case FD_READ:
		{
			TRACE("\nFD_READ");
			int iTask=Sock2DownloadTask((SOCKET)wParam);
			if(iTask!=-1)
			{//下载数据到达信息
				PFTPARAM_DOWNLOAD pParam=(PFTPARAM_DOWNLOAD)m_lstDownload.GetItemData(iTask);
				EnterCriticalSection(&pParam->syncObj.cs);
				SetEvent(pParam->syncObj.hEvent);
				LeaveCriticalSection(&pParam->syncObj.cs);
			}
		}
		break;
	case FD_WRITE:
		{//一个Sock可以写入新数据
			Sleep(0);//切换到文件上传线程
			TRACE("\nFD_WRITE");
			int iTask=Sock2UploadTask((SOCKET)wParam);
			if(iTask!=-1)
			{
				PFTPARAM_UPLOAD pParam=(PFTPARAM_UPLOAD)m_lstUpload.GetItemData(iTask);
				EnterCriticalSection(&pParam->syncObj.cs);
				if(pParam->syncObj.state==SS_WAITTING)  SetEvent(pParam->syncObj.hEvent);
				LeaveCriticalSection(&pParam->syncObj.cs);
			}else
			{
				ASSERT(FALSE);
			}
		}
		break;
	}
	return 0;
}


int SockSend(SOCKET s,char *pszBuf,int nLen,PSYNCOBJ pSyncObj)
{
	int nSent=0;
	while(nSent<nLen && pSyncObj->bCancel==FALSE)
	{
		int nPut=send(s,pszBuf+nSent,nLen-nSent,0);
		if(nPut==-1)
		{
			if(WSAEWOULDBLOCK==WSAGetLastError())
			{
				TRACE("\nSend WSAEWOULDBLOCK");
				//等待下一次写就绪
				EnterCriticalSection(&pSyncObj->cs);
				pSyncObj->state=SS_WAITTING;
				ResetEvent(pSyncObj->hEvent);
				LeaveCriticalSection(&pSyncObj->cs);
				int nRet=WaitForSingleObject(pSyncObj->hEvent,200);
				pSyncObj->state=SS_WORKING;
				TRACE("\nSend Wait Ret=%d",nRet);
				continue;
			}else
			{
				TRACE("\nSend data error");
				break;
			}
		}
		nSent+=nPut;
		TRACE("\nSend Package=%d",nPut);
	}
	return nSent;
}

int SockRecv(SOCKET s,char *pszBuf,int nLen, PSYNCOBJ pSyncObj)
{
	int nReaded=0;
	while(nReaded<nLen && pSyncObj->bCancel==FALSE)
	{
		//确保接收数据线程在读数据时，主线程不处理FD_READ消息
		int nGet=recv(s,pszBuf+nReaded,nLen-nReaded,0);
		if(nGet==-1)
		{
			if(WSAEWOULDBLOCK==WSAGetLastError())
			{
				TRACE("\nRecv WSAEWOULDBLOCK");
				EnterCriticalSection(&pSyncObj->cs);
				pSyncObj->state=SS_WAITTING;
				ResetEvent(pSyncObj->hEvent);
				LeaveCriticalSection(&pSyncObj->cs);
				int nRet=WaitForSingleObject(pSyncObj->hEvent,5);
				pSyncObj->state=SS_WORKING;
				TRACE("\nSend Wait Ret=%d",nRet);
				continue;
			}else
			{
				TRACE("\nRecv data error");
				break;
			}
		}
		nReaded+=nGet;
		TRACE("\nRecv Package=%d",nGet);
	}
	return nReaded;
}

UINT FuncDirDownload(LPVOID lpData)
{
	int nRet=0;
	PFTPARAM_DOWNLOAD pParam=(PFTPARAM_DOWNLOAD)lpData;
	int nFiles=0,nDirs=0;
	int nReaded=SockRecv(pParam->s,(char*)&nFiles,4,&pParam->syncObj);
	if(nReaded!=4)
	{
		pParam->pDlg->PostMessage(UM_FTP_DOWNLOAD,FTPM_ERROR,(LPARAM)pParam->s);
		return 1;
	}
	nReaded=SockRecv(pParam->s,(char*)&nDirs,4,&pParam->syncObj);
	if(nReaded!=4)
	{
		pParam->pDlg->PostMessage(UM_FTP_DOWNLOAD,FTPM_ERROR,(LPARAM)pParam->s);
		return 1;
	}
	DWORD dwTotalSize=0,dwTotalRecv=0;
	nReaded=SockRecv(pParam->s,(char*)&dwTotalSize,4,&pParam->syncObj);
	if(nReaded!=4)
	{
		pParam->pDlg->PostMessage(UM_FTP_DOWNLOAD,FTPM_ERROR,(LPARAM)pParam->s);
		return 1;
	}
	
	char szWorkPath[MAX_PATH+1],szFileName[MAX_PATH+1];
	strcpy(szWorkPath,pParam->szPath);
	CreateDirectory(pParam->szPath,NULL);
	char cFlag=0;
	int iFile=0,iDir=1;
	char *pbyBuf=(char*)malloc(SIZE_BLOCK);
	while((iFile<nFiles || iDir<nDirs) && pParam->syncObj.bCancel==FALSE)
	{
		cFlag=0;
		SockRecv(pParam->s,&cFlag,1,&pParam->syncObj);
		if(cFlag==FLAG_FILE)
		{
			nReaded=SockRecv(pParam->s,szFileName,MAX_PATH+1,&pParam->syncObj);
			if(nReaded!=MAX_PATH+1)
			{
				nRet=1;
				break;
			}
			char szFullPath[MAX_PATH+1];
			sprintf(szFullPath,"%s\\%s",szWorkPath,szFileName);
			FILEINFO fi={0};
			nReaded=SockRecv(pParam->s,(char*)&fi,sizeof(FILEINFO),&pParam->syncObj);
			if(nReaded!=sizeof(FILEINFO))
			{
				nRet=1;
				break;
			}

			FILE *f=fopen(szFullPath,"wb");
			if(!f) {
				nRet=1;
				break;
			}
			DWORD dwRecv=0;
			DWORD dwSize=fi.m_size;
			while(dwRecv<dwSize && pParam->syncObj.bCancel==FALSE)
			{
				DWORD dwFatch=SIZE_BLOCK;
				if(dwFatch>(dwSize-dwRecv)) dwFatch=(dwSize-dwRecv);
				int nGet=SockRecv(pParam->s,pbyBuf,dwFatch,&pParam->syncObj);
				fwrite(pbyBuf,1,nGet,f);
				dwRecv+=nGet;
				dwTotalRecv+=nGet;
				__int64 llProg=dwTotalRecv;
				llProg=llProg*100/dwTotalSize;
				pParam->pDlg->PostMessage(UM_FTP_DOWNLOAD,(WPARAM)llProg,(LPARAM)pParam->s);
			}
			fclose(f);
			CFileStatus fs;
			fs.m_atime=fi.m_atime;
			fs.m_ctime=fi.m_ctime;
			fs.m_mtime=fi.m_mtime;
			fs.m_attribute=fi.m_attribute;
			fs.m_size=0;
			fs.m_szFullName[0]=0;
			CFile::SetStatus(szFullPath,fs);
			iFile++;
		}else if(cFlag==FLAG_DIR_DOWN)
		{
			nReaded=SockRecv(pParam->s,szFileName,MAX_PATH+1,&pParam->syncObj);
			if(nReaded!=MAX_PATH+1) {
				nRet=1;
				break;
			}
			iDir++;
			strcat(szWorkPath,"\\");
			strcat(szWorkPath,szFileName);
			CreateDirectory(szWorkPath,NULL);
		}else if(cFlag==FLAG_DIR_UP)
		{
			int nLen=strlen(szWorkPath);
			while(szWorkPath[nLen]!='\\') nLen--;
			szWorkPath[nLen]=0;
		}else
		{
			nRet=1;
			break;
		}
	}
	free(pbyBuf);
	TRACE("\nDir download over");
	if(pParam->syncObj.bCancel==FALSE)
	{
		if(nRet==0)
			pParam->pDlg->PostMessage(UM_FTP_DOWNLOAD,FTPM_FINISH,(LPARAM)pParam->s);
		else
			pParam->pDlg->PostMessage(UM_FTP_DOWNLOAD,FTPM_ERROR,(LPARAM)pParam->s);
	}
	return nRet;
}

int SendDirectory(PFTPARAM_UPLOAD pParam,LPCTSTR pszPath,char *pszBuf,DWORD &dwSentTotal)
{
	char szFilter[MAX_PATH];
	sprintf(szFilter,"%s\\*.*",pszPath);
	CFileFind find;
	BOOL bFind=find.FindFile(szFilter);
	char cFlag;
	char szPathBuf[MAX_PATH+1];
	while(bFind && pParam->syncObj.bCancel==FALSE)
	{
		bFind=find.FindNextFile();
		if(find.IsDirectory()) continue;
		//发送文件标志
		cFlag=FLAG_FILE;
		SockSend(pParam->s,&cFlag,1,&pParam->syncObj);
		//发送文件名
		strcpy(szPathBuf,find.GetFileName());
		SockSend(pParam->s,szPathBuf,MAX_PATH+1,&pParam->syncObj);
		//发送文件属性
		CFileStatus fs;
		CFile::GetStatus(find.GetFilePath(),fs);
		FILEINFO fi={0};
		fi.m_attribute=fs.m_attribute;
		fi.m_size=fs.m_size;
		fs.m_atime.GetAsSystemTime(fi.m_atime);
		fs.m_ctime.GetAsSystemTime(fi.m_ctime);
		fs.m_mtime.GetAsSystemTime(fi.m_mtime);
		SockSend(pParam->s,(char*)&fi,sizeof(fi),&pParam->syncObj);
		//开始传文件的数据
		DWORD dwSent=0;
		DWORD dwSize=find.GetLength();
		CFile f;
		if(!f.Open(find.GetFilePath(),CFile::modeRead|CFile::typeBinary|CFile::shareDenyWrite))
		{
			return 1;
		}
		while(dwSent<dwSize && pParam->syncObj.bCancel==FALSE)
		{
			UINT uRead=f.Read(pszBuf,SIZE_BLOCK);
			int nPut=SockSend(pParam->s,pszBuf,uRead,&pParam->syncObj);
			dwSent+=nPut;
			dwSentTotal+=nPut;
			//发送一个完成比率
			__int64 llProg=dwSentTotal;
			llProg=llProg*100/pParam->dwSize;
			pParam->pDlg->PostMessage(UM_FTP_UPLOAD,(WPARAM)llProg,pParam->s);
		}
		f.Close();
	}
	find.Close();
	if(pParam->syncObj.bCancel) return 1;
	bFind=find.FindFile(szFilter);
	int nRet=0;
	while(bFind)
	{
		bFind=find.FindNextFile();
		if(find.IsDirectory() && find.IsDots()==FALSE)
		{
			//发送进入子目录标志
			cFlag=FLAG_DIR_DOWN;
			SockSend(pParam->s,&cFlag,1,&pParam->syncObj);
			//发送文件名
			strcpy(szPathBuf,find.GetFileName());
			SockSend(pParam->s,szPathBuf,MAX_PATH+1,&pParam->syncObj);
			nRet=SendDirectory(pParam,find.GetFilePath(),pszBuf,dwSentTotal);
			if(nRet!=0) break;
			//发送退出子目录标志
			cFlag=FLAG_DIR_UP;
			SockSend(pParam->s,&cFlag,1,&pParam->syncObj);
		}
	}
	find.Close();
	return nRet;
}

UINT FuncDirUpload(LPVOID lpData)
{
	PFTPARAM_UPLOAD pParam=(PFTPARAM_UPLOAD)lpData;
	int nSent=SockSend(pParam->s,(char*)&pParam->nFiles,4,&pParam->syncObj);
	if(nSent!=4) 
	{
		return 1;
	}
	nSent=SockSend(pParam->s,(char*)&pParam->nDirs,4,&pParam->syncObj);
	if(nSent!=4)
	{
		return 1;
	}
	nSent=SockSend(pParam->s,(char*)&pParam->dwSize,4,&pParam->syncObj);
	if(nSent!=4)
	{
		return 1;
	}	
	char *pszBuf=(char*)malloc(SIZE_BLOCK);
	DWORD dwSent=0;
	int nRet=SendDirectory(pParam,pParam->szPath,pszBuf,dwSent);
	free(pszBuf);
	if(nRet==0)
	{
		pParam->syncObj.state=SS_FINISH;
	}else
	{
		pParam->pDlg->PostMessage(UM_FTP_UPLOAD,FTPM_ERROR,(LPARAM)pParam->s);
	}
	return nRet;
}

UINT FuncDownload(LPVOID lpData)
{
	PFTPARAM_DOWNLOAD pParam=(PFTPARAM_DOWNLOAD)lpData;
	//接收文件属性.
	FILEINFO fi={0};
	int nRead=SockRecv(pParam->s,(char*)&fi,sizeof(FILEINFO),&pParam->syncObj);
	if(nRead!=sizeof(FILEINFO))
	{
		pParam->pDlg->PostMessage(UM_FTP_DOWNLOAD,FTPM_ERROR,(LPARAM)pParam->s);
		return 1;
	}
	FILE *f=fopen(pParam->szPath,"wb");
	if(f)
	{
		char *pbyBuf=(char*)malloc(SIZE_BLOCK);
		DWORD dwRecv=0;
		DWORD dwSize=fi.m_size;
		while(dwRecv<dwSize && !pParam->syncObj.bCancel)
		{
			UINT uFatch=SIZE_BLOCK;
			if(uFatch>dwSize-dwRecv) uFatch=dwSize-dwRecv;
			int nGet=SockRecv(pParam->s,pbyBuf,uFatch,&pParam->syncObj);
			fwrite(pbyBuf,1,nGet,f);
			dwRecv+=nGet;
			__int64 llProg=dwRecv;
			llProg=llProg*100/dwSize;
			pParam->pDlg->PostMessage(UM_FTP_DOWNLOAD,(WPARAM)llProg,(LPARAM)pParam->s);
		}
		free(pbyBuf);
		fclose(f);
		if(!pParam->syncObj.bCancel)
		{
			CFileStatus fs;
			fs.m_atime=fi.m_atime;
			fs.m_ctime=fi.m_ctime;
			fs.m_mtime=fi.m_mtime;
			fs.m_attribute=fi.m_attribute;
			fs.m_size=0;
			fs.m_szFullName[0]=0;
			CFile::SetStatus(pParam->szPath,fs);
			pParam->pDlg->PostMessage(UM_FTP_DOWNLOAD,FTPM_FINISH,(LPARAM)pParam->s);
		}
	}else 
	{
		pParam->pDlg->PostMessage(UM_FTP_DOWNLOAD,FTPM_ERROR,(LPARAM)pParam->s);
	}
	return 0;
}

UINT FuncUpload(LPVOID lpData)
{
	PFTPARAM_UPLOAD pParam=(PFTPARAM_UPLOAD)lpData;

	CFileStatus fs;
	CFile::GetStatus(pParam->szPath,fs);
	FILEINFO fi={0};
	fi.m_attribute=fs.m_attribute;
	fi.m_size=fs.m_size;
	fs.m_atime.GetAsSystemTime(fi.m_atime);
	fs.m_ctime.GetAsSystemTime(fi.m_ctime);
	fs.m_mtime.GetAsSystemTime(fi.m_mtime);
	//发送文件属性
	int nSend=SockSend(pParam->s,(char*)&fi,sizeof(fi),&pParam->syncObj);
	if(nSend!=sizeof(fi))
	{
		pParam->pDlg->PostMessage(UM_FTP_UPLOAD,FTPM_ERROR,pParam->s);
		return 1;
	}
	//开始发送文件数据
	CFile file;
	BOOL bOpen=file.Open(pParam->szPath,CFile::modeRead|CFile::shareDenyWrite|CFile::typeBinary);
	if(!bOpen)
	{
		pParam->pDlg->PostMessage(UM_FTP_UPLOAD,FTPM_ERROR,pParam->s);
		return 1;
	}
	
	//将数据分块,最大一次传送1M
	char * pbyBuf=(char*)malloc(SIZE_BLOCK);
	DWORD dwSize=fi.m_size;
	DWORD dwSent=0;
	while(dwSent<dwSize && pParam->syncObj.bCancel==FALSE)
	{
		int nReaded=file.Read(pbyBuf,SIZE_BLOCK);
		dwSent+=SockSend(pParam->s,pbyBuf,nReaded,&pParam->syncObj);
		__int64 llProg=dwSent;
		llProg=llProg*100/dwSize;
		pParam->pDlg->PostMessage(UM_FTP_UPLOAD,(WPARAM)llProg,pParam->s);
	}
	free(pbyBuf);
	file.Close();
	pParam->syncObj.state=SS_FINISH;
	TRACE("\nUpload Finish*************");
	return 0;
}

void CFileTransDlg::StartDownload(SOCKET s, char * pszFileName,DWORD dwIP)
{//开始下载文件
	//查找文件是否在下载列表中,通过iImage来指示任务下载状态
	int iItem=-1;
	for(int i=0;i<m_lstDownload.GetItemCount();i++)
	{
		if(GetListCtrlItemImage(&m_lstDownload,i)!=0) continue;
		PFTTASKID pid=(PFTTASKID)m_lstDownload.GetItemData(i);
		if(strcmp(pszFileName,pid->szFileName)==0 && dwIP==pid->dwIP)
		{
			iItem=i;
			break;
		}
	}
	if(iItem==-1)
	{
		closesocket(s);
		return;
	}
	FTPARAM_DOWNLOAD *paramDownload=new FTPARAM_DOWNLOAD;
	paramDownload->s=s;
	//创建一个数据发送的同步对象
	InitializeCriticalSection(&paramDownload->syncObj.cs);
	paramDownload->syncObj.bCancel=FALSE;
	paramDownload->syncObj.state=SS_WORKING;
	paramDownload->syncObj.hEvent=CreateEvent(NULL,FALSE,TRUE,NULL);
	strcpy(paramDownload->szPath,pszFileName);
	paramDownload->pDlg=this;
	paramDownload->dwTime=GetTickCount();
	PFTTASKID pid=(PFTTASKID)m_lstDownload.GetItemData(iItem);
	//转移游戏邀请消息的数据
	memcpy(paramDownload->szMsgBuf,pid->szMsgBuf,pid->dwMsgBufLen);
	paramDownload->dwMsgBufLen=pid->dwMsgBufLen;
	paramDownload->pThread=AfxBeginThread((pid->nDirs>0)?FuncDirDownload:FuncDownload,paramDownload,THREAD_PRIORITY_BELOW_NORMAL,0,CREATE_SUSPENDED);
	delete pid;
	m_lstDownload.SetItemData(iItem,(LPARAM)paramDownload);
	SetListCtrlItemImage(&m_lstDownload,iItem,FTS_DOWNLOAD);
	paramDownload->pThread->ResumeThread();
}


LRESULT CFileTransDlg::OnFtpDownload(WPARAM wParam, LPARAM lParam)
{
	SOCKET sockSend=(SOCKET)lParam;
	int iItem=-1;
	char szBuf[MAX_PATH+1];
	for(int i=0;i<m_lstDownload.GetItemCount();i++)
	{
		int iImage=GetListCtrlItemImage(&m_lstDownload,i);
		if(iImage!=FTS_DOWNLOAD) continue;
		PFTPARAM_DOWNLOAD pParam=(PFTPARAM_DOWNLOAD)m_lstDownload.GetItemData(i);
		if(pParam->s==sockSend)
		{
			iItem=i;
			break;
		}
	}
	if(iItem==-1) return 0;
	PFTPARAM_DOWNLOAD pParam=(PFTPARAM_DOWNLOAD)m_lstDownload.GetItemData(iItem);
	if(wParam==FTPM_ERROR)
	{//通讯错误
		WaitForSingleObject(pParam->pThread->m_hThread,INFINITE);
		DeleteCriticalSection(&pParam->syncObj.cs);
		CloseHandle(pParam->syncObj.hEvent);
		closesocket(pParam->s);
		delete pParam;
		m_lstDownload.SetItemData(iItem,0);
		SetListCtrlItemImage(&m_lstDownload,iItem,FTS_ERROR);
	}else if(wParam==FTPM_CANCEL)
	{//数据通讯中止
		EnterCriticalSection(&pParam->syncObj.cs);
		pParam->syncObj.bCancel=TRUE;
		SetEvent(pParam->syncObj.hEvent);
		LeaveCriticalSection(&pParam->syncObj.cs);
		WaitForSingleObject(pParam->pThread->m_hThread,INFINITE);
		DeleteCriticalSection(&pParam->syncObj.cs);
		CloseHandle(pParam->syncObj.hEvent);
		closesocket(pParam->s);
		delete pParam;
		m_lstDownload.DeleteItem(iItem);
	}else if(wParam==FTPM_SOCKHALT)
	{//连接关闭
		EnterCriticalSection(&pParam->syncObj.cs);
		pParam->syncObj.bCancel=TRUE;
		SetEvent(pParam->syncObj.hEvent);
		LeaveCriticalSection(&pParam->syncObj.cs);
		WaitForSingleObject(pParam->pThread->m_hThread,INFINITE);
		DeleteCriticalSection(&pParam->syncObj.cs);
		CloseHandle(pParam->syncObj.hEvent);
		closesocket(pParam->s);
		delete pParam;
		m_lstDownload.SetItemData(iItem,0);
		SetListCtrlItemImage(&m_lstDownload,iItem,FTS_CANCEL);
	}else if(wParam==FTPM_FINISH)
	{//检查是否所有文件传输纯种都完成
		CTimeSpan timeUsed((GetTickCount()-pParam->dwTime)/1000);
		m_lstDownload.SetItemText(iItem,4,timeUsed.Format("%M分%S秒"));
		//先检查是否有游戏邀请消息的数据
		if(pParam->dwMsgBufLen)
		{
			CIntraDoorDlg *pMainDlg=(CIntraDoorDlg*)GetOwner();
			char szName[50];
			m_lstDownload.GetItemText(iItem,1,szName,50);
			pMainDlg->OnGameDownloadFinish(szName,pParam->szMsgBuf,pParam->dwMsgBufLen);
		}
		char *pName=new char[MAX_PATH];
		strcpy(pName,pParam->szPath);
		DeleteCriticalSection(&pParam->syncObj.cs);
		CloseHandle(pParam->syncObj.hEvent);
		closesocket(pParam->s);
		delete pParam;
		m_lstDownload.SetItemData(iItem,(DWORD)pName);
		SetListCtrlItemImage(&m_lstDownload,iItem,FTS_FINISH);
		
		BOOL bOver=TRUE;
		for(int i=0;i<m_lstDownload.GetItemCount();i++)
		{
			int iImage=GetListCtrlItemImage(&m_lstDownload,i);
			if(iImage==FTS_WAITTING)
			{
				bOver=FALSE;
				break;
			}
		}
		if(bOver) ListenEnd();
	}else if(wParam >=0 && wParam<=100)
	{//文件传输进度消息
		sprintf(szBuf,"%d%%",wParam);
		m_lstDownload.SetItemText(iItem,0,szBuf);
	}
	return 0;
}


LRESULT CFileTransDlg::OnFtpUpload(WPARAM wParam, LPARAM lParam)
{
	int iItem=-1;
	for(int i=0;i<m_lstUpload.GetItemCount();i++)
	{
		int iImage=GetListCtrlItemImage(&m_lstUpload,i);
		if(iImage!=FTS_UPLOAD) continue;
		PFTPARAM_UPLOAD pParam=(PFTPARAM_UPLOAD)m_lstUpload.GetItemData(i);
		if(pParam->s==(SOCKET)lParam)
		{
			iItem=i;
			break;
		}
	}
	if(iItem==-1) return 0;
	PFTPARAM_UPLOAD pParam=(PFTPARAM_UPLOAD)m_lstUpload.GetItemData(iItem);
	if(wParam==FTPM_ERROR)
	{//任务失败
		DeleteCriticalSection(&pParam->syncObj.cs);
		CloseHandle(pParam->syncObj.hEvent);
		closesocket(pParam->s);
		delete pParam;
		m_lstUpload.SetItemData(iItem,0);
		SetListCtrlItemImage(&m_lstUpload,iItem,FTS_ERROR);
	}else if(wParam==FTPM_CANCEL)
	{//用户中止
		EnterCriticalSection(&pParam->syncObj.cs);
		pParam->syncObj.bCancel=TRUE;
		SetEvent(pParam->syncObj.hEvent);
		LeaveCriticalSection(&pParam->syncObj.cs);
		WaitForSingleObject(pParam->pThread->m_hThread,INFINITE);
		DeleteCriticalSection(&pParam->syncObj.cs);
		CloseHandle(pParam->syncObj.hEvent);
		closesocket(pParam->s);
		delete pParam;
		m_lstUpload.DeleteItem(iItem);
	}else if(wParam==FTPM_SOCKHALT)
	{//Socket连接关闭
		EnterCriticalSection(&pParam->syncObj.cs);
		pParam->syncObj.bCancel=TRUE;
		SetEvent(pParam->syncObj.hEvent);
		LeaveCriticalSection(&pParam->syncObj.cs);
		WaitForSingleObject(pParam->pThread->m_hThread,INFINITE);
		DeleteCriticalSection(&pParam->syncObj.cs);
		CloseHandle(pParam->syncObj.hEvent);
		closesocket(pParam->s);
		delete pParam;
		m_lstUpload.SetItemData(iItem,0);
		SetListCtrlItemImage(&m_lstUpload,iItem,FTS_CANCEL);
	}else if(wParam==FTPM_FINISH)
	{//一个任务已经完成
		SetListCtrlItemImage(&m_lstUpload,iItem,FTS_FINISH);
		PFTPARAM_UPLOAD pParam=(PFTPARAM_UPLOAD)m_lstUpload.GetItemData(iItem);
		CTimeSpan timeUsed((GetTickCount()-pParam->dwTime)/1000);
		m_lstUpload.SetItemText(iItem,4,timeUsed.Format("%M分%S秒"));
		DeleteCriticalSection(&pParam->syncObj.cs);
		CloseHandle(pParam->syncObj.hEvent);
		closesocket(pParam->s);
		delete pParam;
		m_lstUpload.SetItemData(iItem,0);
		if(IsDlgButtonChecked(IDC_UPLOAD_AUTODEL))
		{//自动删除一个已经完成的任务
			m_lstUpload.SetSelectionMark(iItem);
			OnUploadDeletetask();
		}
	}else if(wParam>=0 && wParam<=100)
	{//上传进度
		char szBuf[100];
		sprintf(szBuf,"%d%%",wParam);
		m_lstUpload.SetItemText(iItem,0,szBuf);
	}	
	return 0;
}

void CFileTransDlg::PostNcDestroy() 
{
	CDialog::PostNcDestroy();
	delete this;
}

int CFileTransDlg::AddDownload(DWORD dwIP,LPCTSTR pszFileName,DWORD dwSize,int nFiles,int nDirs)
{
	int iItem=m_lstDownload.GetItemCount();
	m_lstDownload.InsertItem(iItem,"0%");
	int nNameLen=strlen(pszFileName);
	while(pszFileName[nNameLen]!='\\') nNameLen--;
	m_lstDownload.SetItemText(iItem,1,pszFileName+nNameLen+1);
	char szBuf[50];
	sprintf(szBuf,"%u.%u.%u.%u",dwIP&0xFF,(dwIP>>8)&0xFF,(dwIP>>16)&0xFF,(dwIP>>24)&0xFF);
	m_lstDownload.SetItemText(iItem,2,szBuf);
	GetSizeString(dwSize,szBuf);
	m_lstDownload.SetItemText(iItem,3,szBuf);

	SetListCtrlItemImage(&m_lstDownload,iItem,FTS_WAITTING);
	PFTTASKID pid=new FTTASKID;
	strcpy(pid->szFileName,pszFileName);
	pid->dwIP=dwIP;
	pid->nDirs=nDirs;
	pid->nFiles=nFiles;
	pid->dwSize=dwSize;
	pid->dwMsgBufLen=0;
	m_lstDownload.SetItemData(iItem,(LPARAM)pid);
	if(m_sockSvr==0) ListenStart();
	return iItem;
}

int CFileTransDlg::AddDownloadGame(DWORD dwIP,LPCTSTR pszFileName,DWORD dwSize,int nFiles,int nDirs,char *pszMsgData,DWORD dwMsgDataLen)
{
	int iTask=AddDownload(dwIP,pszFileName,dwSize,nFiles,nDirs);
	PFTTASKID pid=(PFTTASKID)m_lstDownload.GetItemData(iTask);
	memcpy(pid->szMsgBuf,pszMsgData,dwMsgDataLen);
	pid->dwMsgBufLen=dwMsgDataLen;
	return iTask;
}

BOOL CFileTransDlg::AddUpload(DWORD dwIP, LPCTSTR pszFileName,DWORD dwSize,int nFiles,int nDirs)
{
	//先查找是否存在一个相同的上传任务
	for(int i=0;i<m_lstUpload.GetItemCount();i++)
	{
		int iImage=GetListCtrlItemImage(&m_lstUpload,i);
		if(iImage==FTS_WAITTING)
		{
			PFTTASKID pTaskID=(PFTTASKID)m_lstUpload.GetItemData(i);
			if(strcmp(pTaskID->szFileName,pszFileName)==0 && pTaskID->dwIP==dwIP) return FALSE;
		}else if(iImage==FTS_UPLOAD)
		{
			PFTPARAM_UPLOAD pParam=(PFTPARAM_UPLOAD)m_lstUpload.GetItemData(i);
			if(pParam->dwIP==dwIP && strcmp(pParam->szPath,pszFileName)==0) return FALSE;
		}
	}
	MakeShow(PANEL_UPLOAD);
	PFTTASKID pID=new FTTASKID;
	pID->dwIP=dwIP;
	strcpy(pID->szFileName,pszFileName);
	pID->dwSize=dwSize;
	pID->nDirs=nDirs;
	pID->nFiles=nFiles;
	int iItem=m_lstUpload.GetItemCount();
	m_lstUpload.InsertItem(iItem,"0%");
	m_lstUpload.SetItemText(iItem,1,pszFileName);
	char szBuf[50];
	sprintf(szBuf,"%u.%u.%u.%u",dwIP&0xFF,(dwIP>>8)&0xFF,(dwIP>>16)&0xFF,(dwIP>>24)&0xFF);
	m_lstUpload.SetItemText(iItem,2,szBuf);
	GetSizeString(dwSize,szBuf);
	m_lstUpload.SetItemText(iItem,3,szBuf);

	m_lstUpload.SetItemData(iItem,(DWORD)pID);
	SetListCtrlItemImage(&m_lstUpload,iItem,FTS_WAITTING);
	return TRUE;
}

//上传请求被拒绝
void CFileTransDlg::UploadReject(LPCTSTR pszFileName,DWORD dwIP)
{
	for(int i=0;i<m_lstUpload.GetItemCount();i++)
	{
		int iImage=GetListCtrlItemImage(&m_lstUpload,i);
		if(iImage!=FTS_WAITTING) continue;
		PFTTASKID pid=(PFTTASKID)m_lstUpload.GetItemData(i);
		if(strcmp(pid->szFileName,pszFileName)==0 && dwIP==pid->dwIP)
		{
			m_lstUpload.SetItemText(i,0,"拒绝");
			SetListCtrlItemImage(&m_lstUpload,i,FTS_ERROR);
			delete pid;
			m_lstUpload.SetItemData(i,0);
			break;
		}
	}
}


void CFileTransDlg::StartUpload(LPCTSTR pszFileName,LPCTSTR pszNewFileName,DWORD dwIP)
{//开始上传文件
	FTPARAM_UPLOAD *paramUpload=new FTPARAM_UPLOAD;
	paramUpload->s=socket(AF_INET,SOCK_STREAM,0);
	SOCKADDR_IN addrTo;
	addrTo.sin_family=AF_INET;
	addrTo.sin_addr.S_un.S_addr=dwIP;
	addrTo.sin_port=htons(PORT_DOOR);
	int iItem=-1;
	for(int i=0;i<m_lstUpload.GetItemCount();i++)
	{
		int iImage=GetListCtrlItemImage(&m_lstUpload,i);
		if(iImage!=FTS_WAITTING) continue;
		PFTTASKID pid=(PFTTASKID)m_lstUpload.GetItemData(i);
		if(strcmp(pid->szFileName,pszFileName)==0 && pid->dwIP==dwIP)
		{
			iItem=i;
			break;
		}
	}
	ASSERT(iItem!=-1);
	PFTTASKID pid=(PFTTASKID)m_lstUpload.GetItemData(iItem);
	DWORD dwFlag=FLAG_FILETRANS;
	strcpy(paramUpload->szPath,pszFileName);
	paramUpload->dwIP=dwIP;
	//设置2M的缓冲区
	int nBufSize=SIZE_BLOCK*2;
	setsockopt(paramUpload->s,SOL_SOCKET,SO_SNDBUF,(char *)&nBufSize,sizeof(int));
	if(connect(paramUpload->s,(SOCKADDR*)&addrTo,sizeof(SOCKADDR))==SOCKET_ERROR) goto error;

	//发送握手信号
	if(4!=send(paramUpload->s,(char*)&dwFlag,4,0)) goto error;
	//发送文件名,以便接收方知道接收的是哪个文件
	char szBuf[MAX_PATH+1];
	strcpy(szBuf,pszNewFileName);
	if(MAX_PATH+1 != theApp.Send(paramUpload->s,szBuf,MAX_PATH+1,10000)) goto error;

	paramUpload->dwSize=pid->dwSize;
	paramUpload->nFiles=pid->nFiles;
	paramUpload->nDirs=pid->nDirs;
	paramUpload->pDlg=this;
	paramUpload->dwTime=GetTickCount();
	InitializeCriticalSection(&paramUpload->syncObj.cs);
	paramUpload->syncObj.bCancel=FALSE;
	paramUpload->syncObj.state=SS_WORKING;
	paramUpload->syncObj.hEvent=CreateEvent(NULL,FALSE,TRUE,NULL);

	WSAAsyncSelect(paramUpload->s,m_hWnd,UM_SOCKETMSG,FD_CLOSE|FD_WRITE);
	SetListCtrlItemImage(&m_lstUpload,iItem,FTS_UPLOAD);
	paramUpload->pThread=AfxBeginThread(pid->nDirs>0?FuncDirUpload:FuncUpload,paramUpload,THREAD_PRIORITY_BELOW_NORMAL,0,CREATE_SUSPENDED);
	delete pid;
	m_lstUpload.SetItemData(iItem,(DWORD)paramUpload);
	paramUpload->pThread->ResumeThread();
	return;
error:
	closesocket(paramUpload->s);
	delete paramUpload;
	delete pid;
	m_lstUpload.SetItemData(iItem,0);
	SetListCtrlItemImage(&m_lstUpload,iItem,FTS_ERROR);
}

void CFileTransDlg::OnSelchangeTabTasktype(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	switch(m_nCurPanel)
	{
	case PANEL_DOWNLOAD:
		ShowPanel(g_uPanelDownload,sizeof(g_uPanelDownload)/sizeof(UINT),FALSE);
		break;
	case PANEL_UPLOAD:
		ShowPanel(g_uPanelUpload,sizeof(g_uPanelUpload)/sizeof(UINT),FALSE);
		break;
	case PANEL_CONFIRM:
		ShowPanel(g_uPanelConfirm,sizeof(g_uPanelConfirm)/sizeof(UINT),FALSE);
		break;
	}
	m_nCurPanel=m_tabTaskType.GetCurSel();
	switch(m_nCurPanel)
	{
	case PANEL_DOWNLOAD:
		ShowPanel(g_uPanelDownload,sizeof(g_uPanelDownload)/sizeof(UINT),TRUE);
		break;
	case PANEL_UPLOAD:
		ShowPanel(g_uPanelUpload,sizeof(g_uPanelUpload)/sizeof(UINT),TRUE);
		break;
	case PANEL_CONFIRM:
		ShowPanel(g_uPanelConfirm,sizeof(g_uPanelConfirm)/sizeof(UINT),TRUE);
		break;
	}
	*pResult = 0;
}

void CFileTransDlg::OnDestroy() 
{
	int i;
	//取消所有任务
	for(i=0;i<m_lstDownload.GetItemCount();i++)
	{
		int iImage=GetListCtrlItemImage(&m_lstDownload,i);
		if(iImage==FTS_WAITTING || iImage==FTS_FINISH)
		{
			PFTTASKID pid=(PFTTASKID)m_lstDownload.GetItemData(i);
			delete pid;
		}else if(iImage==FTS_DOWNLOAD)
		{
			PFTPARAM_DOWNLOAD  pParam=(PFTPARAM_DOWNLOAD)m_lstDownload.GetItemData(i);
			pParam->syncObj.bCancel=TRUE;
			EnterCriticalSection(&pParam->syncObj.cs);
			SetEvent(pParam->syncObj.hEvent);
			LeaveCriticalSection(&pParam->syncObj.cs);
			WaitForSingleObject(pParam->pThread->m_hThread,INFINITE);
			DeleteCriticalSection(&pParam->syncObj.cs);
			CloseHandle(pParam->syncObj.hEvent);
			delete pParam;
		}
	}
	for(i=0;i<m_lstUpload.GetItemCount();i++)
	{
		int iImage=GetListCtrlItemImage(&m_lstUpload,i);
		if(iImage==FTS_WAITTING)
		{
			PFTTASKID pid=(PFTTASKID)m_lstUpload.GetItemData(i);
			delete pid;
		}else if(iImage==FTS_UPLOAD)
		{
			PFTPARAM_UPLOAD  pParam=(PFTPARAM_UPLOAD)m_lstUpload.GetItemData(i);
			pParam->syncObj.bCancel=TRUE;
			EnterCriticalSection(&pParam->syncObj.cs);
			SetEvent(pParam->syncObj.hEvent);
			LeaveCriticalSection(&pParam->syncObj.cs);
			WaitForSingleObject(pParam->pThread->m_hThread,INFINITE);
			DeleteCriticalSection(&pParam->syncObj.cs);
			CloseHandle(pParam->syncObj.hEvent);
			delete pParam;
		}
	}
	for(i=0;i<m_lstTask.GetItemCount();i++)
	{
		PFILETRANSTASK pParam=(PFILETRANSTASK)m_lstTask.GetItemData(i);
		delete pParam;
	}
	CDialog::OnDestroy();	
}

void CFileTransDlg::GetSizeString(DWORD dwSize, char *pszBuf)
{
	char szUnit[2]="B";
	if(dwSize>(1<<10))
	{
		szUnit[0]='K';
		dwSize>>=10;
		if(dwSize>(1<<10))
		{
			dwSize>>=10;
			szUnit[0]='M';
		}
	}
	sprintf(pszBuf,"%d%s",dwSize,szUnit);
}

void CFileTransDlg::OnDblclkListDownload(NMHDR* pNMHDR, LRESULT* pResult) 
{
	int nItem=m_lstDownload.GetSelectionMark();
	if(nItem!=-1)
	{
		int iImage=GetListCtrlItemImage(&m_lstDownload,nItem);
		if(iImage==FTS_FINISH)
		{
			char *pName=(char*)m_lstDownload.GetItemData(nItem);
			ShellExecute(m_hWnd,"open",pName,NULL,NULL,SW_SHOW);
		}
	}
	*pResult = 0;
}

int CFileTransDlg::Sock2DownloadTask(SOCKET s)
{
	for(int i=0;i<m_lstDownload.GetItemCount();i++)
	{
		int iImage=GetListCtrlItemImage(&m_lstDownload,i);
		if(iImage==FTS_DOWNLOAD) 
		{
			PFTPARAM_DOWNLOAD pParam=(PFTPARAM_DOWNLOAD)m_lstDownload.GetItemData(i);
			if(pParam->s==s) return i;
		}
	}
	return -1;
}

int CFileTransDlg::Sock2UploadTask(SOCKET s)
{
	for(int i=0;i<m_lstUpload.GetItemCount();i++)
	{//上传任务中断
		int iImage=GetListCtrlItemImage(&m_lstUpload,i);
		if(iImage==FTS_UPLOAD) 
		{
			PFTPARAM_UPLOAD pParam=(PFTPARAM_UPLOAD)m_lstUpload.GetItemData(i);
			if(pParam->s==s) return i;
		}
	}
	return -1;
}

int CFileTransDlg::GetConfirmTask()
{
	int iRet=-1;
	for(int i=m_lstTask.GetItemCount()-1;i>=0;i--)
	{
		if(ListView_GetCheckState(m_lstTask.m_hWnd,i))
		{
			iRet=i;
			break;
		}
	}
	return iRet;
}

int CFileTransDlg::GetTaskIndex(FILETRANSTASK *pTask)
{
	for(int i=m_lstTask.GetItemCount()-1;i>=0;i--)
	{
		FILETRANSTASK *pftt=(FILETRANSTASK*)m_lstTask.GetItemData(i);
		if(strcmp(pTask->szName,pftt->szName)==0 && pTask->dwIP==pftt->dwIP) return i;
	}
	return -1;
}


void CFileTransDlg::AddTask(char *pszFileName,DWORD dwIP,DWORD dwSize,int nFiles,int nDirs)
{
	MakeShow(PANEL_CONFIRM);
	FILETRANSTASK	*pftTask=new FILETRANSTASK;
	strcpy(pftTask->szName,pszFileName);
	pftTask->nFiles=nFiles;
	pftTask->dwIP=dwIP;
	pftTask->dwSize=dwSize;
	pftTask->nDirs=nDirs;
	int iTask=GetTaskIndex(pftTask);
	if(iTask!=-1)
	{//防止任务重复
		delete pftTask;
		return;
	}
	char szBuf[100];
	iTask=m_lstTask.GetItemCount();
	sprintf(szBuf,"%u.%u.%u.%u",dwIP&0xFF,(dwIP>>8)&0xFF,(dwIP>>16)&0xFF,(dwIP>>24)&0xFF);
	m_lstTask.InsertItem(iTask,szBuf);
	int nLen=strlen(pszFileName);
	while(pszFileName[nLen]!='\\') nLen--;
	m_lstTask.SetItemText(iTask,1,pszFileName+nLen+1);

	char szUnit[]="B";
	if(dwSize>=(2<<10))
	{
		dwSize>>=10;
		szUnit[0]='K';
		if(dwSize>=(2<<10))
		{
			dwSize>>=10;
			szUnit[0]='M';
		}
	}
	sprintf(szBuf,"%u%s",dwSize,szUnit);
	m_lstTask.SetItemText(iTask,2,szBuf);
	sprintf(szBuf,"%d",nFiles);
	m_lstTask.SetItemText(iTask,3,szBuf);
	m_lstTask.SetItemData(iTask,(LPARAM)pftTask);
	ListView_SetCheckState(m_lstTask.m_hWnd,iTask,1);
}


void CFileTransDlg::DelTask(char *pszFileName, DWORD dwIP)
{
	int iTask=-1;
	for(int i=0;i<m_lstTask.GetItemCount();i++)
	{
		FILETRANSTASK	*pftTask=( FILETRANSTASK *)m_lstTask.GetItemData(i);
		if(strcmp(pszFileName,pftTask->szName)==0 && dwIP==pftTask->dwIP)
		{
			iTask=i;
			break;
		}
	}
	if(iTask!=-1)
	{
		FILETRANSTASK	*pftTask=( FILETRANSTASK *)m_lstTask.GetItemData(iTask);
		delete pftTask;
		m_lstTask.DeleteItem(iTask);
	}
}

void CFileTransDlg::OnDownloadCanceltask() 
{
	int iItem=m_lstDownload.GetSelectionMark();
	if(iItem!=-1)
	{
		int iImage=GetListCtrlItemImage(&m_lstDownload,iItem);
		if(iImage==FTS_DOWNLOAD)
		{//正在传输
			if(MessageBox("确定要中止该文件的传输吗？",NULL,MB_OKCANCEL)==IDCANCEL) return;
			PFTPARAM_DOWNLOAD pParam=(PFTPARAM_DOWNLOAD)m_lstDownload.GetItemData(iItem);
			PostMessage(UM_FTP_DOWNLOAD,FTPM_CANCEL,pParam->s);
		}
	}
}

void CFileTransDlg::OnDownloadDeletetask() 
{
	int iItem=m_lstDownload.GetSelectionMark();
	if(iItem!=-1)
	{
		int iImage=GetListCtrlItemImage(&m_lstDownload,iItem);
		if(iImage==FTS_WAITTING || iImage==FTS_FINISH)
		{
			PFTTASKID pid=(PFTTASKID)m_lstDownload.GetItemData(iItem);
			delete pid;
		}else if(iImage==FTS_DOWNLOAD)
		{
			MessageBox("正在下载,不能删除");
			return;
		}
		m_lstDownload.DeleteItem(iItem);
	}	
}

void CFileTransDlg::OnUploadCanceltask() 
{
	int iItem=m_lstUpload.GetSelectionMark();
	if(iItem!=-1)
	{
		int iImage=GetListCtrlItemImage(&m_lstUpload,iItem);
		if(iImage==FTS_UPLOAD)
		{//正在传输
			if(MessageBox("确定要中止该文件的传输吗？",NULL,MB_OKCANCEL)==IDCANCEL) return;
			PFTPARAM_UPLOAD pParam=(PFTPARAM_UPLOAD)m_lstUpload.GetItemData(iItem);
			PostMessage(UM_FTP_UPLOAD,FTPM_CANCEL,pParam->s);
		}else if(iImage==FTS_WAITTING)
		{//用户还没有确认接收的任务
			PFTTASKID pid=(PFTTASKID)m_lstUpload.GetItemData(iItem);
			theApp.SendMsg(pid->dwIP,PORT_DOOR,MAIN_FILETRANS,SUB_FILETRANS_DEL,pid->szFileName,strlen(pid->szFileName)+1);
			delete pid;
			m_lstUpload.DeleteItem(iItem);
		}
	}
}

void CFileTransDlg::OnUploadDeletetask() 
{
	int iItem=m_lstUpload.GetSelectionMark();
	if(iItem!=-1)
	{
		int iImage=GetListCtrlItemImage(&m_lstUpload,iItem);
		if(iImage==FTS_WAITTING || iImage==FTS_UPLOAD)
		{
			MessageBox("正在上传,不能删除");
			return;
		}else
		{
			m_lstUpload.DeleteItem(iItem);
		}
	}
}

void CFileTransDlg::OnDownloadOpendir() 
{
	int iTask=m_lstDownload.GetSelectionMark();
	if(iTask!=-1)
	{
		int nState=GetListCtrlItemImage(&m_lstDownload,iTask);
		if(nState==FTS_FINISH)
		{
			char *pszPath=(char*)m_lstDownload.GetItemData(iTask);
			CString str=pszPath;
			int nPos=str.ReverseFind('\\');
			CString strPath=str.Left(nPos);
			CString strTitle=str.Right(str.GetLength()-nPos-1);
			ShellExecute(NULL,"explore",strPath,NULL,NULL,SW_SHOWDEFAULT);
		}
	}
}

void CFileTransDlg::OnConfirmRecieve() 
{
	while(1)
	{
		int iTask=GetConfirmTask();
		if(iTask==-1) break;
		FILETRANSTASK ftt=*(FILETRANSTASK*)m_lstTask.GetItemData(iTask);
		int nLen=strlen(ftt.szName);
		while(ftt.szName[nLen]!='\\') nLen--;
		CFileDialog saveDlg(FALSE,NULL,ftt.szName+nLen+1);
		if(saveDlg.DoModal()==IDCANCEL) return;
		iTask=GetTaskIndex(&ftt);
		if(iTask==-1)
		{
			MessageBox("该下载任务已经被上传方取消");
		}else
		{
			FILETRANSTASK *pTaskInfo=(FILETRANSTASK*)m_lstTask.GetItemData(iTask);
			CString strNewName=saveDlg.GetPathName();
			AddDownload(pTaskInfo->dwIP,strNewName,pTaskInfo->dwSize,pTaskInfo->nFiles,pTaskInfo->nDirs);
			//向上传方发送一个确认接收消息
			char szBuf[500];
			strcpy(szBuf,pTaskInfo->szName);
			int nLen=strlen(pTaskInfo->szName)+1;
			strcpy(szBuf+nLen,strNewName);
			nLen+=strNewName.GetLength()+1;
			theApp.SendMsg(pTaskInfo->dwIP,PORT_DOOR,MAIN_FILETRANS,SUB_FILETRANS_ACK,szBuf,nLen);
			m_lstTask.DeleteItem(iTask);
			delete pTaskInfo;
		}
	}
}

void CFileTransDlg::OnConfirmReject() 
{
	for(int i=m_lstTask.GetItemCount()-1;i>=0;i--)
	{
		if(ListView_GetCheckState(m_lstTask.m_hWnd,i))
		{
			PFILETRANSTASK pTaskInfo=(PFILETRANSTASK)m_lstTask.GetItemData(i);
			//向上传方发送一个拒绝接收消息
			char szBuf[500];
			strcpy(szBuf,pTaskInfo->szName);
			int nLen=strlen(szBuf)+1;
			szBuf[nLen++]=0;
			theApp.SendMsg(pTaskInfo->dwIP,PORT_DOOR,MAIN_FILETRANS,SUB_FILETRANS_ACK,szBuf,nLen);
			m_lstTask.DeleteItem(i);
			delete pTaskInfo;
		}
	}	
}

void CFileTransDlg::ShowPanel(UINT *pPanel, int nCtrls, BOOL bShow)
{
	int nCmdShow=bShow?SW_SHOW:SW_HIDE;
	for(int i=0;i<nCtrls;i++)
	{
		GetDlgItem(pPanel[i])->ShowWindow(nCmdShow);
	}
}

void CFileTransDlg::MakeShow(int nPanel)
{
	if(IsWindowVisible()==FALSE)
	{
		CWnd *pMain=GetOwner();
		RECT rc;
		pMain->GetWindowRect(&rc);
		SetWindowPos(NULL,rc.right,rc.top,0,0,SWP_NOZORDER|SWP_NOSIZE|SWP_SHOWWINDOW);
	}
	m_tabTaskType.SetCurSel(nPanel);
	LRESULT lRes=0;
	OnSelchangeTabTasktype(NULL,&lRes);
}
