// UserInfoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "intradoor.h"
#include "UserInfoDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CUserInfoDlg dialog


CUserInfoDlg::CUserInfoDlg(USERINFO *pUserInfo)
	: CDialog(CUserInfoDlg::IDD, NULL)
{
	//{{AFX_DATA_INIT(CUserInfoDlg)
	m_strGroup = _T("");
	//}}AFX_DATA_INIT
	m_strName=pUserInfo->szName;
	m_strDesc=pUserInfo->szDesc;
	m_strPhone=pUserInfo->szPhone;
	m_strGroup=pUserInfo->szGroup;
	m_nSex=pUserInfo->cSex;
}


void CUserInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CUserInfoDlg)
	DDX_Control(pDX, IDC_AVATAR_PREVIEW, m_ctrlAvatar);
	DDX_Text(pDX, IDC_DESC, m_strDesc);
	DDV_MaxChars(pDX, m_strDesc, 300);
	DDX_Text(pDX, IDC_NAME, m_strName);
	DDV_MaxChars(pDX, m_strName, 50);
	DDX_Text(pDX, IDC_PHONE, m_strPhone);
	DDV_MaxChars(pDX, m_strPhone, 30);
	DDX_Text(pDX, IDC_GROUP, m_strGroup);
	DDV_MaxChars(pDX, m_strGroup, 30);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CUserInfoDlg, CDialog)
	//{{AFX_MSG_MAP(CUserInfoDlg)
	ON_BN_CLICKED(IDC_USERAVATAR, OnUserAvatar)
	ON_BN_CLICKED(IDC_AVATAR_NORMAL, OnAvatarNormal)
	//}}AFX_MSG_MAP
	ON_CONTROL_RANGE(BN_CLICKED,IDC_SEX_MALE,IDC_SEX_SECRET,OnSexClick)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUserInfoDlg message handlers

BOOL CUserInfoDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	CheckRadioButton(IDC_SEX_MALE,IDC_SEX_SECRET,IDC_SEX_MALE+m_nSex);	
	char szAvatarFile[MAX_PATH];
	sprintf(szAvatarFile,"%s\\avatar\\myshow.gif",theApp.m_szPath);
	if(GetFileAttributes(szAvatarFile)==-1)
	{
		if(m_nSex==1)
			sprintf(szAvatarFile,"%s\\avatar\\female.gif",theApp.m_szPath);
		else
			sprintf(szAvatarFile,"%s\\avatar\\male.gif",theApp.m_szPath);
	}
	m_ctrlAvatar.SetGifFile(szAvatarFile);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CUserInfoDlg::OnOK() 
{
	m_nSex=GetCheckedRadioButton(IDC_SEX_MALE,IDC_SEX_SECRET)-IDC_SEX_MALE;
	CDialog::OnOK();
}

void CUserInfoDlg::OnUserAvatar() 
{
	CFileDialog openDlg(TRUE,"GIF",NULL,0,"GIF FILE(*.GIF)|*.GIF||");
	if(openDlg.DoModal()==IDOK)
	{
		char szDestFile[MAX_PATH];
		sprintf(szDestFile,"%s\\Avatar\\myshow.gif",theApp.m_szPath);
		CopyFile(openDlg.GetPathName(),szDestFile,FALSE);
		m_ctrlAvatar.SetGifFile(szDestFile);
	}
}


void CUserInfoDlg::OnSexClick(UINT uSex)
{
	char szFileName[MAX_PATH];
	sprintf(szFileName,"%s\\Avatar\\myshow.gif",theApp.m_szPath);
	if(GetFileAttributes(szFileName)!=-1) return;
	switch(uSex)
	{
	case IDC_SEX_FEMALE:
		sprintf(szFileName,"%s\\Avatar\\female.gif",theApp.m_szPath);
		break;
	default:
		sprintf(szFileName,"%s\\Avatar\\male.gif",theApp.m_szPath);
		break;
	}
	m_ctrlAvatar.SetGifFile(szFileName);
}

void CUserInfoDlg::OnAvatarNormal() 
{
	char szFileName[MAX_PATH];
	sprintf(szFileName,"%s\\Avatar\\myshow.gif",theApp.m_szPath);
	DeleteFile(szFileName);
	UINT uSexID=GetCheckedRadioButton(IDC_SEX_MALE,IDC_SEX_SECRET)|(BN_CLICKED<<16);
	OnSexClick(uSexID);
}
