#if !defined(AFX_FILETRANSDLG_H__6B84B963_0D9D_4D72_BCCF_060D4239D2DB__INCLUDED_)
#define AFX_FILETRANSDLG_H__6B84B963_0D9D_4D72_BCCF_060D4239D2DB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FileTransDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CFileTransDlg dialog
#include <afxtempl.h>

#define UM_FTP_DOWNLOAD	(WM_USER+200)
#define UM_FTP_UPLOAD	(WM_USER+300)

typedef struct tagFILETRANSTASK
{
	DWORD dwIP;
	char szName[MAX_PATH];
	DWORD dwSize;
	int	 nFiles;
	int  nDirs;
}FILETRANSTASK,*PFILETRANSTASK;


typedef struct tagFILEINFO
{
	SYSTEMTIME m_ctime;
	SYSTEMTIME m_mtime;
	SYSTEMTIME m_atime;
	LONG m_size;
	BYTE m_attribute;
}FILEINFO,*PFILEINFO;

typedef enum tagFTSTATE
{
	FTS_WAITTING=0,	//等待
	FTS_DOWNLOAD,	//正在下载
	FTS_UPLOAD,		//正在上传
	FTS_FINISH,		//完成
	FTS_ERROR,		//错误
	FTS_CANCEL,		//被动中止
}FTSTATE;

typedef struct tagFTTASKID
{
	char szFileName[MAX_PATH];
	int nDirs;
	int nFiles;
	DWORD dwIP;
	DWORD dwSize;
	//给游戏下载使用的保存当前邀请消息数据
	char szMsgBuf[500];
	DWORD dwMsgBufLen;
}FTTASKID,*PFTTASKID;


class CFileTransDlg : public CDialog
{
// Construction
public:
	void MakeShow(int nPanel);
	void ShowPanel(UINT *pPanel,int nCtrls,BOOL  bShow);
	void DelTask(char *pszFileName,DWORD dwIP);
	void AddTask(char *pszFileName,DWORD dwIP,DWORD dwSize,int nFiles,int nDirs);
	void GetSizeString(DWORD dwSize,char *pszBuf);
	void StartDownload(SOCKET s,char * pszFileName,DWORD dwIP);
	void StartUpload(LPCTSTR pszFileName,LPCTSTR pszNewFileName,DWORD dwIP);
	void UploadReject(LPCTSTR pszFileName,DWORD dwIP);
	BOOL AddUpload(DWORD dwIP,LPCTSTR pszFileName,DWORD dwSize,int nFiles,int nDirs);
	int AddDownload(DWORD dwIP,LPCTSTR pszFileName,DWORD dwSize,int nFiles,int nDirs);
	int AddDownloadGame(DWORD dwIP,LPCTSTR pszFileName,DWORD dwSize,int nFiles,int nDirs,char *pszMsgData,DWORD dwMsgDataLen);
	void ListenEnd();
	void ListenStart();
	CFileTransDlg(CWnd* pParent = NULL);   // standard constructor
	
	int		m_nCurPanel;
// Dialog Data
	//{{AFX_DATA(CFileTransDlg)
	enum { IDD = IDD_FILETRANS };
	CListCtrl	m_lstTask;
	CTabCtrl	m_tabTaskType;
	CListCtrl	m_lstUpload;
	CListCtrl	m_lstDownload;
	//}}AFX_DATA
	CImageList	m_ilState;
	SOCKET	m_sockSvr;			//监听Socket
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFileTransDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
protected:
	int GetTaskIndex(FILETRANSTASK *pTask);
	int GetConfirmTask();
	int Sock2UploadTask(SOCKET s);
	int Sock2DownloadTask(SOCKET s);
	afx_msg LRESULT OnFtpDownload(WPARAM wParam,LPARAM lParam);
	afx_msg LRESULT OnFtpUpload(WPARAM wParam,LPARAM lParam);
	afx_msg LRESULT OnSockMsg(WPARAM wParam,LPARAM lParam);

	// Generated message map functions
	//{{AFX_MSG(CFileTransDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeTabTasktype(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDestroy();
	afx_msg void OnDblclkListDownload(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDownloadCanceltask();
	afx_msg void OnDownloadDeletetask();
	afx_msg void OnDownloadOpendir();
	afx_msg void OnUploadCanceltask();
	afx_msg void OnUploadDeletetask();
	afx_msg void OnConfirmRecieve();
	afx_msg void OnConfirmReject();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

typedef enum  tagSOCKSTATE
{//Sock状态
	SS_WORKING=0,	//正在读/写
	SS_WAITTING,	//正在等待
	SS_FINISH,		//数据发送完成
}SOCKSTATE;

typedef struct tagSYNCOBJ
{
	CRITICAL_SECTION	cs;
	BOOL		bCancel;
	HANDLE		hEvent;
	SOCKSTATE	state;
}SYNCOBJ,*PSYNCOBJ;

typedef struct tagFTPARAM_DOWNLOAD
{
	SOCKET s;
	char szPath[MAX_PATH+1];
	CFileTransDlg *pDlg;
	//给游戏下载使用的保存当前邀请消息数据
	char szMsgBuf[500];
	DWORD dwMsgBufLen;
	CWinThread *pThread;
	SYNCOBJ syncObj;
	DWORD	dwTime;
}FTPARAM_DOWNLOAD,*PFTPARAM_DOWNLOAD;

typedef struct tagFTPARAM_UPLOAD
{
	SOCKET s;
	DWORD dwSize;
	int	 nFiles;
	int	 nDirs;
	DWORD dwIP;
	char szPath[MAX_PATH+1];
	CFileTransDlg *pDlg;
	CWinThread *pThread;
	SYNCOBJ syncObj;
	DWORD	dwTime;
}FTPARAM_UPLOAD,*PFTPARAM_UPLOAD;

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FILETRANSDLG_H__6B84B963_0D9D_4D72_BCCF_060D4239D2DB__INCLUDED_)
