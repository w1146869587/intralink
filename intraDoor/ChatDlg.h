#if !defined(AFX_CHATDLG_H__E4C08585_1CFB_4A2E_91E6_FA2E64B1D1FA__INCLUDED_)
#define AFX_CHATDLG_H__E4C08585_1CFB_4A2E_91E6_FA2E64B1D1FA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ChatDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CChatDlg dialog
class CIntraDoorDlg;
class CChatDlg : public CDialog
{
// Construction
public:
	void AddSent(CString strAddressor,CString strSent);
	CChatDlg(CWnd* pParent = NULL);   // standard constructor
	CString		m_strMateName;
	DWORD		m_dwIP;
	
	CIntraDoorDlg	*m_pMainDlg;
// Dialog Data
	//{{AFX_DATA(CChatDlg)
	enum { IDD = IDD_CHAT };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CChatDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual void OnCancel();

	// Generated message map functions
	//{{AFX_MSG(CChatDlg)
	afx_msg void OnSend();
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnSave();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHATDLG_H__E4C08585_1CFB_4A2E_91E6_FA2E64B1D1FA__INCLUDED_)
