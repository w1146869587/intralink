// intraDoor.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "intraDoor.h"
#include "intraDoorDlg.h"
#include "UserInfoDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIntraDoorApp

BEGIN_MESSAGE_MAP(CIntraDoorApp, CWinApp)
	//{{AFX_MSG_MAP(CIntraDoorApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIntraDoorApp construction

CIntraDoorApp::CIntraDoorApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
	m_pdwLocalIP=NULL;
	m_pDlg=NULL;
	memset(m_dwBrdcstIP,0,sizeof(DWORD)*20);
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CIntraDoorApp object

CIntraDoorApp theApp;

int GetListCtrlItemImage(CListCtrl *plstCtrl, int iItem)
{
	LVITEM lv;
	lv.mask=LVIF_IMAGE;
	lv.iItem=iItem;
	lv.iSubItem=0;
	plstCtrl->GetItem(&lv);
	return lv.iImage;
}

void SetListCtrlItemImage(CListCtrl *plstCtrl, int iItem,int iImage)
{
	LVITEM lv;
	lv.mask=LVIF_IMAGE;
	lv.iItem=iItem;
	lv.iSubItem=0;
	lv.iImage=iImage;
	plstCtrl->SetItem(&lv);
}

/////////////////////////////////////////////////////////////////////////////
// CIntraDoorApp initialization
BOOL CIntraDoorApp::InitInstance()
{
	HWND hPrev=FindWindow("#32770",NAME_INTRADOOR);
	if(hPrev)
	{
		if(!::IsWindowVisible(hPrev))
			SendMessage(hPrev,WM_MYRESTORE,0,0);
		else if(::IsIconic(hPrev))
		{
			SendMessage(hPrev,WM_SYSCOMMAND,SC_RESTORE,0);
		}else
		{
			SetForegroundWindow(hPrev);
			SetActiveWindow(hPrev);
		}
		return FALSE;
	}

	WSADATA wsad;
	if(0!=WSAStartup(MAKEWORD(1,1),&wsad))
	{
		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		return FALSE;
	}
	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.


#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif
	int nLen=GetModuleFileName(NULL,m_szPath,MAX_PATH);
	while(m_szPath[nLen]!='\\') nLen--;
	m_szPath[nLen]=0;
	//将程序目录写入注册表
	HKEY hKey=0;
	if(ERROR_SUCCESS!=RegOpenKeyEx(HKEY_LOCAL_MACHINE,NAME_REGITEM,0,KEY_ALL_ACCESS,&hKey))
	{
		HKEY hKeySoftware=0;
		RegOpenKeyEx(HKEY_LOCAL_MACHINE,"Software",0,KEY_ALL_ACCESS,&hKeySoftware);
		ASSERT(hKeySoftware);
		RegCreateKey(hKeySoftware,strstr(NAME_REGITEM,"\\")+1,&hKey);
		RegCloseKey(hKeySoftware);
	}
	RegSetValueEx(hKey,"path",0,REG_SZ,(BYTE*)m_szPath,strlen(m_szPath)+1);
	RegCloseKey(hKey);
	if(!LoadUserInfo()) return FALSE;

	char szHostName[128];
	gethostname(szHostName,128);
	HOSTENT *phe=gethostbyname(szHostName);
	int i=0;
	while(phe->h_addr_list[i]) i++;
	m_pdwLocalIP=new DWORD[i+1];
	i=0;
	while(phe->h_addr_list[i]){
		memcpy(m_pdwLocalIP+i,phe->h_addr_list[i],4);
		i++;
	}
	m_pdwLocalIP[i]=0;

	m_pDlg=new CIntraDoorDlg;
	m_pDlg->Create(CIntraDoorDlg::IDD);
	m_pDlg->ShowWindow(SW_SHOWDEFAULT);
	m_pMainWnd=m_pDlg;
	return TRUE;
}

int CIntraDoorApp::ExitInstance() 
{
	if(m_pdwLocalIP) delete []m_pdwLocalIP;
	if(m_pDlg)
	{
		m_pDlg->DestroyWindow();
		delete m_pDlg;	
	}
	WSACleanup();
	return CWinApp::ExitInstance();
}

BOOL CIntraDoorApp::IsLocalIP(DWORD dwIP)
{
	if(dwIP==0x0100007f) return TRUE;
	if(!m_pdwLocalIP) return FALSE;
	int i=0;
	while(m_pdwLocalIP[i])
	{
		if(m_pdwLocalIP[i]==dwIP) return TRUE;
		i++;
	}
	return FALSE;
}

int CIntraDoorApp::RecvFrom(SOCKET s, char *szBuf, SOCKADDR *pAddr)
{
	//首先读12字节的包头
	int nLen=pAddr?sizeof(SOCKADDR):0;
	int nReaded=recvfrom(s,szBuf,512,0,pAddr,&nLen);
	nLen=0;
	while(nReaded<12)
	{
		int nGet=recvfrom(s,szBuf+nReaded,512-nReaded,0,NULL,&nLen);
		nReaded+=nGet;
	}
	//获取续数据长度
	DWORD dwLen=0;
	memcpy(&dwLen,szBuf,4);
	ASSERT(dwLen<=500);//512-12
	if(dwLen)
	{//读后续数据
		DWORD dwRemain=dwLen+12-nReaded;
		while(dwRemain)
		{
			int nGet=recvfrom(s,szBuf+nReaded,dwRemain,0,NULL,&nLen);
			dwRemain-=nGet;
			nReaded+=nGet;
		}
	}
	return nReaded;
}

int CIntraDoorApp::Recv(SOCKET s,char *szBuf, int nLen,DWORD dwTimeout)
{
	DWORD dwBegin=GetTickCount();
	int nReader=0;
	while(nReader<nLen)
	{
		int nGet=recv(s,szBuf+nReader,nLen-nReader,0);
		if(nGet==-1)
		{
			if(dwTimeout==INFINITE)
			{
				Sleep(100);
				continue;
			}
			DWORD dwEnd=GetTickCount();
			if(dwEnd-dwBegin>dwTimeout)
				break;
			else
				continue;
		}
		nReader+=nGet;
	}
	return nReader;
}

int CIntraDoorApp::Send(SOCKET s,char *szBuf, int nLen,DWORD dwTimeout)
{
	DWORD dwBegin=GetTickCount();
	int nSent=0;
	while(nSent<nLen)
	{
		int nPut=send(s,szBuf+nSent,nLen-nSent,0);
		if(nPut==-1)
		{
			if(dwTimeout==INFINITE)
			{
				Sleep(100);
				continue;
			}
			DWORD dwEnd=GetTickCount();
			if(dwEnd-dwBegin>dwTimeout)
				break;
			else
				continue;
		}
		nSent+=nPut;
	}
	return nSent;
}

//给指定地址发送一个指定UDP消息
BOOL CIntraDoorApp::SendMsg(DWORD dwIP,USHORT uPort, DWORD dwMainType, DWORD dwSubType, const LPVOID pData, DWORD dwSize)
{
	if(dwSize>500) return FALSE;
	SOCKET sock=socket(AF_INET,SOCK_DGRAM,0);
	char szMsgBuf[512];
	PIBMSG pMsg=(PIBMSG)szMsgBuf;
	pMsg->dwMainType=dwMainType;
	pMsg->dwSubType=dwSubType;
	pMsg->dwDataLen=dwSize;
	memcpy(pMsg->byData,pData,dwSize);
	SOCKADDR_IN addr;
	addr.sin_family=AF_INET;
	addr.sin_port=htons(uPort);
	addr.sin_addr.S_un.S_addr=dwIP;
	sendto(sock,szMsgBuf,dwSize+12,0,(SOCKADDR*)&addr,sizeof(SOCKADDR));
	closesocket(sock);
	return TRUE;
}

void CIntraDoorApp::BrdcstMsg(DWORD dwMainType, DWORD dwSubType, LPVOID pData, DWORD dwSize)
{
	SOCKET sockBcSend=socket(AF_INET,SOCK_DGRAM,0);
	SOCKADDR_IN addrSend;
	addrSend.sin_family=AF_INET;
	addrSend.sin_port=htons(PORT_DOOR);
	//设置该套接字为广播类型，
	DWORD optval=1;
	setsockopt(sockBcSend,SOL_SOCKET,SO_BROADCAST,(char *)&optval,sizeof(DWORD));

	char szMsgBuf[512];
	PIBMSG pMsg=(PIBMSG)szMsgBuf;
	pMsg->dwMainType=dwMainType;
	pMsg->dwSubType=dwSubType;
	pMsg->dwDataLen=dwSize;
	memcpy(pMsg->byData,pData,dwSize);

	int iBrdcst=0;
	while(m_dwBrdcstIP[iBrdcst])
	{
 		addrSend.sin_addr.S_un.S_addr=m_dwBrdcstIP[iBrdcst++];
 		sendto(sockBcSend,szMsgBuf,dwSize+12,0,(SOCKADDR*)&addrSend,sizeof(SOCKADDR));
	}

	closesocket(sockBcSend);	
}

BOOL CIntraDoorApp::LoadUserInfo()
{
	char szUserName[UIS_NAME+1]={0};
	DWORD dwLen=UIS_NAME+1;
	GetUserName(szUserName,&dwLen);
	char szUserInfoCfg[MAX_PATH];
	sprintf(szUserInfoCfg,"%s\\userinfo.ini",m_szPath);
	GetPrivateProfileString("USERINFO","GUID","",m_UserInfo.szGuid,UIS_GUID,szUserInfoCfg);
	GetPrivateProfileString("USERINFO","NAME",szUserName,m_UserInfo.szName,UIS_NAME,szUserInfoCfg);
	GetPrivateProfileString("USERINFO","DESC","",m_UserInfo.szDesc,UIS_DESC,szUserInfoCfg);
	GetPrivateProfileString("USERINFO","PHONE","",m_UserInfo.szPhone,UIS_PHONE,szUserInfoCfg);
	GetPrivateProfileString("USERINFO","GROUP","默认",m_UserInfo.szGroup,UIS_GROUP,szUserInfoCfg);
	m_UserInfo.cSex=GetPrivateProfileInt("USERINFO","SEX",2,szUserInfoCfg);
	m_UserInfo.cHideMe=GetPrivateProfileInt("USERINFO","HIDEME",0,szUserInfoCfg);
	m_UserInfo.szState[0]=0;
	if(m_UserInfo.szGuid[0]==0)
	{
		GUID guid;
		::CoInitialize(NULL);
		::CoCreateGuid(&guid);
		::CoUninitialize();
		sprintf(m_UserInfo.szGuid,"%08X-%04X-%04X-%02X%02X-%02X%02X%02X%02X%02X%02X",
			guid.Data1,
			guid.Data2,
			guid.Data3,
			guid.Data4[0],guid.Data4[1],
			guid.Data4[2],guid.Data4[3],guid.Data4[4],guid.Data4[5],guid.Data4[6],guid.Data4[7]);	
		WritePrivateProfileString("USERINFO","GUID",m_UserInfo.szGuid,szUserInfoCfg);
		return InputUserInfo();
	}
	return TRUE;
}

BOOL CIntraDoorApp::InputUserInfo()
{
	CUserInfoDlg dlg(&m_UserInfo);
	if(dlg.DoModal()==IDCANCEL) return FALSE;

	strcpy(m_UserInfo.szName,dlg.m_strName);
	strcpy(m_UserInfo.szDesc,dlg.m_strDesc);
	strcpy(m_UserInfo.szPhone,dlg.m_strPhone);
	strcpy(m_UserInfo.szGroup,dlg.m_strGroup);
	m_UserInfo.cSex=(char)dlg.m_nSex;
	char szUserInfoCfg[MAX_PATH];
	sprintf(szUserInfoCfg,"%s\\userinfo.ini",m_szPath);
	WritePrivateProfileString("USERINFO","NAME",m_UserInfo.szName,szUserInfoCfg);
	WritePrivateProfileString("USERINFO","DESC",m_UserInfo.szDesc,szUserInfoCfg);
	WritePrivateProfileString("USERINFO","PHONE",m_UserInfo.szPhone,szUserInfoCfg);
	WritePrivateProfileString("USERINFO","GROUP",m_UserInfo.szGroup,szUserInfoCfg);
	char szBuf[20];
	sprintf(szBuf,"%d",m_UserInfo.cSex);
	WritePrivateProfileString("USERINFO","SEX",szBuf,szUserInfoCfg);
	return TRUE;
}

#include   <Ws2tcpip.h>     

int CIntraDoorApp::GetBroadcastIP(SOCKET s, DWORD dwBrdcstIP[])
{
	INTERFACE_INFO   info[20]={0};//assume   20   interfaces   is   enough;   
    DWORD   dwRet;   
    char   buff[256];   
	dwBrdcstIP[0]=0;
	if   (s==INVALID_SOCKET) return -1;  
	if(0!=WSAIoctl(s,SIO_GET_INTERFACE_LIST, buff,256,info,sizeof(INTERFACE_INFO)*5+1,&dwRet,NULL,0)) return -1;
	int nRet=0;
	DWORD dwIP,dwMask;
	for(int   i=0;info[i].iiFlags!=0;i++)   
	{
		memcpy(&dwIP,&info[i].iiAddress.AddressIn.sin_addr,4);
		memcpy(&dwMask,&info[i].iiNetmask.AddressIn.sin_addr,4);
		dwBrdcstIP[nRet]=(dwIP&dwMask)|(~dwMask);
		nRet++;
	}
	return nRet;
}
