// intraDoorDlg.cpp : implementation file
//

#include "stdafx.h"
#include "intraDoor.h"
#include "intraDoorDlg.h"
#include "gamesvrdlg.h"
#include "GameSetupDirDlg.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About
#include "hyperlink.h"

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	CHyperLink	m_wndHomeSite;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	DDX_Control(pDX, IDC_HOMESITE, m_wndHomeSite);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIntraDoorDlg dialog

CIntraDoorDlg::CIntraDoorDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CIntraDoorDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CIntraDoorDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	for(int i=0;i<MAX_GAMESVR;i++)
	{
		m_GameSvr[i].dwThreadID=0;
		m_GameSvr[i].szName[0]=0;
	}
}

void CIntraDoorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CIntraDoorDlg)
	DDX_Control(pDX, IDC_TAB_GROUP, m_tabGroup);
	DDX_Control(pDX, IDC_LIST_GAME, m_lstGame);
	DDX_Control(pDX, IDC_LIST_USER, m_lstUser);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CIntraDoorDlg, CDialog)
	//{{AFX_MSG_MAP(CIntraDoorDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_DESTROY()
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_USER, OnDblclkListUser)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_USER, OnRclickListUser)
	ON_COMMAND(IDM_USER_CHAT, OnUserChat)
	ON_COMMAND(IDM_USER_SENDFILE, OnUserSendfile)
	ON_COMMAND(IDM_USER_SENDDIR, OnUserSenddir)
	ON_COMMAND(IDM_USER_REFRESH, OnUserRefresh)
	ON_COMMAND(IDM_USER_FIND, OnUserFind)
	ON_COMMAND(IDM_HELP_ABOUT, OnHelpAbout)
	ON_COMMAND(IDM_HELP_INDEX, OnHelpIndex)
	ON_COMMAND(IDM_SYS_QUIT, OnSysQuit)
	ON_COMMAND(IDM_SYS_SHOWMAIN, OnSysShowmain)
	ON_COMMAND(IDM_SYS_USERINFO, OnSysUserinfo)
	ON_COMMAND(IDM_WIN_GAMESVR, OnWinGameSvr)
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDM_GAME_START, OnGameNew)
	ON_BN_CLICKED(IDM_GAME_DEL, OnGameDel)
	ON_BN_CLICKED(IDC_STATE_SET, OnStateSet)
	ON_BN_CLICKED(IDC_HIDEME, OnHideme)
	ON_COMMAND(IDM_USER_BRDCST, OnUserBrdcst)
	ON_BN_CLICKED(IDM_GAME_REFRESH, OnGameRefresh)
	ON_WM_ENDSESSION()
	ON_NOTIFY(NM_RCLICK, IDC_LIST_GAME, OnRclickListGame)
	ON_BN_CLICKED(IDC_FILETRANS_MONITOR, OnFiletransMonitor)
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_GROUP, OnSelchangeTabGroup)
	ON_COMMAND(IDM_USER_CHATGROUP, OnUserChatgroup)
	//}}AFX_MSG_MAP
	ON_MESSAGE(UM_SOCKETMSG,OnSocketMsg)
	ON_MESSAGE(WM_SHELLNOTIFY,OnShellNotify)
	ON_MESSAGE(WM_MYRESTORE,OnMyRestore)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIntraDoorDlg message handlers
#define SIZE_COLUMN		6
#define CI_NAME		0
#define CI_GROUP	1
#define CI_TEL		2
#define CI_DESC		3
#define CI_STATE	4
#define CI_IP		5

char g_szColName[SIZE_COLUMN][20]=
{
	"名称",
	"分组",
	"电话",
	"备注",
	"状态",
	"IP",
};

int g_nDefColWid[SIZE_COLUMN]=
{
	100,
	100,
	80,
	200,
	100,
	120,
};

BOOL CIntraDoorDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	char szSetting[MAX_PATH];
	sprintf(szSetting,"%s\\settings.ini",theApp.m_szPath);
	// TODO: Add extra initialization here
	int i;
	for(i=0;i<SIZE_COLUMN;i++) m_lstUser.InsertColumn(i,g_szColName[i],50);
	LVCOLUMN lvc;
	lvc.mask=LVCF_ORDER|LVCF_TEXT|LVCF_WIDTH;
	lvc.cchTextMax=20;
	for( i=0;i<SIZE_COLUMN;i++)
	{
		lvc.pszText=g_szColName[i];
		lvc.iOrder=GetPrivateProfileInt("ORDER",g_szColName[i],i,szSetting);
		lvc.cx=GetPrivateProfileInt("WIDTH",g_szColName[i],g_nDefColWid[i],szSetting);
		m_lstUser.SetColumn(i,&lvc);
	}
	ListView_SetExtendedListViewStyle(m_lstUser.m_hWnd,LVS_EX_HEADERDRAGDROP);
	m_ilFace.Create(16,16,ILC_COLORDDB|ILC_MASK,0,0);
	CBitmap bmp;
	bmp.LoadBitmap(IDB_FACE);
	m_ilFace.Add(&bmp,RGB(255,0,255));
	m_lstUser.SetImageList(&m_ilFace,LVSIL_SMALL);

	m_lstGame.InsertColumn(0,"游戏",0,100);
	m_lstGame.InsertColumn(1,"版本",0,100);
	m_lstGame.InsertColumn(2,"ID",0,200);
	m_ilGame.Create(16,16,ILC_COLORDDB|ILC_MASK,0,0);
	m_lstGame.SetImageList(&m_ilGame,LVSIL_SMALL);
	OnGameRefresh();

	m_tabGroup.InsertItem(0,"全部");

	m_pFileTransDlg=new CFileTransDlg;
	m_pFileTransDlg->Create(CFileTransDlg::IDD,this);
	m_pFindDlg=new CFindDlg;
	m_pFindDlg->Create(CFindDlg::IDD,this);

	m_sockBcRecv=socket(AF_INET,SOCK_DGRAM,0);
	if(theApp.GetBroadcastIP(m_sockBcRecv,theApp.m_dwBrdcstIP)==-1)
	{
		MessageBox("获取广播IP地址失败");
		PostMessage(WM_QUIT);
		return TRUE;
	}
	
	SOCKADDR_IN addrRecv;
	addrRecv.sin_addr.S_un.S_addr=htonl(INADDR_ANY);
	addrRecv.sin_family=AF_INET;
	addrRecv.sin_port=htons(PORT_DOOR);

	int nNetRet=bind(m_sockBcRecv,(SOCKADDR*)&addrRecv,sizeof(SOCKADDR));
	if(nNetRet==-1)
	{
		MessageBox("绑定PORT失败");
		PostMessage(WM_QUIT);
		return TRUE;
	}
	ASSERT(nNetRet==0);
	nNetRet = WSAAsyncSelect(m_sockBcRecv, m_hWnd, UM_SOCKETMSG, FD_READ);
	OnUserRefresh();

	CheckDlgButton(IDC_HIDEME,theApp.m_UserInfo.cHideMe);
	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CIntraDoorDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CIntraDoorDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

LRESULT CIntraDoorDlg::OnSocketMsg(WPARAM wParam, LPARAM lParam)
{
	if(wParam == m_sockBcRecv)
	{//广播消息收到的数据
		if(LOWORD(lParam)==FD_READ)
		{
			char szBuf[512];
			SOCKADDR_IN addrSend;
			int		nLen=sizeof(SOCKADDR);
			int nRead=theApp.RecvFrom(m_sockBcRecv,szBuf,(SOCKADDR *)&addrSend);
			DWORD dwIP;
			memcpy(&dwIP,&addrSend.sin_addr.S_un.S_un_b,4);

			PIBMSG pMsg=(PIBMSG)szBuf;
			if(pMsg->dwMainType==MAIN_USER)
			{
				if(theApp.IsLocalIP(dwIP)) return 0;
				if(pMsg->dwSubType==SUB_USER_LOGIN)	
				{//上线
					USERINFO ui;
					if(!m_mapIP2UI.Lookup(dwIP,ui))
					{
						ExtractUserInfo((char*)pMsg->byData,pMsg->dwDataLen,ui);
						m_mapIP2UI[dwIP]=ui;
						UpdateGroupList();
						if(ui.cHideMe==0)	AddUser2List(dwIP,ui);
					}
					//将自己在线的信息发给新上线的用户
					char szMsgBuf[500];
					DWORD dwLen=FillUserInfo(szMsgBuf,theApp.m_UserInfo);
					theApp.SendMsg(dwIP,PORT_DOOR,MAIN_USER,SUB_USER_ONLINE,szMsgBuf,dwLen);
				}else if(pMsg->dwSubType==SUB_USER_LOGOFF)
				{//下线
					USERINFO ui={0};
					if(m_mapIP2UI.Lookup(dwIP,ui))
					{
						m_mapIP2UI.RemoveKey(dwIP);
						if(ui.cHideMe==0)
						{
							int iUser=FindUser(dwIP);
							if(iUser!=-1) m_lstUser.DeleteItem(iUser);
						}
					}
				}else if(pMsg->dwSubType==SUB_USER_ONLINE)
				{//在线
					USERINFO ui;
					ExtractUserInfo((char*)pMsg->byData,pMsg->dwDataLen,ui);
					m_mapIP2UI[dwIP]=ui;
					UpdateGroupList();
					if(ui.cHideMe == 0) AddUser2List(dwIP,ui);
				}else if(pMsg->dwSubType==SUB_USER_STATE)
				{//用户状态
					USERINFO ui;
					if(m_mapIP2UI.Lookup(dwIP,ui))
					{
						char *pszState=(char*)pMsg->byData;
						if(strlen(pszState)<=UIS_STATE)
						{
							strcpy(ui.szState,pszState);
							m_mapIP2UI[dwIP]=ui;
							UpdateGroupList();
							if(ui.cHideMe==0)
							{
								int iUser=FindUser(dwIP);
								if(iUser!=-1) m_lstUser.SetItemText(iUser,CI_STATE,pszState);
							}
						}
					}
				}else if(pMsg->dwSubType==SUB_USER_HIDEME)
				{//隐身
					USERINFO ui;
					if(m_mapIP2UI.Lookup(dwIP,ui))
					{
						ui.cHideMe=pMsg->byData[0];
						m_mapIP2UI[dwIP]=ui;
						UpdateGroupList();
						if(ui.cHideMe)
							RemoveUserFromList(dwIP);
						else
							AddUser2List(dwIP,ui);
					}
				}else if(pMsg->dwSubType==SUB_USER_USERINOFO)
				{//用户信息改变
					USERINFO ui;
					ExtractUserInfo((char*)pMsg->byData,pMsg->dwDataLen,ui);
					m_mapIP2UI[dwIP]=ui;
					UpdateGroupList();
					if(ui.cHideMe==0)	AddUser2List(dwIP,ui);
				}
			}else if(pMsg->dwMainType==MAIN_CHAT)
			{
				if(theApp.IsLocalIP(dwIP)) return 0;
				CChatDlg *pChatDlg=NULL;
				POSITION pos=m_lstChatDlg.GetHeadPosition();
				while(pos)
				{
					CChatDlg *pDlg=m_lstChatDlg.GetNext(pos);
					if(pDlg->m_dwIP==dwIP)
					{
						pChatDlg=pDlg;
						break;
					}
				}

				if(pChatDlg==NULL)
				{
					pChatDlg=new CChatDlg;
					pChatDlg->m_pMainDlg=this;
					pChatDlg->m_dwIP=dwIP;
					USERINFO ui;
					if(m_mapIP2UI.Lookup(dwIP,ui))	pChatDlg->m_strMateName=ui.szName;

					HICON hIcon=m_ilFace.ExtractIcon(ui.cSex);
					pChatDlg->Create(CChatDlg::IDD);
					pChatDlg->CenterWindow();
					pChatDlg->SetOwner(GetDesktopWindow());
					pChatDlg->SetIcon(hIcon,FALSE);
					m_lstChatDlg.AddTail(pChatDlg);
				}
				pChatDlg->BringWindowToTop();
				pChatDlg->AddSent(pChatDlg->m_strMateName,(char*)pMsg->byData);
			}else if(pMsg->dwMainType==MAIN_FILETRANS)
			{//传文件
				OnMyRestore(0,0);
				if(pMsg->dwSubType==SUB_FILETRANS_REQ)
				{//请求文件发送
					DWORD dwSize=0;
					memcpy(&dwSize,pMsg->byData,4);
					m_pFileTransDlg->AddTask((char*)pMsg->byData+4,dwIP,dwSize,1,0);
				}else if (pMsg->dwSubType==SUB_FILETRANS_DIR)
				{//请求目录发送
					DWORD dwSize=0;
					memcpy(&dwSize,pMsg->byData,4);
					int nFiles=0,nDirs=0;
					memcpy(&nFiles,pMsg->byData+4,4);
					memcpy(&nDirs,pMsg->byData+8,4);
					m_pFileTransDlg->AddTask((char*)pMsg->byData+12,dwIP,dwSize,nFiles,nDirs);
				}else if(pMsg->dwSubType==SUB_FILETRANS_ACK)
				{//对自己请求的回应
					char *pszFileName=(char*)pMsg->byData;
					char *pszNewFile=pszFileName+strlen(pszFileName)+1;
					if(pszNewFile[0]==0)
					{
						m_pFileTransDlg->UploadReject(pszFileName,dwIP);
					}else
					{
						m_pFileTransDlg->StartUpload(pszFileName,pszNewFile,dwIP);
					}
				}else if(pMsg->dwSubType==SUB_FILETRANS_DEL)
				{//文件传输请求被上传方取消
					m_pFileTransDlg->DelTask((char*)pMsg->byData,dwIP);
				}else if(pMsg->dwSubType==SUB_FILETRANS_GAME)
				{//传游戏目录,开始监听后自动发送应答消息
					DWORD dwSize=0;
					memcpy(&dwSize,pMsg->byData,4);
					int nFiles=0,nDirs=0;
					memcpy(&nFiles,pMsg->byData+4,4);
					memcpy(&nDirs,pMsg->byData+8,4);
					char *pszPath=(char*)pMsg->byData+12;
					char *pszName=pszPath+strlen(pszPath)+1;
					//获得邀请消息数据
					char *pszInviteMsgBuf=pszName+strlen(pszName)+1;
					DWORD dwInviteMsgLen=pMsg->dwDataLen-(pszInviteMsgBuf-(char*)pMsg->byData);
					char szFullPath[MAX_PATH];
					sprintf(szFullPath,"%s\\game\\%s",theApp.m_szPath,pszName);
					m_pFileTransDlg->AddDownloadGame(dwIP,szFullPath,dwSize,nFiles,nDirs,pszInviteMsgBuf,dwInviteMsgLen);
					//向上传方发送一个确认接收消息
					char szBuf[500];
					strcpy(szBuf,pszPath);
					int nLen=strlen(pszPath)+1;
					strcpy(szBuf+nLen,szFullPath);
					nLen+=strlen(szFullPath)+1;
					theApp.SendMsg(dwIP,PORT_DOOR,MAIN_FILETRANS,SUB_FILETRANS_ACK,szBuf,nLen);
				}
			}else if(pMsg->dwMainType==MAIN_GAME)
			{//游戏消息
				if(pMsg->dwSubType==SUB_GAME_USERINFO)
				{//查询自己的信息
					char szMsgBuf[500];
					DWORD dwLen=FillUserInfo(szMsgBuf,theApp.m_UserInfo);
					theApp.SendMsg(dwIP,PORT_CLIENT,MAIN_GAME,SUB_GAME_USERINFO,szMsgBuf,dwLen);
				}else if(pMsg->dwSubType==SUB_GAME_QUERYMATE)
				{//查询用户信息
					addrSend.sin_port=htons(PORT_CLIENT);
					SOCKET sock=socket(AF_INET,SOCK_DGRAM,0);
					char szMsgBuf[512];
					PIBMSG pMsg=(PIBMSG)szMsgBuf;
					pMsg->dwMainType=MAIN_GAME;
					pMsg->dwSubType=SUB_GAME_QUERYMATE;
					POSITION pos=m_mapIP2UI.GetStartPosition();
					DWORD dwIP;
					USERINFO ui;
					while(pos)
					{
						m_mapIP2UI.GetNextAssoc(pos,dwIP,ui);
						memcpy(szMsgBuf+12,&dwIP,4);
						pMsg->dwDataLen=4+FillUserInfo(szMsgBuf+16,ui);
						sendto(sock,szMsgBuf,pMsg->dwDataLen+12,0,(SOCKADDR*)&addrSend,sizeof(SOCKADDR));
						Sleep(10);//必须sleep以避免阻塞
					}
					//发送一个结束包
					memset(pMsg->byData,0,4);
					pMsg->dwDataLen=4;
					sendto(sock,szMsgBuf,16,0,(SOCKADDR*)&addrSend,sizeof(SOCKADDR));
					closesocket(sock);
				}else if(pMsg->dwSubType==SUB_GAME_INVITE)
				{//邀请游戏:
					OnMyRestore(0,0);
					SHORT sPort;
					char  cSeat;
					char *pbyData=(char *)pMsg->byData;
					char *pszSvrGuid=pbyData;
					pbyData+=strlen(pbyData)+1;
					memcpy(&sPort,pbyData,2);
					sPort=ntohs(sPort);
					pbyData+=2;
					cSeat=pbyData[0];
					pbyData++;
					char *pszGameID=pbyData;
					pbyData+=strlen(pbyData)+1;
					char *pszGameName=pbyData;
					pbyData+=strlen(pbyData)+1;
					DWORD dwVer=0;
					memcpy(&dwVer,pbyData,4);
					pbyData+=4;
					char *pszGameDir=pbyData;
					pbyData+=strlen(pbyData)+1;
					//在游戏安装目录中查找是否已经安装该游戏
					int iGame=GUID2GameInfo(pszGameID);
					if(iGame!=-1)
					{//已经安装了这个游戏
						GAMEINFO *pgi=(GAMEINFO *)m_lstGame.GetItemData(iGame);
						if(pgi->dwVersion!=dwVer)
						{
							theApp.SendMsg(dwIP,PORT_DOOR,MAIN_GAME,SUB_GAME_VERERROR,NULL,0);
						}else
						{
							//通过邀请者IP获取邀请者名称
							char szSvrName[100]={0};
							GetHostName(&addrSend,szSvrName);
							char szTip[500];
							sprintf(szTip,"%s 邀请您一起玩[%s],是否接受?",szSvrName,pszGameName);
							if(MessageBox(szTip,NULL,MB_YESNO|MB_ICONQUESTION)==IDYES)
							{
								char szGamePath[MAX_PATH],szStartInfo[200];
								sprintf(szGamePath,"%s\\game\\%s\\%s",theApp.m_szPath,pgi->szPath,pgi->szClient);
								sprintf(szStartInfo,"%s:%u|%d",pszSvrGuid,sPort,cSeat);
								ShellExecute(m_hWnd,"open",szGamePath,szStartInfo,NULL,SW_SHOW);
							}else
							{//回应谢绝消息
								theApp.SendMsg(dwIP,PORT_DOOR,MAIN_GAME,SUB_GAME_REFUSE,NULL,0);
							}
						}
					}else
					{//没有找到安装程序或者安装程序被破坏
						char szMsgBuf[512];
						PIBMSG pMsgAck=(PIBMSG)szMsgBuf;
						pMsgAck->dwDataLen=0;
						pMsgAck->dwMainType=MAIN_GAME;
						char szSvrName[100]={0};
						GetHostName(&addrSend,szSvrName);
						CGameSetupDirDlg	setupDlg;
						setupDlg.m_strDir=pszGameDir;
						setupDlg.m_strTip.Format("%s 邀请您一起玩 %s,但没有找到安装程序或者安装程序被破坏,是否从%s处请求下载?",szSvrName,pszGameName,szSvrName);
						if(setupDlg.DoModal()==IDOK)
						{
							//通知邀请方开始上传
							pMsgAck->dwSubType=SUB_GAME_REQUESTSETUP;
							char *pszData=(char*)pMsgAck->byData;
							strcpy(pszData,pszGameID);
							pszData+=strlen(pszData)+1;
							strcpy(pszData,setupDlg.m_strDir);
							pszData+=strlen(pszData)+1;
							//将邀请消息数据附加到该消息尾部
							memcpy(pszData,pMsg->byData,pMsg->dwDataLen);
							pszData+=pMsg->dwDataLen;
							pMsgAck->dwDataLen=pszData-(char*)pMsgAck->byData;;
						}else
						{
							pMsgAck->dwSubType=SUB_GAME_REFUSE;
							pMsgAck->dwDataLen=0;
						}
						addrSend.sin_port=htons(PORT_DOOR);
						SOCKET sock=socket(AF_INET,SOCK_DGRAM,0);
						sendto(sock,szMsgBuf,pMsgAck->dwDataLen+12,0,(SOCKADDR*)&addrSend,sizeof(SOCKADDR));
						closesocket(sock);
					}
				}else if(pMsg->dwSubType==SUB_GAME_REFUSE)
				{//谢绝邀请
					OnMyRestore(0,0);
					char szName[100];
					GetHostName(&addrSend,szName);
					char szTip[200];
					sprintf(szTip,"%s 谢绝了您的邀请",szName);
					MessageBox(szTip);
				}else if(pMsg->dwSubType==SUB_GAME_VERERROR)
				{
					OnMyRestore(0,0);
					char szName[100];
					GetHostName(&addrSend,szName);
					char szTip[200];
					sprintf(szTip,"%s 由于游戏版本不同谢绝了您的邀请",szName);
					MessageBox(szTip);
				}else if(pMsg->dwSubType==SUB_GAME_REQUESTSETUP)
				{//下载安装程序
					char *pszGuid=(char*)pMsg->byData;
					char *pszPath=pszGuid+strlen(pszGuid)+1;
					//分离出游戏邀请消息的数据
					char *pszInviteMsg=pszPath+strlen(pszPath)+1;
					DWORD dwInviteMsgLen=pMsg->dwDataLen-(pszInviteMsg-(char*)pMsg->byData);
					int iGame=GUID2GameInfo(pszGuid);
					if(iGame!=-1)
					{
						GAMEINFO *pgi=(GAMEINFO*)m_lstGame.GetItemData(iGame);
						char szGamePath[MAX_PATH];
						sprintf(szGamePath,"%s\\game\\%s",theApp.m_szPath,pgi->szPath);
						ReqUploadGame(dwIP,szGamePath,pszPath,pszInviteMsg,dwInviteMsgLen);
					}
				}else if(pMsg->dwSubType==SUB_GAME_SVREND)
				{//游戏服务器退出
					USHORT uPort;
					memcpy(&uPort,pMsg->byData,2);
					if(uPort>=PORT_SVRBASE && uPort<PORT_SVRBASE+MAX_GAMESVR)
					{
						m_GameSvr[uPort-PORT_SVRBASE].szName[0]=0;
						m_GameSvr[uPort-PORT_SVRBASE].dwThreadID=0;
					}
				}else if(pMsg->dwSubType==SUB_GAME_SERVERIP)
				{//查询服务器IP
					char *pszSvrGuid=(char*)pMsg->byData;
					DWORD dwSvrIP=-1;
					if(strcmp(pszSvrGuid,theApp.m_UserInfo.szGuid)==0)
					{
						dwSvrIP=inet_addr("127.0.0.1");
					}else
					{
						POSITION pos=m_mapIP2UI.GetStartPosition();
						USERINFO ui;
						DWORD    dwIP;
						while(pos)
						{
							m_mapIP2UI.GetNextAssoc(pos,dwIP,ui);
							if(strcmp(ui.szGuid,pszSvrGuid)==0) dwSvrIP=dwIP;
						}
					}

					char szMsgBuf[512];
					PIBMSG pMsg=(PIBMSG)szMsgBuf;
					pMsg->dwMainType=MAIN_GAME;
					pMsg->dwSubType=SUB_GAME_SERVERIP;
					memcpy(pMsg->byData,&dwSvrIP,4);	//-1表示查询失败
					pMsg->dwDataLen=4;

					addrSend.sin_port=htons(PORT_CLIENT);
					SOCKET sock=socket(AF_INET,SOCK_DGRAM,0);
					sendto(sock,szMsgBuf,16,0,(SOCKADDR*)&addrSend,sizeof(SOCKADDR));
					closesocket(sock);
					
				}
			}
		}
	}
	return 0;
}

void CIntraDoorDlg::OnDestroy() 
{
	SaveState();
	CDialog::OnDestroy();
}

DWORD CIntraDoorDlg::FillUserInfo(char *pszBuf,USERINFO ui)
{
	char *pbyData=pszBuf;
	//guid
	strcpy(pbyData,ui.szGuid);
	pbyData+=strlen(ui.szGuid)+1;
	//name
	strcpy(pbyData,ui.szName);
	pbyData+=strlen(ui.szName)+1;
	//phone
	strcpy(pbyData,ui.szPhone);
	pbyData+=strlen(ui.szPhone)+1;
	//desc
	strcpy(pbyData,ui.szDesc);
	pbyData+=strlen(ui.szDesc)+1;
	//sex
	pbyData[0]=ui.cSex;
	pbyData+=1;
	//hideme
	pbyData[0]=ui.cHideMe;
	pbyData+=1;
	//state
	strcpy(pbyData,ui.szState);
	pbyData+=strlen(ui.szState)+1;
	//group
	strcpy(pbyData,ui.szGroup);
	pbyData+=strlen(ui.szGroup)+1;
	return (pbyData-pszBuf);
}

void CIntraDoorDlg::ExtractUserInfo(char *pszData,int nSize,USERINFO &ui)
{
	char *pszBuf=pszData;
	//guid
	strcpy(ui.szGuid,pszBuf);
	pszBuf+=strlen(pszBuf)+1;
	//name
	strcpy(ui.szName,pszBuf);
	pszBuf+=strlen(pszBuf)+1;
	//phone
	strcpy(ui.szPhone,pszBuf);
	pszBuf+=strlen(pszBuf)+1;
	//desc
	strcpy(ui.szDesc,pszBuf);
	pszBuf+=strlen(pszBuf)+1;
	//sex
	ui.cSex=pszBuf[0];
	pszBuf++;
	//hideme
	ui.cHideMe=pszBuf[0];
	pszBuf++;
	//state
	strcpy(ui.szState,pszBuf);
	pszBuf+=strlen(pszBuf)+1;
	//group
	strcpy(ui.szGroup,"默认");
	if(pszBuf-pszData<nSize)
	{//ver 1.5
		strcpy(ui.szGroup,pszBuf);
		pszBuf+=strlen(pszBuf)+1;
	}
}

void CIntraDoorDlg::OnDblclkListUser(NMHDR* pNMHDR, LRESULT* pResult) 
{
	OnUserChat();
	*pResult = 0;
}

//获取文件的版本号
DWORD PEVersion(LPCTSTR pszFileName)
{
	VS_FIXEDFILEINFO *pstFileVersion;
	DWORD dwResHandle;
	DWORD dwVerInfoSize=GetFileVersionInfoSize((char*)pszFileName,&dwResHandle);
	if(!dwVerInfoSize) return 0;
	void *pBuf=HeapAlloc(GetProcessHeap(),0,dwVerInfoSize);
	GetFileVersionInfo((char*)pszFileName, dwResHandle, dwVerInfoSize, pBuf); 
	DWORD dwVer=0;
	UINT nVersionLen;
	if(VerQueryValue(pBuf, "\\", (void**)&pstFileVersion, &nVersionLen) && nVersionLen>=sizeof(VS_FIXEDFILEINFO) )
	{
		dwVer=MAKELONG(MAKEWORD(LOWORD(pstFileVersion->dwFileVersionLS),HIWORD(pstFileVersion->dwFileVersionLS)),
			MAKEWORD(LOWORD(pstFileVersion->dwFileVersionMS),HIWORD(pstFileVersion->dwFileVersionMS)));
	}
	HeapFree(GetProcessHeap(),0,pBuf);
	return dwVer;
}


BOOL CIntraDoorDlg::AddGame(GAMEINFO &gi)
{
	char szClientPath[MAX_PATH];
	sprintf(szClientPath,"%s\\game\\%s\\%s",theApp.m_szPath,gi.szPath,gi.szClient);
	HICON hIcon=NULL;
	UINT nIcons=ExtractIconEx(szClientPath,0,NULL,&hIcon,1);
	if(nIcons==1)
	{
		gi.dwVersion=PEVersion(szClientPath);
		int index=m_ilGame.Add(hIcon);
		m_lstGame.InsertItem(index,gi.szName,index);
		char szBuf[100];
		sprintf(szBuf,"%u.%u.%u.%u",(gi.dwVersion>>24)&0xFF,(gi.dwVersion>>16)&0xFF,(gi.dwVersion>>8)&0xFF,gi.dwVersion&0xFF);
		m_lstGame.SetItemText(index,1,szBuf);
		m_lstGame.SetItemText(index,2,gi.szGuid);
		GAMEINFO *pgi=new GAMEINFO;
		*pgi=gi;
		m_lstGame.SetItemData(index,(DWORD)pgi);
		DeleteObject(hIcon);
		return TRUE;
	}
	return FALSE;
}

void CIntraDoorDlg::GetHostName(SOCKADDR_IN *pAddr, char *pszName)
{
	DWORD dwIP;
	memcpy(&dwIP,&pAddr->sin_addr,4);
	pszName[0]=0;
	//通过服务器IP获取邀请者名称
	USERINFO ui={0};
	if(m_mapIP2UI.Lookup(dwIP,ui))
	{//从用户表中查询邀请者
		strcpy(pszName,ui.szName);
	}else
	{//直接通过IP获得邀请者
		char szSvrIP[100];
		strcpy(szSvrIP,inet_ntoa(pAddr->sin_addr));
		HOSTENT *phe=gethostbyaddr(szSvrIP,strlen(szSvrIP),AF_INET);
		strcpy(pszName,phe->h_name);
	}
}

//在游戏安装目录中查找是否已经安装该游戏
int CIntraDoorDlg::GUID2GameInfo(char *pszGuid)
{
	for(int i=0;i<m_lstGame.GetItemCount();i++)
	{
		GAMEINFO *pgi=(GAMEINFO*)m_lstGame.GetItemData(i);
		if(strcmp(pgi->szGuid,pszGuid)==0) return i;
	}
	return -1;
}

//发送一个文件传输请求
void CIntraDoorDlg::ReqUploadFile(DWORD dwIP, LPCTSTR pszFileName)
{
	char szMsgBuf[500];
	CFileStatus fs;
	CFile::GetStatus(pszFileName,fs);
	memcpy(szMsgBuf,&fs.m_size,4);
	strcpy(szMsgBuf+4,pszFileName);
	int nLen=4+strlen(pszFileName)+1;
	if(m_pFileTransDlg->AddUpload(dwIP,pszFileName,fs.m_size,1,0))
		theApp.SendMsg(dwIP,PORT_DOOR,MAIN_FILETRANS,SUB_FILETRANS_REQ,szMsgBuf,nLen);
	else
		MessageBox("已经有一个相同的上传任务在上传队列中");
}


USHORT CIntraDoorDlg::GetSvrPort()
{
	for(int i=0;i<MAX_GAMESVR;i++)
	{
		if(m_GameSvr[i].dwThreadID==0) return PORT_SVRBASE+i;
	}
	return 0;
}


//计算目录中文件总容量
DWORD CIntraDoorDlg::CalcDir(LPCTSTR pszDirName,int &nFiles,int &nDirs)
{
	DWORD dwSize=0;
	CFileFind find;
	char szBuf[MAX_PATH];
	sprintf(szBuf,"%s\\*.*",pszDirName);
	nDirs++;
	BOOL bFind=find.FindFile(szBuf);
	while(bFind)
	{
		bFind=find.FindNextFile();
		if(find.IsDirectory()) continue;
		dwSize+=find.GetLength();
		nFiles++;
	}
	find.Close();
	bFind=find.FindFile(szBuf);
	while(bFind)
	{
		bFind=find.FindNextFile();
		if(find.IsDirectory() && find.IsDots()==FALSE)
		{
			dwSize+=CalcDir(find.GetFilePath(),nFiles,nDirs);
		}
	}
	find.Close();
	return dwSize;
}

void CIntraDoorDlg::ReqUploadDir(DWORD dwIP, LPCTSTR pszDirName)
{
	int nFiles=0,nDirs=0;
	DWORD dwSize=CalcDir(pszDirName,nFiles,nDirs);
	char byBuf[500];
	memcpy(byBuf,&dwSize,4);
	memcpy(byBuf+4,&nFiles,4);
	memcpy(byBuf+8,&nDirs,4);
	strcpy(byBuf+12,pszDirName);
	if(m_pFileTransDlg->AddUpload(dwIP,pszDirName,dwSize,nFiles,nDirs))
		theApp.SendMsg(dwIP,PORT_DOOR,MAIN_FILETRANS,SUB_FILETRANS_DIR,byBuf,12+strlen(pszDirName)+1);
	else
		MessageBox("已经有一个相同的上传任务在上传队列中");
}

void CIntraDoorDlg::ReqUploadGame(DWORD dwIP, LPCTSTR pszDirName,LPCTSTR pszNewDirName,char * pszInviteMsg,DWORD dwInviteMsgLen)
{
	int nFiles=0,nDirs=0;
	DWORD dwSize=CalcDir(pszDirName,nFiles,nDirs);
	char byBuf[500];
	char *pbyData=byBuf;
	memcpy(pbyData,&dwSize,4);
	pbyData+=4;
	memcpy(pbyData,&nFiles,4);
	pbyData+=4;
	memcpy(pbyData,&nDirs,4);
	pbyData+=4;
	strcpy(pbyData,pszDirName);
	pbyData+=strlen(pszDirName)+1;
	strcpy(pbyData,pszNewDirName);
	pbyData+=strlen(pszNewDirName)+1;
	//附加邀请消息数据
	memcpy(pbyData,pszInviteMsg,dwInviteMsgLen);
	pbyData+=dwInviteMsgLen;
	if(m_pFileTransDlg->AddUpload(dwIP,pszDirName,dwSize,nFiles,nDirs))
		theApp.SendMsg(dwIP,PORT_DOOR,MAIN_FILETRANS,SUB_FILETRANS_GAME,byBuf,pbyData-byBuf);
	else
		MessageBox("已经有一个相同的上传任务在上传队列中");
}

void CIntraDoorDlg::AddUser2List(DWORD dwIP,USERINFO ui)
{
	int iCurGroup=m_tabGroup.GetCurSel();
	char szGroup[UIS_GROUP+1];
	TCITEM tItem;
	tItem.mask=TCIF_TEXT;
	tItem.cchTextMax=UIS_GROUP+1;
	tItem.pszText=szGroup;
	m_tabGroup.GetItem(iCurGroup,&tItem);
	if(iCurGroup!=0 && strcmp(ui.szGroup,szGroup)!=0) return;

	int iUser=FindUser(dwIP);

	//name
	if(iUser==-1)
	{
		iUser=m_lstUser.GetItemCount();
		m_lstUser.InsertItem(iUser,ui.szName,ui.cSex);
	}else
	{
		m_lstUser.SetItemText(iUser,CI_NAME,ui.szName);
		SetListCtrlItemImage(&m_lstUser,iUser,ui.cSex);
	}
	//group
	m_lstUser.SetItemText(iUser,CI_GROUP,ui.szGroup);
	//phone
	m_lstUser.SetItemText(iUser,CI_TEL,ui.szPhone);
	//desc
	m_lstUser.SetItemText(iUser,CI_DESC,ui.szDesc);
	//state
	m_lstUser.SetItemText(iUser,CI_STATE,ui.szState);
	//ip
	char szBuf[50];
	sprintf(szBuf,"%u.%u.%u.%u",dwIP&0xFF,(dwIP>>8)&0xFF,(dwIP>>16)&0xFF,(dwIP>>24)&0xFF);
	m_lstUser.SetItemText(iUser,CI_IP,szBuf);
	//set data
	m_lstUser.SetItemData(iUser,dwIP);
}

void CIntraDoorDlg::RemoveUserFromList(DWORD dwIP)
{
	int iUser=FindUser(dwIP);
	if(iUser!=-1) m_lstUser.DeleteItem(iUser);
}

void CIntraDoorDlg::OnRclickListUser(NMHDR* pNMHDR, LRESULT* pResult) 
{
	CMenu menu;
	menu.LoadMenu(IDM_INTRADOOR);
	POINT pt;
	GetCursorPos(&pt);
	CMenu *pSubMenu=menu.GetSubMenu(1);
	int iUser=m_lstUser.GetSelectionMark();
	if(iUser==-1)
	{
		pSubMenu->EnableMenuItem(IDM_USER_CHAT,MF_BYCOMMAND|MF_GRAYED);
		pSubMenu->EnableMenuItem(IDM_USER_SENDFILE,MF_BYCOMMAND|MF_GRAYED);
		pSubMenu->EnableMenuItem(IDM_USER_SENDDIR,MF_BYCOMMAND|MF_GRAYED);
	}
	pSubMenu->TrackPopupMenu(0,pt.x,pt.y,this);
	*pResult = 0;
}

void CIntraDoorDlg::OnRclickListGame(NMHDR* pNMHDR, LRESULT* pResult) 
{
	CMenu menu;
	menu.LoadMenu(IDM_INTRADOOR);
	POINT pt;
	GetCursorPos(&pt);
	CMenu *pSubMenu=menu.GetSubMenu(2);
	pSubMenu->TrackPopupMenu(0,pt.x,pt.y,this);
	*pResult = 0;
}

void CIntraDoorDlg::OnUserChat() 
{
	int iItem=m_lstUser.GetSelectionMark();
	if(iItem!=-1)
	{
		DWORD dwIP=m_lstUser.GetItemData(iItem);
		CChatDlg *pChatDlg=NULL;
		POSITION pos=m_lstChatDlg.GetHeadPosition();
		while(pos)
		{
			CChatDlg *pDlg=m_lstChatDlg.GetNext(pos);
			if(pDlg->m_dwIP==dwIP)
			{
				pChatDlg=pDlg;
				break;
			}
		}
		if(pChatDlg==NULL)
		{
			pChatDlg=new CChatDlg;
			pChatDlg->m_pMainDlg=this;
			pChatDlg->m_dwIP=dwIP;
			pChatDlg->m_strMateName=m_lstUser.GetItemText(iItem,0);
			int ilFace=GetListCtrlItemImage(&m_lstUser,iItem);
			HICON hIcon=m_ilFace.ExtractIcon(ilFace);
			pChatDlg->Create(CChatDlg::IDD);
			pChatDlg->CenterWindow();
			pChatDlg->SetOwner(GetDesktopWindow());
			pChatDlg->SetIcon(hIcon,FALSE);
			m_lstChatDlg.AddTail(pChatDlg);
		}
		pChatDlg->BringWindowToTop();

	}	
}

void CIntraDoorDlg::OnUserSendfile() 
{
	int nSel=m_lstUser.GetSelectionMark();
	if(nSel==-1)
	{
		MessageBox("还没有指定收文件用户!");
		return ;
	}
	CFileDialog openDlg(TRUE,NULL,NULL,OFN_ENABLESIZING|OFN_ALLOWMULTISELECT ,"所有文件(*.*)|*.*||");
	if(openDlg.DoModal()==IDOK)
	{
		DWORD dwIP=m_lstUser.GetItemData(nSel);
		POSITION pos=openDlg.GetStartPosition();
		while(pos)
		{
			CString strFile=openDlg.GetNextPathName(pos);
			ReqUploadFile(dwIP,strFile);
		}
	}	
}

#include "dirDialog.h"

void CIntraDoorDlg::OnUserSenddir() 
{
	int nSel=m_lstUser.GetSelectionMark();
	if(nSel==-1)
	{
		MessageBox("还没有指定收文件用户!");
		return ;
	}
	CDirDialog dirpick;
	if(dirpick.DoBrowse(this)==IDOK)
	{
		DWORD dwIP=m_lstUser.GetItemData(nSel);
		ReqUploadDir(dwIP,dirpick.m_strPath);
	}	
}

void CIntraDoorDlg::OnUserRefresh() 
{
	m_mapIP2UI.RemoveAll();
	m_lstUser.DeleteAllItems();
	char szMsgBuf[500];
	DWORD dwLen=FillUserInfo(szMsgBuf,theApp.m_UserInfo);
	theApp.BrdcstMsg(MAIN_USER,SUB_USER_LOGIN,szMsgBuf,dwLen);
}

void CIntraDoorDlg::OnUserFind() 
{
	m_pFindDlg->CenterWindow(this);
	m_pFindDlg->ShowWindow(SW_SHOW);	
}

void CIntraDoorDlg::OnHelpAbout() 
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

void CIntraDoorDlg::OnHelpIndex() 
{
	char szHelp[MAX_PATH];
	sprintf(szHelp,"%s\\intradoor.chm",theApp.m_szPath);
	if(GetFileAttributes(szHelp)==-1)
		MessageBox("没有找到帮助文件");
	else
		ShellExecute(m_hWnd,"open",szHelp,NULL,NULL,SW_SHOW);	
}

void CIntraDoorDlg::OnSysQuit() 
{
	PostQuitMessage(0);	
}

void CIntraDoorDlg::OnSysShowmain() 
{
	OnMyRestore(0,0);
}

void CIntraDoorDlg::OnSysUserinfo() 
{
	if(theApp.InputUserInfo())
	{
		char szMsgBuf[500];
		DWORD dwLen=FillUserInfo(szMsgBuf,theApp.m_UserInfo);
		theApp.BrdcstMsg(MAIN_USER,SUB_USER_USERINOFO,szMsgBuf,dwLen);
	}
}

void CIntraDoorDlg::ShowTray(BOOL bShow)
{
	NOTIFYICONDATA	notifyicon;
	notifyicon.cbSize=sizeof(NOTIFYICONDATA);
	notifyicon.hWnd=m_hWnd;
	notifyicon.uID=124;
	if(bShow)
	{
		notifyicon.uFlags=NIF_ICON|NIF_MESSAGE|NIF_TIP;
		notifyicon.hIcon=theApp.LoadIcon(IDR_MAINFRAME);
		notifyicon.uCallbackMessage=WM_SHELLNOTIFY;
		sprintf(notifyicon.szTip,"启程内网快车1.0");
		Shell_NotifyIcon(NIM_ADD,&notifyicon);
	}else
	{
		Shell_NotifyIcon(NIM_DELETE,&notifyicon);
	}
}

LRESULT CIntraDoorDlg::OnMyRestore(WPARAM, LPARAM)
{
	if(!IsWindowVisible())
	{
		ShowTray(FALSE);
		ShowWindow(SW_SHOW);
		SendMessage(WM_SYSCOMMAND,SC_RESTORE);
	}else
	{
		SetForegroundWindow();
	}
	return 0;
}

LRESULT CIntraDoorDlg::OnShellNotify(WPARAM wParam, LPARAM lParam)
{
	switch(lParam)
	{
	case WM_LBUTTONDBLCLK:
		OnMyRestore(0,0);
		break;
	case WM_RBUTTONUP:
		{
			CMenu menu;
			menu.LoadMenu(IDM_SYSTEM);
			CMenu *pMenu=menu.GetSubMenu(0);
			POINT pt;
			GetCursorPos(&pt);
			this->SetForegroundWindow();
			pMenu->TrackPopupMenu(0,pt.x,pt.y,this,NULL);
			this->PostMessage(WM_NULL);
		}
		break;
	}
	return 0;
}        

void CIntraDoorDlg::OnWinGameSvr() 
{
	CGameSvrDlg svrDlg(this);
	svrDlg.DoModal();			
}

void CIntraDoorDlg::OnClose() 
{
	SendMessage(WM_SYSCOMMAND,SC_MINIMIZE,0);
	ShowWindow(SW_HIDE);
	ShowTray(TRUE);
}

void CIntraDoorDlg::OnGameNew() 
{
	USHORT uPort=GetSvrPort();
	if(uPort==0)
	{
		MessageBox("没有可用的服务器端口");
		return ;
	}
	int iGame=m_lstGame.GetSelectionMark();
	if(iGame==-1)
	{
		MessageBox("请先选择游戏");
		return;
	}
	GAMEINFO *pgi=(GAMEINFO*)m_lstGame.GetItemData(iGame);
	//启动游戏服务器，并自动在0号座位登陆一个客户端
	char szPath[MAX_PATH],szCmdLine[200];
	sprintf(szPath,"%s\\game\\%s\\%s",theApp.m_szPath,pgi->szPath,pgi->szServer);
	sprintf(szCmdLine,"%u",uPort);
	DWORD dwAttr=GetFileAttributes(szPath);
	if(dwAttr==-1 || dwAttr==FILE_ATTRIBUTE_DIRECTORY)
	{//error
		MessageBox("没有找到游戏服务器程序");
		return ;
	}
	//启动Server
	STARTUPINFO         si;    
	PROCESS_INFORMATION pi;         
	DWORD dwWaitRet=1;
	memset(&si, 0, sizeof(si));     
	si.cb  = sizeof(si);  
	if(!CreateProcess(szPath,szCmdLine,NULL,NULL,FALSE,0,NULL,NULL,&si,&pi))
	{
		MessageBox("启动游戏服务器失败");
		return ;
	}
	//等待服务器初始化完成
	dwWaitRet=WaitForInputIdle(pi.hProcess, 10000); 
	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);
	m_GameSvr[uPort-PORT_SVRBASE].dwThreadID=pi.dwThreadId;
	//启动Client
	sprintf(szPath,"%s\\game\\%s\\%s",theApp.m_szPath,pgi->szPath,pgi->szClient);
	sprintf(szCmdLine,"%s:%u|0",theApp.m_UserInfo.szGuid,uPort);
	if(!CreateProcess(szPath,szCmdLine,NULL,NULL,FALSE,0,NULL,NULL,&si,&pi))
	{
		MessageBox("启动游戏失败");
		return ;
	}
	//不需要等待游戏客户端，否则将会造成死锁
	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);
	strcpy(m_GameSvr[uPort-PORT_SVRBASE].szClientPath,szPath);
	strcpy(m_GameSvr[uPort-PORT_SVRBASE].szName,pgi->szName);
}

void CIntraDoorDlg::OnGameDel() 
{
	int iGame=m_lstGame.GetSelectionMark();
	if(iGame==-1)
	{
		MessageBox("请先选择游戏");
		return;
	}
	GAMEINFO *pgi=(GAMEINFO*)m_lstGame.GetItemData(iGame);
	char szTip[500];
	sprintf(szTip,"确定要删除游戏:%s 版本:%u.%u.%u.%u 吗?",pgi->szName,(pgi->dwVersion>>24)&0xFF,(pgi->dwVersion>>16)&0xFF,(pgi->dwVersion>>8)&0xFF,pgi->dwVersion&0xFF);
	if(MessageBox(szTip,NULL,MB_OKCANCEL|MB_ICONQUESTION)==IDOK)
	{
		char szDir[MAX_PATH]={0};
		sprintf(szDir,"%s\\game\\%s",theApp.m_szPath,pgi->szPath);
		SHFILEOPSTRUCT shfo={0};
		shfo.hwnd=m_hWnd;
		shfo.wFunc=FO_DELETE;
		shfo.pFrom=szDir;
		shfo.fFlags=FOF_NOCONFIRMATION;
		if(SHFileOperation(&shfo)==0)
		{//del ok
			delete pgi;
			m_lstGame.DeleteItem(iGame);
			m_ilGame.Remove(iGame);
			for(int i=iGame;i<m_lstGame.GetItemCount();i++)
			{
				SetListCtrlItemImage(&m_lstGame,i,i);
			}
		}else
		{
			MessageBox("游戏删除失败，可能正在使用");
		}
	}
}

void CIntraDoorDlg::OnGameDownloadFinish(char *pszGamePath,char *pszMsgBuf, DWORD dwLen)
{
	char szConfig[MAX_PATH];
	sprintf(szConfig,"%s\\game\\%s\\config.ini",theApp.m_szPath,pszGamePath);
	GAMEINFO gi={0};
	strcpy(gi.szPath,pszGamePath);
	GetPrivateProfileString("GAME","NAME",NULL,gi.szName,100,szConfig);
	GetPrivateProfileString("GAME","GUID",NULL,gi.szGuid,200,szConfig);
	GetPrivateProfileString("GAME","CLIENT",NULL,gi.szClient,100,szConfig);
	GetPrivateProfileString("GAME","SERVER",NULL,gi.szServer,100,szConfig);
	AddGame(gi);
	USHORT sPort;
	char *pbyData=(char *)pszMsgBuf;
	char *pszSvrGuid=pbyData;
	pbyData+=strlen(pbyData)+1;
	memcpy(&sPort,pbyData,2);
	sPort=ntohs(sPort);
	pbyData+=2;
	char cSeat=pbyData[0];
	char szGamePath[MAX_PATH],szStartInfo[200];
	sprintf(szGamePath,"%s\\game\\%s\\%s",theApp.m_szPath,gi.szPath,gi.szClient);
	sprintf(szStartInfo,"%s:%u|%d",pszSvrGuid,sPort,cSeat);
	ShellExecute(m_hWnd,"open",szGamePath,szStartInfo,NULL,SW_SHOW);
}

void CIntraDoorDlg::OnStateSet() 
{
	GetDlgItemText(IDC_STATE,theApp.m_UserInfo.szState,	UIS_STATE);
	theApp.BrdcstMsg(MAIN_USER,SUB_USER_STATE,theApp.m_UserInfo.szState,strlen(theApp.m_UserInfo.szState)+1);
}

void CIntraDoorDlg::OnHideme() 
{
	theApp.m_UserInfo.cHideMe=(char)IsDlgButtonChecked(IDC_HIDEME);	
	theApp.BrdcstMsg(MAIN_USER,SUB_USER_HIDEME,&theApp.m_UserInfo.cHideMe,1);
}

#include "brdcstinputdlg.h"
void CIntraDoorDlg::OnUserBrdcst() 
{
	CBrdcstInputDlg inputDlg;
	if(inputDlg.DoModal()==IDOK)
	{
		theApp.BrdcstMsg(MAIN_CHAT,0,(LPVOID)(LPCTSTR)inputDlg.m_strContent,inputDlg.m_strContent.GetLength()+1);
	}
}

void CIntraDoorDlg::OnGameRefresh() 
{
	DeleteGameList();
	CFileFind find;
	char szTarget[MAX_PATH];
	sprintf(szTarget,"%s\\Game",theApp.m_szPath);
	if(GetFileAttributes(szTarget)==-1)
	{//没有游戏目录时自动创建一个
		CreateDirectory(szTarget,NULL);
		return;
	}
	sprintf(szTarget,"%s\\game\\*.*",theApp.m_szPath);

	BOOL bFind=find.FindFile(szTarget);
	
	while(bFind)
	{
		bFind=find.FindNextFile();
		if(find.IsDirectory() && find.IsDots()==FALSE)
		{//找到一个子目录,再找配置文件
			char szConfig[MAX_PATH];
			sprintf(szConfig,"%s\\config.ini",find.GetFilePath());
			GAMEINFO gi={0};
			if(GetPrivateProfileString("GAME","NAME",NULL,gi.szName,100,szConfig)==0) break;
			GetPrivateProfileString("GAME","GUID",NULL,gi.szGuid,200,szConfig);
			strcpy(gi.szPath,find.GetFileTitle());
			GetPrivateProfileString("GAME","CLIENT",NULL,gi.szClient,100,szConfig);
			GetPrivateProfileString("GAME","SERVER",NULL,gi.szServer,100,szConfig);
			AddGame(gi);
		}
	}
	find.Close();
}

void CIntraDoorDlg::DeleteGameList()
{
	for(int i=m_lstGame.GetItemCount()-1;i>=0;i--)
	{
		GAMEINFO *pgi=(GAMEINFO*)m_lstGame.GetItemData(i);
		delete pgi;
		m_ilGame.Remove(i);
	}
	m_lstGame.DeleteAllItems();
	
}

void CIntraDoorDlg::OnEndSession(BOOL bEnding) 
{
	SaveState();
	CDialog::OnEndSession(bEnding);	
}

void CIntraDoorDlg::SaveState()
{
	ShowTray(FALSE);
	//保存隐身标志
	char szUserInfoCfg[MAX_PATH];
	sprintf(szUserInfoCfg,"%s\\userinfo.ini",theApp.m_szPath);
	WritePrivateProfileString("USERINFO","HIDEME",theApp.m_UserInfo.cHideMe?"1":"0",szUserInfoCfg);
	int i;
	for(i=0;i<MAX_GAMESVR;i++)
	{
		if(m_GameSvr[i].dwThreadID) 	PostThreadMessage(m_GameSvr[i].dwThreadID,WM_QUIT,0,0);
	}

	DeleteGameList();

	//保存列表项的顺序
	char szSetting[MAX_PATH];
	sprintf(szSetting,"%s\\settings.ini",theApp.m_szPath);
	LVCOLUMN lvc;
	lvc.mask=LVCF_ORDER|LVCF_WIDTH;
	char szBuf[20];
	for(i=0;i<SIZE_COLUMN;i++)
	{
		m_lstUser.GetColumn(i,&lvc);
		sprintf(szBuf,"%d",lvc.iOrder);
		WritePrivateProfileString("ORDER",g_szColName[i],szBuf,szSetting);
		sprintf(szBuf,"%d",lvc.cx);
		WritePrivateProfileString("WIDTH",g_szColName[i],szBuf,szSetting);
	}
	theApp.BrdcstMsg(MAIN_USER,SUB_USER_LOGOFF,NULL,0);
}

void CIntraDoorDlg::OnFiletransMonitor() 
{
	if(m_pFileTransDlg->IsWindowVisible()) return;
	RECT rc;
	GetWindowRect(&rc);
	m_pFileTransDlg->SetWindowPos(NULL,rc.left,rc.bottom,0,0,SWP_NOZORDER|SWP_NOSIZE|SWP_SHOWWINDOW);
}

void CIntraDoorDlg::UpdateGroupList()
{
	CStringArray saGroup;
	USERINFO ui;
	DWORD    dwIP;
	POSITION pos=m_mapIP2UI.GetStartPosition();
	while(pos)
	{
		m_mapIP2UI.GetNextAssoc(pos,dwIP,ui);
		BOOL bFind=FALSE;
		for(int i=0;i<saGroup.GetSize();i++)
		{
			if(strcmp(saGroup[i],ui.szGroup)==0)
			{
				bFind=TRUE;
				break;
			}
		}
		if(!bFind) saGroup.Add(ui.szGroup);
	}
	char szBuf[UIS_GROUP+1];
	TCITEM tItem;
	tItem.mask=TCIF_TEXT;
	tItem.cchTextMax=UIS_GROUP+1;
	tItem.pszText=szBuf;
	for(int i=0;i<saGroup.GetSize();i++)
	{
		BOOL bExist=FALSE;
		for(int j=1;j<m_tabGroup.GetItemCount();j++)
		{
			m_tabGroup.GetItem(j,&tItem);
			if(strcmp(saGroup[i],szBuf)==0)
			{
				bExist=TRUE;
				break;
			}
		}
		if(!bExist)
		{
			m_tabGroup.InsertItem(m_tabGroup.GetItemCount(),saGroup[i]);
		}
	}
}

int CIntraDoorDlg::FindUser(DWORD dwIP)
{
	for(int i=0;i<m_lstUser.GetItemCount();i++)
	{
		if(m_lstUser.GetItemData(i)==dwIP) return i;
	}
	return -1;
}

void CIntraDoorDlg::OnSelchangeTabGroup(NMHDR* pNMHDR, LRESULT* pResult) 
{
	int iCurSel=m_tabGroup.GetCurSel();
	char szGroup[UIS_GROUP+1];
	TCITEM tItem;
	tItem.mask=TCIF_TEXT;
	tItem.cchTextMax=UIS_GROUP+1;
	tItem.pszText=szGroup;
	m_tabGroup.GetItem(iCurSel,&tItem);

	m_lstUser.DeleteAllItems();
	USERINFO ui;
	DWORD    dwIP;
	POSITION pos=m_mapIP2UI.GetStartPosition();
	while(pos)
	{
		m_mapIP2UI.GetNextAssoc(pos,dwIP,ui);
		if(iCurSel==0 || strcmp(szGroup,ui.szGroup)==0)
		{
			AddUser2List(dwIP,ui);
		}
	}
	
	*pResult = 0;
}

//群发
void CIntraDoorDlg::OnUserChatgroup() 
{
	CBrdcstInputDlg inputDlg;
	if(inputDlg.DoModal()==IDOK)
	{
		for(int i=0;i<m_lstUser.GetItemCount();i++)
		{
			theApp.SendMsg(m_lstUser.GetItemData(i),PORT_DOOR,MAIN_CHAT,0,(void*)(LPCTSTR)inputDlg.m_strContent,inputDlg.m_strContent.GetLength()+1);
			Sleep(10);//防止网络阻塞
		}
	}	
}
