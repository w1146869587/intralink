//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by intraDoor.rc
//
#define IDR_MANIFEST                    1
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_INTRADOOR_DIALOG            102
#define IDP_SOCKETS_INIT_FAILED         103
#define IDR_MAINFRAME                   128
#define IDD_USERINFO                    129
#define IDD_CHAT                        130
#define IDD_FILETRANS                   131
#define IDD_FILECONFIRM                 132
#define IDD_GAMESVR                     133
#define IDB_FTSTATE                     135
#define IDM_USER                        136
#define IDM_SYSTEM                      137
#define IDD_FIND                        138
#define IDM_INTRADOOR                   139
#define IDD_GAMESETUPDIR                140
#define IDD_BRDCST                      141
#define IDB_FACE                        143
#define IDC_LIST_USER                   1000
#define IDC_NAME                        1001
#define IDC_SEX_MALE                    1002
#define IDC_SEX_FEMALE                  1003
#define IDC_SEX_SECRET                  1004
#define IDC_PHONE                       1005
#define IDC_DESC                        1006
#define IDC_HISTORY                     1007
#define IDC_SEND                        1008
#define IDC_SAY                         1009
#define IDC_LIST_DOWNLOAD               1010
#define IDC_FILETRANS                   1011
#define IDC_FILETRANS_MONITOR           1011
#define IDC_FTLIST                      1012
#define IDC_LIST_UPLOAD                 1013
#define IDC_LIST_TASK                   1014
#define IDC_CONFIRM                     1015
#define IDC_CONFIRM_RECIEVE             1015
#define IDC_REJECT                      1016
#define IDC_CONFIRM_REJECT              1016
#define IDC_AVATAR_PREVIEW              1017
#define IDC_AVATAR                      1018
#define IDC_USERAVATAR                  1020
#define IDC_USERINFO                    1021
#define IDC_LIST_GAMESVR                1024
#define IDC_LOGIN                       1025
#define IDC_ENDSVR                      1026
#define IDC_AVATAR_NORMAL               1027
#define IDC_DELETETASK                  1028
#define IDC_DOWNLOAD_DELETETASK         1028
#define IDC_CANCELTASK                  1029
#define IDC_DOWNLOAD_CANCELTASK         1029
#define IDC_TAB_TASKTYPE                1030
#define IDC_UPLOAD_DELETETASK           1031
#define IDC_HIDEME                      1032
#define IDC_FIND                        1034
#define IDC_LIST_GAME                   1037
#define IDC_STATE                       1043
#define IDC_GAMEDIR                     1044
#define IDC_TIP                         1045
#define IDC_STATE_SET                   1046
#define IDC_CONTENT                     1047
#define IDC_HOMESITE                    1050
#define IDC_UPLOAD_AUTODEL              1053
#define IDC_UPLOAD_CANCELTASK           1054
#define IDC_DOWNLOAD_OPENDIR            1055
#define IDC_LABEL                       1056
#define IDC_GROUP                       1057
#define IDC_TAB_GROUP                   1058
#define IDC_SAVE                        1059
#define IDM_USER_CHAT                   32771
#define IDM_USER_SENDFILE               32772
#define IDM_USER_SENDDIR                32773
#define IDM_USER_FIND                   32774
#define IDM_USER_REFRESH                32775
#define IDM_SYS_ABOUT                   32776
#define IDM_SYS_SHOWMAIN                32777
#define IDM_SYS_USERINFO                32778
#define IDM_SYS_QUIT                    32779
#define IDM_SYS_HELP                    32782
#define IDM_WIN_FILETRANS               32785
#define IDM_WIN_FILEREQ                 32786
#define IDM_WIN_GAMESVR                 32787
#define IDM_HELP_ABOUT                  32788
#define IDM_HELP_INDEX                  32789
#define IDM_USER_BRDCST                 32794
#define IDM_GAME_START                  32795
#define IDM_GAME_DEL                    32796
#define IDM_GAME_REFRESH                32797
#define IDM_USER_CHATGROUP              32799

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        146
#define _APS_NEXT_COMMAND_VALUE         32800
#define _APS_NEXT_CONTROL_VALUE         1060
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
