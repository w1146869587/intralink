// GameSvrDlg.cpp : implementation file
//

#include "stdafx.h"
#include "intradoor.h"
#include "GameSvrDlg.h"
#include "intradoorDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGameSvrDlg dialog


CGameSvrDlg::CGameSvrDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CGameSvrDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CGameSvrDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CGameSvrDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGameSvrDlg)
	DDX_Control(pDX, IDC_LIST_GAMESVR, m_lstGameSvr);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CGameSvrDlg, CDialog)
	//{{AFX_MSG_MAP(CGameSvrDlg)
	ON_BN_CLICKED(IDC_LOGIN, OnLogin)
	ON_BN_CLICKED(IDC_ENDSVR, OnEndsvr)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGameSvrDlg message handlers

BOOL CGameSvrDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_lstGameSvr.InsertColumn(0,"��Ϸ",0,200);
	m_lstGameSvr.InsertColumn(1,"�˿�",0,100);

	CIntraDoorDlg *pMainDlg=(CIntraDoorDlg*)GetOwner();
	for(int i=0;i<MAX_GAMESVR;i++)
	{
		if(pMainDlg->m_GameSvr[i].dwThreadID)
		{
			int iItem=m_lstGameSvr.GetItemCount();
			m_lstGameSvr.InsertItem(iItem,pMainDlg->m_GameSvr[i].szName);
			char szPort[20];
			sprintf(szPort,"%d",PORT_SVRBASE+i);
			m_lstGameSvr.SetItemText(iItem,1,szPort);
			m_lstGameSvr.SetItemData(iItem,i);
		}
	}
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CGameSvrDlg::OnLogin() 
{
	CIntraDoorDlg *pMainDlg=(CIntraDoorDlg*)GetOwner();
	int iItem=m_lstGameSvr.GetSelectionMark();
	if(iItem==-1)
	{
		return;
	}
	char szCmdLine[200];
	int iGame=m_lstGameSvr.GetItemData(iItem);
	sprintf(szCmdLine,"%s:%u|-1",theApp.m_UserInfo.szGuid,PORT_SVRBASE+iGame);
	ShellExecute(m_hWnd,"open",pMainDlg->m_GameSvr[iGame].szClientPath,szCmdLine,NULL,SW_SHOW);
}

void CGameSvrDlg::OnEndsvr() 
{
	CIntraDoorDlg *pMainDlg=(CIntraDoorDlg*)GetOwner();
	int iItem=m_lstGameSvr.GetSelectionMark();
	if(iItem==-1)
	{
		return;
	}
	int iGame=m_lstGameSvr.GetItemData(iItem);
	PostThreadMessage(pMainDlg->m_GameSvr[iGame].dwThreadID,WM_QUIT,0,0);
	m_lstGameSvr.DeleteItem(iItem);
}
