#if !defined(AFX_AVATARCTRL_H__4853A62F_16A5_461B_B80A_5CF4953C1EF0__INCLUDED_)
#define AFX_AVATARCTRL_H__4853A62F_16A5_461B_B80A_5CF4953C1EF0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AvatarCtrl.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CAvatarCtrl window
#include "gif/gif.h"

class CAvatarCtrl : public CStatic
{
// Construction
public:
	CAvatarCtrl();
	CGIF		m_gif;
	int			m_nCurFrame;
// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAvatarCtrl)
	//}}AFX_VIRTUAL

// Implementation
public:
	BOOL SetGifFile(LPCTSTR pszFileName);
	virtual ~CAvatarCtrl();

	// Generated message map functions
protected:
	void DrawFrame(CGifFrame *pFrame,CDC *pDC=NULL);
	//{{AFX_MSG(CAvatarCtrl)
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnPaint();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AVATARCTRL_H__4853A62F_16A5_461B_B80A_5CF4953C1EF0__INCLUDED_)
