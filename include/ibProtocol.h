#ifndef _INTRANETBUSPROTOCOL_
#define _INTRANETBUSPROTOCOL_

#define NAME_INTRADOOR		"内网快车"
#define NAME_REGITEM		"Software\\setoutsoft\\IntraLink"

#define PORT_DOOR		3050		//大厅占用的端口
#define PORT_CLIENT		3051		//游戏客户端占用的端口
#define PORT_SVRBASE	3110		//游戏服务器的初始端口
#define MAX_GAMESVR		10			//最多同时启动的服务器数量
#define UM_SOCKETMSG	(WM_USER+100)

#define UIS_GUID	37
#define UIS_NAME	50
#define UIS_DESC	300
#define UIS_PHONE	30
#define UIS_STATE	30
#define UIS_GROUP	30
#pragma pack(push,1)
typedef struct tagUSERINFO
{//用户信息,不能超过500字节，以利于用一个数据包发送
	char	szGuid[UIS_GUID+1];		//用户ID
	char	szName[UIS_NAME+1];		//用户名称
	char	szDesc[UIS_DESC+1];		//用户自我说明
	char	szPhone[UIS_PHONE+1];	//电话
	char	szState[UIS_STATE+1];	//用户状态
	char	cSex;					//用户姓别
	char	cHideMe;				//隐身标志
	char	szGroup[UIS_GROUP+1];	//分组信息
}USERINFO,*PUSERINFO;

typedef struct tagIBMSG
{
	DWORD dwDataLen;	//数据长度
	DWORD dwMainType;	//主类型
	DWORD dwSubType;	//子类型
	BYTE  byData[1];	//数据地址
}IBMSG,*PIBMSG;
#pragma pack(pop)

#define MAIN_USER		1
#define SUB_USER_LOGIN		1	//用户上线
#define SUB_USER_LOGOFF		2	//用户下线
#define SUB_USER_ONLINE		3	//用户在线
#define SUB_USER_STATE		4	//用户更改状态
#define SUB_USER_HIDEME		5	//用户更改隐身状态
#define SUB_USER_USERINOFO	6	//用户信息修改消息

#define MAIN_CHAT		2

#define MAIN_FILETRANS	3	//文件传输消息
#define SUB_FILETRANS_REQ	0	//请求文件传输 +文件大小+文件名
#define SUB_FILETRANS_ACK	1	//回复文件传输 +接受标志+文件名+新文件名
#define SUB_FILETRANS_DIR	2	//请求目录发送 +目录大小+目录文件数+子目录数+目录名
#define SUB_FILETRANS_DEL	3	//文件传输请求被上传方取消 +文件名
#define	SUB_FILETRANS_GAME	4	//传游戏	+目录大小+目录文件数+子目录数+目录名+保存目录名

#define MAIN_GAME	4		//游戏与大厅交互
#define SUB_GAME_USERINFO	0	//查询自己的用户信息
#define SUB_GAME_QUERYMATE	1	//游戏中发送的查询在线人消息,回应的byData中前4个字节为用户IP,0表示终止
#define SUB_GAME_INVITE		2	//游戏中发送的邀请消息,服务器ID(GUID)+1字节座位号+游戏ID(GUID)+游戏名称+4游戏版本+游戏目录
#define SUB_GAME_REQUESTSETUP	3	//请求安装程序 游戏ID+下载方选择的保存目录名(不是全路径)
#define SUB_GAME_REFUSE		4	//拒绝邀请
#define SUB_GAME_SVREND		5	//游戏服务器退出
#define SUB_GAME_SERVERIP	6	//通过服务器名称查询服务器的IP,由游戏客户端发送
#define SUB_GAME_VERERROR	7	//游戏版本不同
#endif//_INTRANETBUSPROTOCOL_